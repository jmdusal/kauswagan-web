<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumTable extends Migration
{
    
    public function up()
    {
        if(!Schema::hasTable('albums'))
            {
                Schema::create('albums', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->string('date_input');
                    $table->timestamps();
                    
                });
            }
        }

        public function down()
        {
            Schema::dropIfExists('albums');
        }
    }

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesGalleryTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('stories_gallery'))
            {
                Schema::create('stories_gallery', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('display_image');
                    $table->string('date_input');
                    $table->biginteger('story_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('story_id')->references('id')->on('stories')->onDelete('CASCADE');
                    $table->primary('id');
                });
            }
        }


        public function down()
        {
            Schema::dropIfExists('stories_gallery');
        }
    }

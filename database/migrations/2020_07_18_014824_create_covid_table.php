<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCovidTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('covids'))
            {
                Schema::create('covids', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->longtext('description');
                    $table->string('display_image');
                    $table->string('date_created');
                    $table->string('is_flagship');
                    $table->biginteger('author_id')->unsigned();
                    $table->foreign('author_id')->references('id')->on('authors')->onDelete('CASCADE');
                    $table->primary('id');
                    

                });
            }
        }

        
        public function down()
        {
            Schema::dropIfExists('covids');
        }
    }

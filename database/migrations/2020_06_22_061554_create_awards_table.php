<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardsTable extends Migration
{


    public function up()
    {
        if(!Schme::hasTable('awards'))
            {
                Schema::create('awards', function(Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->longtext('description');
                    $table->string('display_image');
                    $table->longtext('link');
                    $table->string('date_award_taken');
                });
            }
        }

        public function down()
        {
            Schema::dropIfExists('awards');
        }
    }

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangaysOfficialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('barangays_official'))
            {
                Schema::create('barangays_official', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('name');
                    $table->string('position');
                    $table->string('display_image');
                    $table->biginteger('barangay_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('barangay_id')->references('id')->on('barangays')->onDelete('CASCADE');
                    
                });
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangays_official');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('authors'))
            {
                Schema::create('authors', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('name');
                    $table->string('display_image');
                    $table->string('description');
                    $table->string('facebook_link');
                    $table->string('twitter_link');
                    $table->primary('id');
                });
            }
        }

        
        public function down()
        {
            Schema::dropIfExists('authors');
        }
    }

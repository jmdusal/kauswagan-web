<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoriesTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('directories'))
            {
                Schema::create('directories', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('directory_name');
                    $table->longtext('description');
                    $table->timestamps();
                });
            }
        }
        

        public function down()
        {
            Schema::dropIfExists('directories');
        }
    }

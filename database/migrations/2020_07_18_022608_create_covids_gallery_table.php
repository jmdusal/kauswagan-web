<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCovidsGalleryTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('covids_gallery'))
            {
                Schema::create('covids_gallery', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('display_image');
                    $table->string('date_input');
                    $table->biginteger('covid_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('covid_id')->references('id')->on('covids')->onDelete('CASCADE');
                });
            }
        }

        public function down()
        {
            Schema::dropIfExists('covids_gallery');
        }
    }

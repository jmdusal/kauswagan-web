<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('stories'))
            {
                Schema::create('stories', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->string('description');
                    $table->string('display_image');
                    $table->string('is_flagship');
                    $table->string('input_by');
                    $table->biginteger('category_id')->unsigned();
                    $table->biginteger('author_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
                    $table->foreign('author_id')->references('id')->on('authors')->onDelete('CASCADE');
                    $table->primary('id');

                });
            }
        }


        public function down()
        {
            Schema::dropIfExists('stories');
        }
    }

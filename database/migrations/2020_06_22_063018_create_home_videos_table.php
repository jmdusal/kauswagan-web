<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('home_videos'))
            {
                Schema::create('home_videos', function(Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->string('display_image');
                    $table->longtext('embed_video');
                    $table->string('is_flagship');
                    $table->timestamps();
                });
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_videos');
    }
}

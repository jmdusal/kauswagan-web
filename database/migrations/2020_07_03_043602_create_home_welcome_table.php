<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeWelcomeTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('home_welcome'))
            {
                Schema::create('home_welcome', function(Blueprint $table)
                {
                    $table->increments('id');
                    $table->string('title');
                    $table->string('display_image');
                    $table->longtext('description');
                    $table->timestamps();
                });
            }
        }


        public function down()
        {
            Schema::dropIfExists('home_welcome');
        }
    }

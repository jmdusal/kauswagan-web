<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsGalleryTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('news_gallery'))
            {
                Schema::create('news_gallery', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('display_image');
                    $table->string('date_input');
                    $table->biginteger('newy_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('newy_id')->references('id')->on('news')->onDelete('CASCADE');
                    $table->primary('id');
                });
            }
        }


        public function down()
        {
            Schema::dropIfExist('news_gallery');
        }
    }

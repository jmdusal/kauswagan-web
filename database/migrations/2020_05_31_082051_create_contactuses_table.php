<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactusesTable extends Migration
{
    
    public function up()
    {
        if(!Schema::hasTable('contactuses'))
            {
                Schema::create('contactuses', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('name');
                    $table->string('email');
                    $table->text('message');
                    $table->timestamps();
                });
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

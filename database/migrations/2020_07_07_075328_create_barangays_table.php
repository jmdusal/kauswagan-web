<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangaysTable extends Migration
{
    
    public function up()
    {
        if(!Schema::hasTable('barangays'))
            {
                Schema::create('barangays', function (Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('barangay_name');
                    $table->longtext('description');
                    $table->string('display_image');
                    $table->timestamps();
                });
            }
        }

        public function down()
        {
            Schema::dropIfExists('barangays');
        }
    }

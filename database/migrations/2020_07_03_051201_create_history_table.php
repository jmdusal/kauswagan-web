<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration
{
    
    public function up()
    {
        if(!Schema::hasTable('histories'))
            {
                Schema::create('histories', function(Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->longtext('description');
                    $table->timestamps();
                });
            }
        }

        
        public function down()
        {
            Schema::dropIfExists('histories');
        }
    }

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeGalleriesTable extends Migration
{
    
    public function up()
    {
        if(!Schema::hasTable('home_galleries'))
            {
                Schema::create('home_galleries', function(Blueprint $table)
                {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->string('display_image');
                    $table->string('date_taken');
                    $table->biginteger('album_id')->unsigned();
                    $table->timestamps();
                    $table->foreign('album_id')->references('id')->on('albums')->onDelete('CASCADE');
                    $table->primary('id');
                    
                });
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_galleries');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMayorOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('mayor_office'))
            {
                Schema::create('mayor_office', function(Blueprint $table)
                {
                    $table->increments('id');
                    $table->string('mayor_name');
                    $table->string('display_image');
                    $table->longtext('mini_title');
                    $table->longtext('description');
                    $table->timestamps();
                });
            }
        }


        public function down()
        {
            Schema::dropIfExists('mayor_office');
        }
    }

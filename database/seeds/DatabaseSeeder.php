<?php

// use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public function run()
	{
		Eloquent::unguard();
		$this->call(UsersTablesSeeder::class);
	}
}

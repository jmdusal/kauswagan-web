<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTablesSeeder extends Seeder
{
	public function run()
	{
		User::create([
			'name'              => 'User Test',
			'email'             => 'usertest@admin.com',
			'password'          => Hash::make('teamicrra2001'),
			'remember_token'    => str_random(10),
		]);
	}
}

@extends('user.header')

@section('title')
View Author
@endsection

@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">



                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
<!-- <h4>User Profile</h4>
    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
</div>
</div>
</div>
<div class="col-lg-4">
    <div class="page-header-breadcrumb">
<!--         <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index.html"> <i class="feather icon-home"></i> </a>
            </li>
            <li class="breadcrumb-item"><a href="{{ asset('/authors')}}">Authors Table</a>
            </li>
            <li class="breadcrumb-item">View</li>
        </li>
    </ul> -->
</div>
</div>
</div>
</div>






<div class="page-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="cover-profile">
             <div class="profile-bg-img">
                <img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('adminty/files/assets/images/user-profile/bg-img1.jpg')}}" alt="bg-img">
                <div class="card-block user-info">
                    <div class="col-md-12">
                        <div class="media-left">
                            <a class="profile-image">
                                <img style="height: 200px; width: 200px;" class="user-img img-radius" src="{{asset('storage/authors_images')}}/{{$author->display_image}}" alt="user-img">
                            </a>
                        </div>

                        <div class="media-body row">
                            <div class="col-lg-12">
                                <div class="user-title">
                                    <h2>{{$author->name}}</h2>
                                    <!-- <span class="text-white">Author</span> -->
                                </div>
                            </div>
                            <div>

<!-- <div class="pull-right cover-btn">
<button type="button" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-plus"></i> Follow</button>
<button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message</button>
</div> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="tab-header card business-info services m-b-20">
            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Author Info</a>
                    <div class="slide"></div>
                </li>

            </ul>
        </div>


        <div class="tab-content">
            <div class="tab-pane active" id="personal" role="tabpanel">

                <div class="card business-info services m-b-20">
                    <div class="card-header">
                        <h5 class="card-header-text">Details</h5>
                    </div>
                    <div class="card-block">
                        <div class="view-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">Name</th>
                                                                <td>{{$author->name}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Facebook Link</th>
                                                                <td>{{$author->facebook_link}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Twitter Link</th>
                                                                <td>{{$author->twitter_link}}</td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>



                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
<!-- <th scope="row">Created at</th>
    <td>{{$author->created_at}}</td> -->
</tr>
<tr>
<!-- <th scope="row">Updated at</th>
    <td>{{$author->updated_at}}</td> -->
</tr>

</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="edit-info">
    <div class="row">
        <div class="col-lg-12">
            <div class="general-info">
                <div class="row">
                    <div class="col-lg-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card business-info services m-b-20">
            <div class="card-header">
                <h5 class="card-header-text">Description</h5>

            </div>
            <div class="card-block user-desc">
                <div class="view-desc">
                    <p>{!! $author->description !!}</p>
                </div>

            </div>
        </div>
    </div>
</div>

</div>
<div class="tab-pane" id="binfo" role="tabpanel">

    <div class="card">
        <div class="card-header">
            <h5 class="card-header-text">Galleries</h5><br><br>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Image</button>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">
                    <div class="card b-l-success business-info services m-b-20">
                        <div class="card-header">
                            <div class="service-header">
                                <a href="#">

                                </a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12">




                                    <!-- show up galleries here -->





                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

<form role="form" method="post" action="{{url('/storiesgallery/submit')}}" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                    </center>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="icon-list-demo">
                            <!-- <i id="icon" class="fa fa-wpbeginner fa-lg"></i> -->
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div class="form-group row">
                            <!-- <label class="col-sm-2 col-form-label">Stories</label> -->
                            <div class="col-sm-10">
                                <input type="hidden" name="" value="" class="form-control">
                                <input name="invisible" type="hidden" value="secret">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Upload Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="display_image" class="form-control">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="create" class="btn btn-primary">Send</button>
                <button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
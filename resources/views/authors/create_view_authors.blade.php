@extends('user.header')

@section('title')
Add Author
@endsection

@section('content')

@section('stylesheets')
<script>
  tinymce.init({

    selector: 'textarea',
    height: 500,
    menubar: false,
    plugins: [

    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],

  });
</script>
@endsection

<div class="pcoded-content">
  <div class="pcoded-inner-content">
   @if(count($errors) > 0)
   <div class="alert alert-danger background-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <i class="icofont icofont-close-line-circled text-white"></i>
    </button>
    @foreach($errors->all() as $error)
    <strong>{{$error}}</strong>
    @endforeach
  </div>
  @endif

  <!-- @include('sweet::alert') -->
  <div class="main-body">
    <div class="page-wrapper">

      <div class="page-header">
        <div class="row align-items-end">
          <div class="col-lg-8">
            <div class="page-header-title">
              <div class="d-inline">


              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                            <!-- <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="index.html"> <i class="feather icon-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ asset('/authors')}}">Stories Table</a>
                                </li>
                                <li class="breadcrumb-item">Create</li>
                            </li>
                          </ul> -->
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="page-body">
                    <div class="row">
                      <div class="col-sm-12">

                        <div class="card">
                          <div class="card-header">

                            <div class="card-header-right">

                            </div>
                          </div>

                          <div class="card-block">
                            <h4 class="sub-title">Add Author</h4>
                            <form role="form" method="post" action="{{url('/authors/authorsdata/submit')}}" enctype="multipart/form-data" class="form_submit">
                              {{csrf_field()}}


                              <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control" required="True" placeholder="Enter Full Name">
                                </div>
                              </div>


                              <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Upload Image File</label>
                                <div class="col-sm-10">
                                  <input type="file" name="display_image" class="form-control" required="true">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                  <textarea name="description" rows="12" cols="7" class="form-control"></textarea>

                                </div>

                              </div>
                              <br>
                              <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Facebook Link</label>
                                <div class="col-sm-10">
                                  <input type="url" name="facebook_link" class="form-control" placeholder="e.g https://www.facebook.com/minbits">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Twitter Link</label>
                                <div class="col-sm-10">
                                  <input type="url" name="twitter_link" class="form-control" placeholder="e.g https://twitter.com/minbits">
                                </div>
                              </div>

                              <button type="create" class="btn btn-success">Submit</button>
                            </form>



                          </div>
                        </div>
                      </div>
                    </div>




                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">

  $(".form_submit").submit(function(e) {

    e.preventDefault();

    swal({
      html:   '<div class="loader-block">'+
      '<img src="'+'{{ asset("design/website/img/loading.gif")}}'+'" width="150" height="150">'+
      '</div>'+
      '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
      '<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
      allowOutsideClick: false,
      showConfirmButton:false,
    });

    setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);

    setTimeout(function(){

      $('.form_submit').ajaxSubmit({
        beforeSubmit: function(){
          $('.progress-bar').width('0%')
        },
        uploadProgress: function(event, position, total, percentComplete){

          $('#progress-status').text(percentComplete+'%');
        },            
        error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
            title: "Error!",
            text: "You`ve insert wrong file type! Please upload an image file!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
        },
        success: function(results){

          console.log(results);

          if(results.success == true){

            $('.progress-bar').width('0%').html('');

            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });

            setTimeout(function(){
              window.location.href = '{{ url("/authors/table")}}';
            }, 1000);
            
          }else{

            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1000);
          }

        },
        resetForm: true
      });

    }, 1500);
  });

</script>

@endsection
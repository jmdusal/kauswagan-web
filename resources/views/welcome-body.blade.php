@extends('welcome')

@section('title')
Municipality of Kauswagan
@endsection

@section('content')
<div class="main-container">
  <section
  class="cover height-90 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
  <ul class="slides">

    @foreach ($homewelcome as $key => $welcome)
    <li class="imagebg" data-overlay="4">
      <div class="background-image-holder background--top" style="height: 100%" >
        <img alt="background" src="{{ asset('storage/home_welcome_images')}}/{{$welcome->display_image}}" />
      </div>
      <div class="container pos-vertical-center">
        <div class="row">

          <div class="col-md-12">
            <h1>{{ $welcome->title }}</h1>
            <!--<p>The word "Kauswagan", which literally means "<span class="type--bold">prosperity</span>” or “<span class="type--bold">progress</span>" is a 5th class municipality <br> in the Province of Lanao del Norte in Northern Mindanao, Philippines. <br>The Municipality of Kauswagan is famous for its abundance in coastal resources such as fresh fish and shells.</p> -->
            {!! $welcome->description !!}
            <a class="btn btn--primary type--uppercase" href="{{url('/history')}}">
              <span class="btn__text">
                Our history
              </span>
            </a>
          </div>
        </div>
      </div>
    </li>
    @endforeach



    <li class="imagebg" data-overlay="4">
      <div class="background-image-holder">
        <img alt="background" src="{{ asset('design/website/img/history.jpg')}}" />
      </div>
      <div class="container pos-vertical-center">
        <div class="row">

          <div class="col-md-4">
            <h2>
              Vision
            </h2>
            <p>
              “A leading organic agriculture and energy generating municipality, advocate of sustainable peace and unity among muslims and christians, and an eco- tourism destination with empowered and disciplined <br> constituents with a high sense of patriotism ,living in an ecologically balanced environment, headed by god fearing , strong , and determined leadership “
            </p>
          </div>

          <div class="col-md-4">
            <h2>Mission</h2>
            <p>
              "Attainment of a dynamic and more improved economy, more particularly through increased agricultural production, proper management of existing environment and the promotion of more equitable access to social development opportunities and full utilization of human resources"
            </p>
          </div>
          <div class="col-md-4">
            <h2>GOALS AND OBJECTIVE</h2>
            <p>
              "To improve the socio-economic status and social well-being of its populace To have a more equitable distribution of benefits derived from identified and prioritized development programs and projects.
              To intensify agro-industrial development initiatives to have more opportunities for employment. To increase production and profits from the agricultural section."
            </p>
          </div>
        </div>
        <!--end of row-->
      </div>
      <!--end of container-->
    </li>
  </ul>
</section>






@if(count($stories) != 0)
<section>
  <div class="container">
    <h2 class="text-center">Our Stories</h2>
    <div class="row">
      @foreach($stories as $key => $story)
      @if($key==0)
      @if(count($stories) == 1)  
      <div class="col-md-12">
        @else
        <div class="col-md-7 col-6">
          @endif
          <a href="{{url('/story-detail', $story->id)}}" class="block">
            <div class="feature feature-7 boxed text-center imagebg" data-overlay="2" style="height: 300px">
              <div class="background-image-holder">

                <img alt="background" src="{{ asset('storage/stories_images')}}/{{$story->display_image}}"/>

              </div>
              <div class="row justify-content-center pos-vertical-center">
                <div class="col-md-12">
                  <h4>{{$story->title}}</h4>
                </div>
                <div class="col-md-12">
                  <div style=" overflow: hidden;text-overflow: ellipsis; color: white; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
                    @php 
                    echo strip_tags($story->description);
                    @endphp
                  </div>
                </div>

              </div>
            </div>
          </a>
        </div>
        
        @elseif($key==1)
        <div class="col-md-5 col-6">
          <a href="{{url('/story-detail', $story->id)}}" class="block">
            <div class="feature feature-7 boxed text-center imagebg" data-overlay="2" style="height: 300px">
              <div class="background-image-holder">

               <img alt="background" src="{{ asset('storage/stories_images')}}/{{$story->display_image}}" />

             </div>
             <div class="row justify-content-center pos-vertical-center">
              <div class="col-md-12">
                <h4>{{$story->title}}</h4>
              </div>
              <div class="col-md-12">
                <div style=" overflow: hidden;text-overflow: ellipsis; color: white; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
                  @php 
                  echo strip_tags($story->description);
                  @endphp
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <br>

      @elseif($key==2)
      @if(count($stories) == 3)
      <div class="col-md-12">
        @else
        <div class="col-md-5 col-6">
          @endif

          <a href="{{url('/story-detail', $story->id)}}" class="block">
            <div
            class="feature feature-7 boxed text-center imagebg"
            data-overlay="2" style="height: 300px"
            >
            <div class="background-image-holder">
              <img alt="background" src="{{ asset('storage/stories_images')}}/{{$story->display_image}}" />
            </div>
            <div class="row justify-content-center pos-vertical-center">
              <div class="col-md-12">
                <h4>{{$story->title}}</h4>
              </div>

              <div class="col-md-12">
                <div style=" overflow: hidden;text-overflow: ellipsis; color: white; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
                  @php 
                  echo strip_tags($story->description);
                  @endphp
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      @elseif($key==3)
      <div class="col-md-7 col-6">
        <a href="{{url('/story-detail', $story->id)}}" class="block">
          <div
          class="feature feature-7 boxed text-center imagebg"
          data-overlay="5" style="height: 300px"
          >
          <div class="background-image-holder">
            <img alt="background" src="{{ asset('storage/stories_images')}}/{{$story->display_image}}" />
          </div>
          <div class="row justify-content-center pos-vertical-center">
           <div class="col-md-12">
            <h4>{{$story->title}}</h4>
          </div>

          <div class="col-md-12">
            <div style=" overflow: hidden;text-overflow: ellipsis; color: white; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
              @php 
              echo strip_tags($story->description);
              @endphp
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  @endif

  @endforeach
</div>
<br>
<div class="row justify-content-center">
  <div class="text-center">
    <a class="btn type--uppercase" href="{{url('/stories')}}">
      View All Stories
    </a>
  </div>

</div>
<!--end of row-->
</div>
<!--end of container-->
</section>

@endif




@foreach ($homeuswag as $key => $uswag)
<section class="cover imagebg height-80 text-center" data-overlay="3">
  <div class="background-image-holder">
    <img alt="background" src="{{ asset('storage/home_welcome_images')}}/{{$uswag->display_image}}" />
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-12">
        <span class="h1 inline-block">Uswag </span>
        <span
        class="h1 inline-block typed-text typed-text--cursor"
        data-typed-strings="{{ $uswag->title }}"
        ></span>
        <div class="lead">
         {!! $uswag->description !!} 
       </div>

       <div class="text-center-xs">
        <a
        class="btn btn--primary type--uppercase"
        href="https://www.facebook.com/uswagturismokauswaganon/photos/?ref=page_internal"
        >
        <span class="btn__text">
          Visit FB Page
        </span>
      </a>
    </div>

    <div class="row justify-content-center">
      <a href="#" target="__blank">
        <img src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" height="80" width="80"  alt="Avatar" style="margin: 5px">
      </a>

      <a href="https://www.facebook.com/galingpook/"  target="__blank">
        <img src="{{ asset('design/website/img/kauswagan/galing_pook.png')}}" height="80" width="80"  alt="Avatar" style="border-radius: 50%;margin: 5px">
      </a>

      <a href="https://www.facebook.com/uswagturismokauswaganon/"  target="__blank">
        <img src="{{ asset('design/website/img/kauswagan/uswag_logo.png')}}" height="80" width="80"  alt="Avatar" style="border-radius: 50%;margin: 5px">
      </a>

      <a href="https://www.facebook.com/KayaNatinPH/"  target="__blank">
        <img src="{{ asset('design/website/img/kauswagan/kaya_logo.png')}}" height="80" width="80"  alt="Avatar" style="border-radius: 50%;margin: 5px">
      </a>

      <a href="https://www.facebook.com/groups/123456284520811/"  target="__blank">
        <img src="{{ asset('design/website/img/kauswagan/lgpm_logo.png')}}" height="80" width="80"  alt="Avatar" style="border-radius: 50%;margin: 5px">
      </a>


    </div>

  </div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</section>


@endforeach



@if(count($news) != 0)
<section class="bg--secondary">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-6">
            <h4>News</h4>
          </div>
          <div class="col-6 text-right">
            <a href="{{url('/news')}}">View more</a>
          </div>
        </div>
      </div>
    </div>
    

    <div class="slider" data-paging="true">
      <ul class="slides">
       @foreach($news as $key => $new)
       <li class="col-md-4 col-12">
        <div class="feature feature-1">

          <a href="{{url('/news-detail', $new->id)}}" class="border-round block">
            <img style="object-fit: cover; width: 100%; height: 240px;" alt="Image" src="{{ asset('storage/news_images')}}/{{$new->display_image}}" />
          </a>
          <div class="feature__body boxed boxed--border">
            <h5>{{$new->title}}</h5>

            <div style=" overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
              @php 
              echo strip_tags($story->description);
              @endphp
            </div>

            <a href="news-detail-2.html">
              Read more
            </a>
          </div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</div>
</section>
@endif





@if(count($albums) != 0)
<section class="bg--secondary">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-6">
            <h4>Albums</h4>
          </div>
          <div class="col-6 text-right">
            <a href="{{url('/albums')}}">View more</a>
          </div>
        </div>
      </div>
    </div>
    <div class="slider" data-paging="true">
      <ul class="slides">

        @foreach($albums as $data)
        <li class="col-md-4 col-12">
          <div class="feature feature-1">
            <a href="{{url('/album-detail', $data->album_id)}}" class="block">
              @if($data->display_image != null)
              <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/galleries/albums')}}/{{$data->display_image}}" />
              @else
              <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('design/website/img/albumdefaultimage.png')}}"/>
              @endif  
            </a>
            
            <div class="feature__body boxed boxed--border">
              <h5>{{$data->title}}</h5>
              <h5>
                <?php
                $orig_date =  explode('-', $data->date_input);
                $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                echo date("M d, Y", strtotime($con_date));
                ?>
              </h5>
            </div>
          </div>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</section>
@endif











@if(count($homevideo) != 0)

<section class="text-center imagebg" data-overlay="4">
  <div class="background-image-holder">
    <img alt="background" src="{{ asset('design/website/img/hero-1.jpg')}}" />
  </div>
  <div class="container">
    <div class="row">

      @foreach($homevideo as $key=> $homevid)
      <div class="col-md-12 col-lg-10">
        <h4>{{$homevid->title}}</h4>
        <div class="video-cover border--round">

          {!! $homevid->embed_video !!}

        </div>
        <!--end video cover-->
        <a class="btn btn--primary type--uppercase" href="{{url('/videos')}}">
          <span class="btn__text">
            View More Videos
          </span>
        </a>
      </div>
      @endforeach

    </div>
    <!--end of row-->
  </div>
  <!--end of container-->
</section>


@endif





<section class="space--xs bg--primary">
  <div class="container">
    <div class="cta cta--horizontal text-center-xs row">
      <div class="col-md-4">
        <h4>Uswag Turismo Kauswaganon</h4>
      </div>
      <div class="col-md-5">
        <p class="lead">
          Home of the annual HUGYAW SA KADAGATAN
        </p>
      </div>
      <div class="col-md-3 text-right text-center-xs">
        <a
        class="btn btn--primary type--uppercase"
        href="https://www.facebook.com/uswagturismokauswaganon/"
        >
        <span class="btn__text">
          Learn More
        </span>
      </a>
    </div>
  </div>
</div>
<!--end of container-->
</section>
<div></div>
@endsection


@section('footer')
@endsection
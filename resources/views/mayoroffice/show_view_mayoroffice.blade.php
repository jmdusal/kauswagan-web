@extends('user.header')

@section('title')
View Mayor Data
@endsection

@section('content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		@if($message = Session::get('success'))
		<div class="alert alert-success background-success">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<i class="icofont icofont-close-line-circled text-white"></i>
			</button>
			<strong>{{$message}}</strong>
		</div>
		@endif

		
		@if(count($errors) > 0)
		<div class="alert alert-danger background-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<i class="icofont icofont-close-line-circled text-white"></i>
			</button>
			@foreach($errors->all() as $error)
			<strong>{{$error}}</strong>
			@endforeach
		</div>
		@endif 


		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">
<!-- <h4>User Profile</h4>
	<span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
</div>
</div>
</div>
<div class="col-lg-4">
	<div class="page-header-breadcrumb">
		<!-- <ul class="breadcrumb-title">
			<li class="breadcrumb-item">
				<a href="index.html"> <i class="feather icon-home"></i> </a>
			</li>
			<li class="breadcrumb-item">Mayor Office
			</li>
			<li class="breadcrumb-item">View</li>
		</li>
	</ul> -->
</div>
</div>
</div>
</div>





<div class="page-body">
	<div class="row">
		<div class="col-lg-12">
			<div class="cover-profile">
				<div class="profile-bg-img">
					<img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('storage/mayor_images')}}/{{$mayoroffice->display_image}}" alt="bg-img">

					<div class="card-block user-info">
						<div class="col-md-12">

							<div class="media-body row">
								<div class="col-lg-12">
									<div class="user-title">
										<h1 style="color: white;">{{$mayoroffice->mayor_name}}</h1>
										<h5 style="color: white;">{{$mayoroffice->mini_title}}</h5>
										<br>
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" style="margin-bottom: 10px">Edit Data</button>

									</div>
								</div>
								<div>


								</div> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">

			<div class="tab-header card business-info services m-b-20">
				<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Mayor Info</a>
						<div class="slide"></div>
					</li>

				</li>
			</ul>
		</div>



		<br>

		<div class="tab-content">
			<div class="tab-pane active" id="personal" role="tabpanel">

				<div class="row">
					<div class="col-lg-12">
						<div class="card business-info services m-b-20">
							<div class="card-header">
								<h5 class="card-header-text">Description</h5>

							</div>
							<div class="card-block user-desc">
								<div class="view-desc">
									<p>{!! $mayoroffice->description !!}</p>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>






<form role="form" method="post" action="{{url('/mayor', $mayoroffice->id)}}" enctype="multipart/form-data" class="form_submit">
	{{csrf_field()}}
	<div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg card business-info services m-b-20" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<center>
						<h4 class="modal-title" id="myModalLabel">Edit Data</h4>
					</center>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="icon-list-demo">
							<!-- <i id="icon" class="fa fa-wpbeginner fa-lg"></i> -->
						</div>
					</div>
					<div class="col-md-12">

						<div class="form-group row">

							<div class="col-sm-10">

							</div>
						</div>


						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Mayor Name</label>
							<div class="col-sm-10">
								<input type="text" name="mayor_name" class="form-control" value="{{$mayoroffice->mayor_name}}" required="True" placeholder="Enter Full Name">
							</div>
						</div>


						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Upload Image File</label>
							<div class="col-sm-10">
								<input type="file" name="display_image" class="form-control">
								<img src="{{ asset('storage/mayor_images')}}/{{$mayoroffice->display_image}}" class="img-thumbnail" width="100" />
								<input type="hidden" name="hidden_image" value="{{ $mayoroffice->display_image}}" />
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Mini Title</label>
							<div class="col-sm-10">
								<input type="text" name="mini_title" class="form-control" value="{{$mayoroffice->mini_title}}" required="True" placeholder="Enter Mini Title">
							</div>
						</div>


						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description</label>
							<div class="col-sm-10">
								<textarea name="description" rows="12" cols="7" class="form-control">{{$mayoroffice->description}}</textarea>
							</div>
						</div>



					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-success" value="Edit">
				<button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
			</form>
		</div>
	</div>
</div>
</div>




<script type="text/javascript">

	$(".form_submit").submit(function(e) {

		$('#myModal').modal('hide');

		e.preventDefault();

		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("design/website/img/loading.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);

		setTimeout(function(){

			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){

					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
      },
      success: function(results){

      	console.log(results);

      	if(results.success == true){

      		$('.progress-bar').width('0%').html('');

      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		setTimeout(function(){
      			window.location.href = '{{ url("/mayor/view/1")}}';
      		}, 1000);
      	}else{

      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1000);
      	}


      },
      resetForm: true
  });

		}, 1500);
	});

</script>
@endsection
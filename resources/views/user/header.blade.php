<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title') </title>


<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
  <meta name="author" content="#">

  <link rel="icon" href="{{ asset('logo/kauswagan/kauswagan-logo.png')}}" type="image/gif" sizes="16x16">
  <!-- <link rel="icon" href="{{ asset('adminty/files/assets/images/favicon.ico')}}" type="image/x-icon"> -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

  <!-- HOME DASHBOARD LINKS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/feather/css/feather.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/css/jquery.mCustomScrollbar.css')}}">


  <!-- GALLERIES UPLOAD LINKS -->
  <link href="{{ asset('adminty/files/assets/pages/jquery.filer/css/jquery.filer.css')}}" type="text/css" rel="stylesheet" />
  <link href="{{ asset('adminty/files/assets/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" rel="stylesheet" />

  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('adminty/datatable/dataTables.net-bs/css/dataTables.bootstrap.min.css')}}"> -->

  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/themify-icons/themify-icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/icofont/css/icofont.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/feather/css/feather.css')}}">

  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

  <!-- new add scripted -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>




  <link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" />

  <!-- <script src="https://cdnjs/cloudflare.com/ajax/libs/jquery/3.3.6/jquery.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> -->

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script> -->


  <link rel="stylesheet" type="text/css" href="{{asset('css/ekko-lightbox.css')}}">

  <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.min.css')}}">
  <link href="{{ asset('design/website/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />


<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script> -->

	@yield('stylesheet')


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet"> -->


	<style type="text/css">
		::-webkit-scrollbar {
			-webkit-appearance: none;
			width: 10px;
		}

		::-webkit-scrollbar-thumb {
			border-radius: 5px;
			background-color: rgba(0,0,0,.5);
			-webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
		}
	</style>
</head>
<body>
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
			</div>
		</div>
	</div>

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">
					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="feather icon-menu"></i>
						</a>
						<a href="{{url('/')}}">
							<!-- <img class="img-fluid" src="{{ asset('logo/kauswagan/kauswagan-logo.png')}}" alt="Theme-Logo" /> -->
							<div class="row" style="padding-left: 20px; padding-right: 20px">

								<img class="img-fluid" width="40" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" alt="Theme-Logo" />
								<h5 style='margin-top: 10px; font-family: "Arial Black", Gadget, sans-serif; font-size: 18px'>&nbsp;KAUSWAGAN</h5>
							</div>


							<!-- <img class="img-fluid" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" alt="Theme-Logo"/> -->
						</a>
						<a class="mobile-options">
							<i class="feather icon-more-horizontal"></i>
						</a>
					</div>
					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li class="header-search">
								<div class="main-search morphsearch-search">
									<div class="input-group">
										<!-- <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
										<input type="text" class="form-control">
										<span class="input-group-addon search-btn"><i class="feather icon-search"></i></span> -->
									</div>
								</div>
							</li>
						<!-- 	<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="feather icon-maximize full-screen"></i>
								</a>
							</li> -->
						</ul>
						<ul class="nav-right">
							<li class="header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
										<!-- <i class="feather icon-bell"></i>
											<span class="badge bg-c-pink">5</span> -->
										</div>
										<ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
											<li>
												<h6>Notifications</h6>
												<label class="label label-danger">New</label>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">John Doe</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">Joseph William</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">Sara Soudein</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</li>

								<li class="user-profile header-notification">
									<div class="dropdown-primary dropdown">
										<div class="dropdown-toggle" data-toggle="dropdown">


											<img src="{{ asset('adminty/files/assets/images/unnamed-1.png')}}" class="img-radius" alt="User-Profile-Image">

											<span>{{Auth::user()->name}}</span>
											<i class="feather icon-chevron-down"></i>
										</div>
										<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

											<li>
												<a href="{{url('/change-password')}}">
													<i class="feather icon-settings"></i> Change Password
												</a>
											</li>

											<li>
												<a href="{{ url('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
													<i class="feather icon-log-out"></i> Logout
												</a>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
													{{ csrf_field() }}
												</form>
											</li>


										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</nav>




				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<nav class="pcoded-navbar">
							<div class="pcoded-inner-navbar main-menu">
								<ul class="pcoded-item pcoded-left-item">
									<li class="pcoded-hasmenu">

										<div class="pcoded-navigatio-lavel">Home</div>
										<ul class="pcoded-item pcoded-left-item">

											<li class=" <?php echo $sidebar == "dashboard" ? 'active' : ''?>">
												<a href="{{url('/dashboard')}}">
													<span class="pcoded-micon"><i class="feather icon-home"></i></span>
													<span class="pcoded-mtext">Dashboard</span>
												</a>
											</li>

											<li class=" <?php echo $sidebar == "authors" ? 'active' : ''?>">
												<a href="{{url('/authors/table')}}">
													<span class="pcoded-micon"><i class="feather icon-users"></i></span>
													<span class="pcoded-mtext">Authors</span>
												</a>
											</li>
										</ul>



										<div class="pcoded-navigatio-lavel">Manage</div>
										<ul class="pcoded-item pcoded-left-item">

											<li class=" <?php echo $sidebar == "stories" ? 'active' : ''?>">
												<a href="{{url('/stories/table')}}">
													<span class="pcoded-micon"><i class="feather icon-layers"></i></span>
													<span class="pcoded-mtext">Stories</span>
												</a>
											</li>

											<li class=" <?php echo $sidebar == "news" ? 'active' : ''?>">
												<a href="{{url('/news/table')}}">
													<span class="pcoded-micon"><i class="feather icon-book"></i></span>
													<span class="pcoded-mtext">News</span>
												</a>
											</li>

<!-- <li class="">
<a href="{{url('/blogs/table')}}">
<span class="pcoded-micon"><i class="feather icon-zap"></i></span>
<span class="pcoded-mtext">Blogs</span>
</a>
</li> -->


<!-- <li class="">
<a href="{{url('/activities/table')}}">
<span class="pcoded-micon"><i class="feather icon-activity"></i></span>
<span class="pcoded-mtext">Activities</span>
</a>
</li> -->



<li class="<?php echo $sidebar == "awards" ? 'active' : ''?>">
	<a href="{{url('/awards/table')}}">
		<span class="pcoded-micon"><i class="feather icon-award"></i></span>
		<span class="pcoded-mtext">Awards</span>
	</a>
</li>

<li class="<?php echo $sidebar == "barangays" ? 'active' : ''?>">
	<a href="{{url('/barangays/table')}}">
		<span class="pcoded-micon"><i class="feather icon-map"></i></span>
		<span class="pcoded-mtext">Barangays</span>
	</a>
</li>


<li class="<?php echo $sidebar == "covids" ? 'active' : ''?>">
	<a href="{{url('/covids/table')}}">
		<span class="pcoded-micon"><i class="feather icon-globe"></i></span>
		<span class="pcoded-mtext">Covid</span>
	</a>
</li>

<li class="<?php echo $sidebar == "albums" ? 'active' : ''?>">
	<a href="{{url('/albums/table')}}">
		<span class="pcoded-micon"><i class="feather icon-image"></i></span>
		<span class="pcoded-mtext">Albums</span>
	</a>
</li>




<div class="pcoded-navigatio-lavel">Galleries</div>

<li class="<?php echo $sidebar == "homegalleries" ? 'active' : ''?>">
	<a href="{{url('/homegalleries/table')}}">
		<span class="pcoded-micon"><i class="feather icon-image"></i></span>
		<span class="pcoded-mtext">Photos</span>
	</a>
</li>



<li class="<?php echo $sidebar == "homevideos" ? 'active' : ''?>">
	<a href="{{url('/homevideos/table')}}">
		<span class="pcoded-micon"><i class="feather icon-video"></i></span>
		<span class="pcoded-mtext">Videos</span>
	</a>
</li>

<div class="pcoded-navigatio-lavel">About</div>
<li class="<?php echo $sidebar == "mayoroffices" ? 'active' : ''?>">
	<a href="{{url('/mayor/view', '1')}}">
		<span class="pcoded-micon"><i class="feather icon-droplet"></i></span>
		<span class="pcoded-mtext">Mayor's Office</span>
	</a>
</li>


<li class="<?php echo $sidebar == "directories" ? 'active' : ''?>">
	<a href="{{url('/directories/table')}}">
		<span class="pcoded-micon"><i class="feather icon-phone"></i></span>
		<span class="pcoded-mtext">Directories</span>
	</a>
</li>


<li class="<?php echo $sidebar == "histories" ? 'active' : ''?>">
	<a href="{{url('/histories/table')}}">
		<span class="pcoded-micon"><i class="feather icon-flag"></i></span>
		<span class="pcoded-mtext">Histories</span>
	</a>
</li>

<div class="pcoded-navigatio-lavel">Homepage</div>

<li class="<?php echo $sidebar == "homewelcome" ? 'active' : ''?>">
	<a href="{{url('/homewelcome/view', '1')}}">
		<span class="pcoded-micon"><i class="feather icon-aperture"></i></span>
		<span class="pcoded-mtext">Welcome</span>
	</a>
</li>

<li class="<?php echo $sidebar == "homewelcome" ? 'active' : ''?>">
	<a href="{{url('/homewelcome/view', '2')}}">
		<span class="pcoded-micon"><i class="feather icon-aperture"></i></span>
		<span class="pcoded-mtext">Uswag Turismo</span>
	</a>
</li>


<li class="<?php echo $sidebar == "mails" ? 'active' : ''?>">
	<a href="{{url('/contact_us')}}">
		<span class="pcoded-micon"><i class="feather icon-image"></i></span>
		<span class="pcoded-mtext">Send Mail</span>
	</a>
</li>




</ul>
<!-- <div class="pcoded-navigatio-lavel">Support</div>
<ul class="pcoded-item pcoded-left-item">
<li class="">
<a href="{{url('contact_us')}}">
<span class="pcoded-micon"><i class="feather icon-help-circle"></i></span>
<span class="pcoded-mtext">Submit Issue</span>
</a>
</li>
</ul>
</div> -->
</nav>


<div class="main-content" style="">
	@yield('content')
</div>

<script data-cfasync="false" src="{{ asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/modernizr/js/modernizr.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/chart.js/js/Chart.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/widget/amchart/light.js')}}"></script>
<script src="{{ asset('adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/js/SmoothScroll.js')}}"></script>
<script src="{{ asset('adminty/files/assets/js/pcoded.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/js/vartical-layout.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/dashboard/custom-dashboard.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/js/script.min.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script type="text/javascript" src="{{ asset('adminty/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>



<script src="{{ asset('adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/data-table/js/jszip.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/data-table/js/data-table-custom.js')}}"></script>



<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next/js/i18next.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>




<script type="text/javascript" src="{{ asset('adminty/files/assets/js/script.js')}}"></script>




<script src="{{ asset('adminty/files/assets/pages/wysiwyg-editor/js/tinymce.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/wysiwyg-editor/wysiwyg-editor.js')}}"></script>



<!-- <script src="{{ asset('alert/sweetalert2.all.min.js')}}"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script> -->

<!-- <script src="{{URL::asset('assets/js/sweetalert2.all.min.js')}}"></script> -->


<!-- 
<script src="{{ asset('alert/sweetalert2.all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->




<script src="{{ asset('javascripts/base_url.js')}}"></script>




<script src="{{ asset('adminty/files/assets/pages/jquery.filer/js/jquery.filer.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/filer/custom-filer.js')}}" type="text/javascript"></script>
<script src="{{ asset('adminty/files/assets/pages/filer/jquery.fileuploads.init.js')}}" type="text/javascript"></script>


<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/icon-modal.js')}}"></script>


<script src="{{ asset('adminty/files/assets/pages/ckeditor/ckeditor.js')}}"></script>

<script src="{{ asset('adminty/files/assets/pages/chart/echarts/js/echarts-all.js')}}" type="text/javascript"></script>


<script src="{{asset('js/ekko-lightbox.min.js')}}"></script>

<script src="{{asset('js/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('design/website/js/lightbox.min.js')}}"></script>
<script src="{{asset('js/jquery.form.min.js')}}"></script>




<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-23581568-13');
</script>




</body>
</html>

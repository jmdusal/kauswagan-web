<!-- BODY START -->
@extends('user.header')

@section('title')
Admin Dashboard
@endsection

@section('stylesheet')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

<style>
/*.gallery_image{
	object-fit: contain;
	width: 100%;
	height: 450px;

}
.gallery{
	background-color: black;
	}*/

	.splide__slide img {
		width : 100%;
		height: auto;
	}


	.overlay_container { 
		position: relative; 
		width: 100%; /* for IE 6 */
	}

	.overlay_text { 
		position: absolute; 
		top: 200px; 
		left: 10px; 
		width: 100%; 
	}
	.overlay_text2 { 
		position: absolute; 
		top: 250px; 
		left: 10px; 
		width: 100%; 
	}
</style>
@endsection

@section('content')

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-body">
					<div class="row">



 					<!--	<div class="col-sm-3">
							<a href="{{url('/authors/table')}}">
								<div class="card bg-c-yellow text-white widget-visitor-card">
									<div class="card-block-small text-center">
										
										<h2>
											@if($authors > 1) 
											{{$authors}}&nbsp;Authors

											@elseif($authors == 0)
											No Author

											@else
											{{$authors}}&nbsp;Author

											@endif

										</h2>

										<h6>In Page Authors</h6>
										<p>As of&nbsp;<?php echo date('m-d-Y'); ?></p>
										<i class="feather icon-users"></i>
									</div>
								</div>
							</a>
						</div> -->

						<div class="col-sm-4">
							<a href="{{url('/stories/table')}}">
								<div class="card bg-c-green text-white widget-visitor-card business-info services m-b-20">
									<div class="card-block-small text-center">
										
										<h2>
											@if($stories > 1) 
											{{$stories}}&nbsp;Stories

											@elseif($stories == 0)
											No Story

											@else
											{{$stories}}&nbsp;Story

											@endif
										</h2>

										<h6>In Page Stories</h6>
										<p>As of&nbsp;<?php echo date('Y-d-m'); ?></p>
										<i class="feather icon-layers"></i>
									</div>
								</div>
							</a>
						</div>

						<div class="col-sm-4">
							<a href="{{url('/news/table')}}">
								<div class="card bg-c-pink text-white widget-visitor-card business-info services m-b-20">
									<div class="card-block-small text-center">

										<h2>
											@if($news > 1) 
											{{$news}}&nbsp;News

											@elseif($news == 0)
											No News

											@else
											{{$news}}&nbsp;New

											@endif

										</h2>

										<h6>In Page News</h6>
										<p>As of&nbsp;<?php echo date('Y-d-m'); ?></p>
										<i class="feather icon-book"></i>
									</div>
								</div>
							</a>
						</div>




						<div class="col-sm-4">
							<a href="{{url('/awards/table')}}">
								<div class="card bg-c-blue text-white widget-visitor-card business-info services m-b-20">
									<div class="card-block-small text-center">

										<h2>
											@if($awards > 1) 
											{{$awards}}&nbsp;Awards

											@elseif($awards == 0)
											No Award

											@else
											{{$awards}}&nbsp;Award

											@endif
										</h2>

										<h6>In Page Awards</h6>
										<p>As of&nbsp;<?php echo date('Y-d-m'); ?></p>
										<i class="feather icon-award"></i>
									</div>
								</div>
							</a>
						</div>





















































						<div class="col-md-4">
							<div class="col-md-12">
								<div class="card business-info services m-b-20">
									<div class="card-block text-center">
										<i class="feather icon-image text-c-green d-block f-40"></i>
										<h4 class="m-t-20">
											@if(count($albums) > 1) 
											{{count($albums)}}&nbsp;Albums

											@elseif(count($albums) == 0)
											No photos

											@else
											{{count($albums)}}&nbsp;Album

											@endif


										</h4>
										<p class="m-b-20">As of&nbsp;<?php echo date('Y-d-m'); ?></p>
										<a href="{{url('/homegalleries/table')}}" class="btn btn-success btn-sm btn-round">Check them out</a>
									</div>
								</div>
							</div>

							<div class="col-md-12">		
								<div class="card business-info services m-b-20">
									<div class="card-block text-center">
										<i class="feather icon-video text-c-pink d-block f-40"></i>
										<h4 class="m-t-20">
											@if($homevideos > 1) 
											{{$homevideos}}&nbsp;Videos

											@elseif($homevideos == 0)
											No Videos

											@else
											{{$homevideos}}&nbsp;Video

											@endif
										</h4>
										<p class="m-b-20">As of&nbsp;<?php echo date('m-d-Y'); ?></p>
										<a href="{{url('/homevideos/table')}}" class="btn btn-danger btn-sm btn-round">Check them out</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>




		</div>
	</div>
</div>
</div>
</div>
</div>
</div>


<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

<script type="text/javascript">
	// new Splide( '.splide', {
	// 	type  : 'fade',
	// 	rewind: true,
	// } ).mount();

	document.addEventListener( 'DOMContentLoaded', function () {
		var secondarySlider = new Splide( '#secondary-slider', {
			fixedWidth  : 100,
			height      : 60,
			gap         : 10,
			cover       : true,
			isNavigation: true,
			focus       : 'center',
			breakpoints : {
				'600': {
					fixedWidth: 66,
					height    : 40,
				}
			},
		} ).mount();

		var primarySlider = new Splide( '#primary-slider', {
			type       : 'fade',
			heightRatio: 0.5,
			pagination : false,
			arrows     : false,
			cover      : true,
	} ); // do not call mount() here.

		primarySlider.sync( secondarySlider ).mount();
	} );
</script>
@endsection
@extends('user.header')

@section('title')
View Award
@endsection

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        @if(count($errors) > 0)
        <div class="alert alert-danger background-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="icofont icofont-close-line-circled text-white"></i>
            </button>
            @foreach($errors->all() as $error)
            <strong>{{$error}}</strong>
            @endforeach
        </div>
        @endif
        
        @if($message = Session::get('success'))
        <div class="alert alert-success background-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="icofont icofont-close-line-circled text-white"></i>
            </button>
            <strong>{{$message}}</strong>
        </div>
        @endif

        
        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">

                            </div>
                        </div>
                    </div>
                </div>


                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover-profile">
                               <div class="profile-bg-img">
                                <img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('storage/awards_images')}}/{{$award->display_image}}" alt="bg-img">
                                <div class="card-block user-info">
                                    <div class="col-md-12">

                                        <div class="media-body row">
                                            <div class="col-lg-12">
                                                <div class="user-title">
                                                    <h1 style="color: white;">{{$award->title}}</h1>
                                                    <h5 style="color: white;">{{$award->date_award_taken}}</h5>

                                                </div>
                                            </div>
                                            <div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="tab-header card business-info services m-b-20">
                            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Award Info</a>
                                    <div class="slide"></div>
                                </li>

                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card business-info services m-b-20">
                                <div class="card-header">
                                    <h5 class="card-header-text">Details</h5>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <div class="table-responsive">
                                                                <table class="table m-0">
                                                                    <tbody>

                                                                        <tr>
                                                                            <th scope="row">Title</th>
                                                                            <td>{{$award->title}}</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <th scope="row">Link</th>
                                                                            <td>{{$award->link}}</td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-xl-6">
                                                            <div class="table-responsive">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <th scope="row">Date During the Award</th>
                                                                        <td>{{$award->date_award_taken}}</td>
                                                                    </tr>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="edit-info">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="general-info">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="card business-info services m-b-20">
                                    <div class="card-header">
                                        <h5 class="card-header-text">Description</h5>

                                    </div>
                                    <div class="card-block user-desc">
                                        <div class="view-desc">
                                            <p>{!! $award->description !!}</p>
                                        </div>

                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

@endsection
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Site Description Here">
	<link href="{{ asset('design/website/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/stack-interface.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/flickity.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/iconsmind.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/jquery.steps.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/theme-greensea.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('design/website/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="icon" href="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" type="image/gif" sizes="16x16">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
       <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
       	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script> -->
       	<!-- Styles -->

       	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
       	@yield('stylesheet')
       </head>

       <body class=" ">
       	<a id="start"></a>
       	<div class="nav-container">
       		<div class="bar bar--sm visible-xs">
       			<div class="container">
       				<div class="row">
       					<div class="col-3 col-md-2">
       						<a href="index.html">
       							<img class="logo logo-dark" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}"/>
       							<img class="logo logo-light" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}"/>
       						</a>
       					</div>
       					<div class="col-9 col-md-10 text-right">
       						<a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
       							<i class="icon icon--sm stack-interface stack-menu"></i>
       						</a>
       					</div>
       				</div>
       				<!--end of row-->
       			</div>
       			<!--end of container-->
       		</div>
       		<!--end bar-->



       		<section class="bar bar-3 bar--sm bg--primary">
       			<div class="container">
       				<div class="row">
       					<div class="col-lg-4">
       						<div class="bar__module">
       							<ul class="social-list list-inline list--hover">
       								<li>
       									<a href="https://twitter.com/KauswaganOf" target="__blank">
       										<i class="socicon socicon-twitter icon icon--xs"></i>
       									</a>
       								</li>
       								<li>
       									<a href="https://www.facebook.com/kauswaganmunicipality/" target="__blank">
       										<i class="socicon socicon-facebook icon icon--xs"></i>
       									</a>
       								</li>
       								<li>
       									<a href="https://www.instagram.com/kauswaganldn/?hl=en" target="__blank">
       										<i class="socicon socicon-instagram icon icon--xs"></i>
       									</a>
       								</li>
       								<li>
       									<a
       									href="https://www.youtube.com/channel/UC0FbjU1z8atX5quiBxxKfpQ" target="__blank"
       									>
       									<i class="socicon socicon-youtube icon icon--xs"></i>
       								</a>
       							</li>
       						</ul>
       					</div>
       				</div>
       				<div class="col-lg-4 text-center">
       					<span class="type--fade">Municipality of Kauswagan</span>
       				</div>
       				<div class="col-lg-4 text-right">
       					@if (Auth::guest())
       					<span class="type--fade"><a href="{{url('/login')}}">Login</a></span>
       					@else
       					<span class="type--fade"><a href="{{url('/dashboard')}}">Back to dashboard</a></span>
       					@endif
       				</div>
       			</div>
       			<!--end of row-->
       		</div>
       		<!--end of container-->
       	</section>
       	<!--end bar-->

       	<nav
       	id="menu6"
       	class="bar bar--xs bar-1 hidden-xs"
       	data-scroll-class="50vh:pos-fixed"
       	>
       	<div class="container">
       		<div class="row">
       			<div class="col-lg-1 col-md-2 hidden-xs">
       				<div class="bar__module">
       					<a href="{{url('/')}}">
       						<img class="logo logo-dark" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}"/>
       						<img class="logo logo-light" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}"/>
       					</a>
       				</div>
       				<!--end module-->
       			</div>
       			<div
       			class="col-lg-10 col-md-12 text-center text-left-xs text-left-sm mt-3 mb-4"
       			>
       			<div class="bar__module">
       				<ul class="menu-horizontal text-left">
       					<li class="dropdown">
       						<a href="{{url('/')}}">
       							<span>Home</span>
       						</a>
       					</li>
       				</ul>
       			</div>
       			<div class="bar__module">
       				<ul class="menu-horizontal text-left">
       					<li class="dropdown">
       						<span class="dropdown__trigger">About</span>
       						<div class="dropdown__container">
       							<div class="container">
       								<div class="row">
       									<div class="dropdown__content col-lg-2 col-md-4">
       										<ul class="menu-vertical">
       											<li>
       												<a href="{{url('/office-mayor')}}">
       													<span>Office of the Mayor</span>
       												</a>
       											</li>
<!--                             <li>
                              <a href="{{url('/organizational-chart')}}">
                                <span>Organizational Chart</span>
                              </a>
                          </li> -->
                          <li>
                          	<a href="{{url('/directory')}}">
                          		<span>Directories</span>
                          	</a>
                          </li>
                          <li>
                          	<a href="{{url('/barangays')}}">
                          		<span>Barangays</span>
                          	</a>
                          </li>
                          <li>
                          	<a href="{{url('/history')}}">
                          		<span>History</span>
                          	</a>
                          </li>
                      </ul>
                  </div>
                  <!--end dropdown content-->
              </div>
              <!--end row-->
          </div>
      </div>
      <!--end dropdown container-->
  </li>
</ul>
</div>
<div class="bar__module">
	<ul class="menu-horizontal text-left">
		<li class="dropdown">
			<span class="dropdown__trigger">Stories & News</span>
			<div class="dropdown__container">
				<div class="container">
					<div class="row">
						<div class="dropdown__content col-lg-2 col-md-4">
							<ul class="menu-vertical">
								<li class="dropdown">
									<span class="dropdown__trigger">Articles</span>
									<div class="dropdown__container">
										<div class="container">
											<div class="row">
												<div
												class="dropdown__content col-lg-2 col-md-4"
												>
												<ul class="menu-vertical">
													<li>
														<a href="{{url('/stories')}}">
															Stories
														</a>
													</li>
													<li>
														<a href="{{url('/news')}}">
															News
														</a>
													</li>
												</ul>
											</div>
											<!--end dropdown content-->
										</div>
										<!--end row-->
									</div>
								</div>
								<!--end dropdown container-->
							</li>
							<li class="dropdown">
								<span class="dropdown__trigger">Gallery</span>
								<div class="dropdown__container">
									<div class="container">
										<div class="row">
											<div
											class="dropdown__content col-lg-2 col-md-4"
											>
											<ul class="menu-vertical">
												<li>
													<a href="{{url('/albums')}}">
														Photos
													</a>
												</li>
												<li>
													<a href="{{url('/videos')}}">
														Videos
													</a>
												</li>
											</ul>
										</div>
										<!--end dropdown content-->
									</div>
									<!--end row-->
								</div>
							</div>
							<!--end dropdown container-->
						</li>
					</ul>
				</div>
				<!--end dropdown content-->
			</div>
			<!--end row-->
		</div>
	</div>
	<!--end dropdown container-->
</li>
</ul>
</div>
<div class="bar__module">
	<ul class="menu-horizontal text-left">
		<li class="dropdown">
			<a href="{{url('/bids-and-awards')}}">
				<span>Bids & Awards</span>
			</a>
		</li>
	</ul>
</div>
<div class="bar__module">
	<ul class="menu-horizontal text-left">
		<li class="dropdown">
			<a href="https://www.minbits.com/kauswagan-services/index.html">
				<span>Office & Services</span>
			</a>
		</li>
	</ul>
</div>
<div class="bar__module">
	<ul class="menu-horizontal text-left">
		<li class="dropdown">
			<!-- <a href="http://www.covid19.gov.ph/"> -->
				<a href="{{url('/covid-updates')}}">
					
					<!-- <a href="covid-updates.html"> -->
						<span>COVID-19 Updates</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="bar__module">
			<ul class="menu-horizontal text-left">
				<li class="dropdown">
					<a href="{{url('/transparency')}}">
						<span>Transparency</span>
					</a>
				</li>
			</ul>
		</div>
		<!--end module-->
	</div>
	<div class="col-lg-1 col-md-2 hidden-xs">
		<div class="bar__module">
			<a href="{{url('/transparency')}}">
				<img class="logo logo-dark" alt="Transparency Seal" src="{{ asset('design/website/img/kauswagan/logo-transparency-seal.png')}}" />
				<img class="logo logo-light" alt="Transparency Seal" src="{{ asset('design/website/img/kauswagan/logo-transparency-seal.png')}}" />
			</a>
		</div>
		<!--end module-->
	</div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</nav>
<!--end bar-->
</div>










<div class="main-content">
	@yield('content')
</div>



<footer class="space--sm footer-2 bg--secondary ">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h6 class="type--uppercase">Republic of the Philippines</h6>
				<ul class="list--hover">
					<ul class="list--hover">
						<li>
							<p>All content is in the public domain unless otherwise stated.</p>
						</li>
					</ul>
				</ul>
			</div>
			<div class="col-md-3">
				<h6 class="type--uppercase">Municipality of Kauswagan</h6>
				<ul class="list--hover">
					<li>
						<p>Learn more about the Municipality of Kauswagan, its structure, how government works and the people behind it.</p>
					</li>
				</ul>
			</div>
			<div class="col-md-3">
				<h6 class="type--uppercase">Government Links</h6>
				<ul class="list--hover">
					<li>
						<a href="https://www.bir.gov.ph/">BIR</a>
					</li>
					<li>
						<a href="https://www.dilg.gov.ph/">DILG</a>
					</li>
					<li>
						<a href="https://www.dswd.gov.ph/">DSWD</a>
					</li>
					<li>
						<a href="https://www.dpwh.gov.ph/">DPWH</a>
					</li>
					<li>
						<a href="https://www.deped.gov.ph/">DepEd</a>
					</li>
					<li>
						<a href="https://www.doh.gov.ph/">DOH</a>
					</li>
					<li>
						<a href="https://ncr.denr.gov.ph/">DENR</a>
					</li>
					<li>
						<a href="https://www.da.gov.ph/">DA</a>
					</li>
					<li>
						<a href="https://www.gov.ph/">GOV</a>
					</li>
				</ul>
			</div>
			<div class="col-md-3">
				<h6 class="type--uppercase">Quick Links</h6>
				<ul class="list--hover">
					<li>
						<a href="{{url('/office-mayor')}}">Office of the Mayor</a>
					</li>
					<li>
						<a href="/organizational-chart">Organizational Chart</a>
					</li>
					<li>
						<a href="{{url('/directory')}}">Directories</a>
					</li>
					<li>
						<a href="{{url('/barangays')}}">Barangays</a>
					</li>
					<li>
						<a href="{{url('/history')}}">History</a>
					</li>
					<li>
						<a href="{{url('/stories')}}">Stories</a>
					</li>
					<li>
						<a href="{{url('/news')}}">News</a>
					</li>
					<li>
						<a href="{{url('/gallery')}}">Photos</a>
					</li>
					<li>
						<a href="{{url('/videos')}}">Videos</a>
					</li>
					<li>
						<a href="/bids-and-awards">Bids & Awards</a>
					</li>
					<li>
						<a href="https://www.minbits.com/kauswagan-services/index.html">Office & Services</a>
					</li>
					<li>
						<a href="http://www.covid19.gov.ph/">COVID-19 Updates</a>
					</li>
					<li>
						<a href="{{url('/transparency')}}">Transparency</a>
					</li>
				</ul>
			</div>
		</div>
		<!--end of row-->
		<div class="wrapper wrapper-3">
			<div class="container">
				<div class="row">
					<div class="col widget-area">
						<div class="widget widget-html widget-ph">
							<div class="widget-content">
								<p class="text-center mb-2">
									<img width="80" src="{{ asset('design/website/img/kauswagan/coat-of-arms-ph-cl.svg')}}" alt="Coat of Arms of Philippines"></p>
									<p class="text-center mb-0">REPUBLIC OF THE PHILIPPINES</p>
									<p class="text-center small mb-0">All content is in the public domain unless otherwise stated.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<span class="type--fine-print">&copy;
						<span class="update-year"></span> Municipality of Kauswagan</span>
						<a class="type--fine-print" href="#">Privacy Policy</a>
						<a class="type--fine-print" href="#">Legal</a>
					</div>

					<div class="col-md-4" align="center">
						<span class="type--fine-print">Powerd by <a href="http://minbits.com" target="__blank">Minbits Corp.</a> & <a href="https://www.facebook.com/HRS-Media-News-Information-100359098050123" target="__blank">HRS Media</a></span>
					</div>
					<div class="col-md-4 text-right text-left-xs">
						<ul class="social-list list-inline list--hover">
							<li>
								<a href="https://twitter.com/KauswaganOf">
									<i class="socicon socicon-twitter icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="https://www.facebook.com/kauswaganmunicipality/">
									<i class="socicon socicon-facebook icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="https://www.instagram.com/kauswaganldn/?hl=en">
									<i class="socicon socicon-instagram icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UC0FbjU1z8atX5quiBxxKfpQ">
									<i class="socicon socicon-youtube icon icon--xs"></i>
								</a>
							</li>
						</ul>
					</div>

				</div>

				<!--end of row-->
			</div>
			<!--end of container-->
		</footer>
	</div>




	<div class="main-content">
		@yield('footer')
	</div>
        <!--<div clas
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        	<i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ asset('design/website/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{ asset('design/website/js/flickity.min.js')}}"></script>
        <script src="{{ asset('design/website/js/easypiechart.min.js')}}"></script>
        <script src="{{ asset('design/website/js/parallax.js')}}"></script>
        <script src="{{ asset('design/website/js/typed.min.js')}}"></script>
        <script src="{{ asset('design/website/js/datepicker.js')}}"></script>
        <script src="{{ asset('design/website/js/isotope.min.js')}}"></script>
        <script src="{{ asset('design/website/js/ytplayer.min.js')}}"></script>
        <script src="{{ asset('design/website/js/lightbox.min.js')}}"></script>
        <script src="{{ asset('design/website/js/granim.min.js')}}"></script>
        <script src="{{ asset('design/website/js/jquery.steps.min.js')}}"></script>
        <script src="{{ asset('design/website/js/countdown.min.js')}}"></script>
        <script src="{{ asset('design/website/js/twitterfetcher.min.js')}}"></script>
        <script src="{{ asset('design/website/js/spectragram.min.js')}}"></script>
        <script src="{{ asset('design/website/js/smooth-scroll.min.js')}}"></script>
        <script src="{{ asset('design/website/js/scripts.js')}}"></script>
    </body>
    </html>

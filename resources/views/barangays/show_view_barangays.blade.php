@extends('user.header')

@section('title')
View Barangay
@endsection

@section('stylesheet')
<style type="text/css">
.container {
  position: relative;
  width: 35%;
}

.img-thumbnail {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container:hover .image {
  opacity: 0.3;
}

.container:hover .middle {
  opacity: 1;
}

.text {
  background-color: #FFFFFF;
  color: white;
  font-size: 20px;
  padding: 4px;
}

.ul{

  color: black;
  font-weight: bolder;
  font-size: 20px;
  padding: 1px;
}

.table td{
  max-width: 200px; 
  min-width: 70px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

}

</style>
@endsection

@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      @foreach($errors->all() as $error)
      <strong>{{$error}}</strong>
      @endforeach
    </div>
    @endif

    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong>{{$message}}</strong>
    </div>
    @endif

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">

              </div>
            </div>
          </div>
        </div>


        <div class="page-body">
          <div class="row">
            <div class="col-lg-12">
              <div class="cover-profile">
               <div class="profile-bg-img">
                <a href="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}" data-toggle="lightbox" data-gallery="sample">

                  <img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}" alt="bg-img">
                </a>

                <div class="card-block user-info">
                  <div class="col-md-12">

                    <div class="media-body row">
                      <div class="col-lg-12">
                        <div class="user-title">
                          <h1 style="color: white;">{{$barangay->barangay_name}}</h1>

                        </div>
                      </div>
                      <div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">

            <div class="tab-header card business-info services m-b-20">
              <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Barangay Info</a>
                  <div class="slide"></div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Barangay Officials</a>
                  <div class="slide"></div>
                </li>
              </ul>
            </div>

            <div class="tab-content">
              <div class="tab-pane active" id="personal" role="tabpanel">

                <div class="card business-info services m-b-20">
                  <div class="card-header">
                    <h5 class="card-header-text">Details</h5>
                  </div>
                  <div class="card-block">
                    <div class="view-info">
                      <div class="row">
                        <div class="col-lg-12">

                          <div class="general-info">
                            <div class="row">
                              <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                  <table class="table m-0">
                                    <tbody>

                                      <tr>
                                        <th scope="row">Barangay Name</th>

                                        <td>{{$barangay->barangay_name}}</td>

                                      </tr>
                                      <tr>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>

                              <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                  <table class="table">
                                    <tbody>
                                      <!-- <th scope="row">Date Created</th> -->
                                      <td>

                                      </td>
                                    </tr>

                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="edit-info">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="general-info">
                          <div class="row">
                            <div class="col-lg-6">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="card business-info services m-b-20">
                    <div class="card-header">
                      <h5 class="card-header-text">Description</h5>

                    </div>
                    <div class="card-block user-desc">
                      <div class="view-desc">
                        <p>{!! $barangay->description !!}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="binfo" role="tabpanel">

              <div class="card business-info services m-b-20">
                <div class="card-header">
                  <h1 class="card-header-text">Barangay Officials</h1><br><br>
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Barangay Official</button>
                </div>

                <div class="card-block">
                  <div class="row">
                    <div class="col-md-12">
                      <!-- <div class="card business-info services m-b-20"> -->
                        <div class="card-header">
                          <div class="service-header">
                            <a href="#">

                            </a>
                          </div>

                        </div>
                        <div class="card-block">
                          <div class="row">


                            <div class="col-md-12" align="center">



                              <div class="page-body">
                                <div class="card">
                                  <div class="card-header">
                                    <h2>Barangay Officials</h2>
                                  </div>
                                  <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                      <table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
                                        <thead>
                                          <tr>
                                            <th width="1%">Image</th> 
                                            <th>Barangay Name</th>
                                            <th>Description</th>

                                            <th class="text-center">Action</th>
                                            <!-- <th>Delete</th> -->
                                          </tr>
                                        </thead>
                                        <tbody>

                                          @foreach($barangay->barangaysofficials as $data)
                                          <tr>
                                            <td>
                                              <a href="{{ asset('storage/barangay_officials_images')}}/{{$data->display_image}}" data-lightbox="gallery">
                                                <img src="{{ asset('storage/barangay_officials_images')}}/{{$data->display_image}}" class="img-thumbnail" width="200">
                                              </a>
                                            </td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->position}}</td>

                                            <td class="text-center">
                                              <button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="feather icon-chevron-down"></i></button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/barangayofficial/edit', $data->id)}}" class="btn btn-success">Edit</a>

                                                <a onclick="deleteConfirmation({{$data->id}})" style="color:black; font-weight: bold;" class="dropdown-item">Delete</a>
                                              </div>
                                            </td>
                                          </tr>

                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<form role="form" method="post" action="{{url('/barangayofficials/submit')}}" enctype="multipart/form-data" class="form_submit" data-value="{{$barangay->id}}">
  {{csrf_field()}}
  <div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg card business-info services m-b-20" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <center>
            <h4 class="modal-title" id="myModalLabel">Add Barangay Official</h4>
          </center>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="icon-list-demo">
            </div>
          </div>
          <div class="col-md-12">

            <div class="form-group row">
              <div class="col-sm-10">
                <input type="hidden" name="barangay_id" value="{{$barangay->id}}" class="form-control">
                <input name="invisible" type="hidden" value="secret">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" placeholder="Enter Name" required="true">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Position</label>
              <div class="col-sm-10">
                <input type="text" name="position" class="form-control" placeholder="Enter Position" required="true">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Upload Image</label>
              <div class="col-sm-10">
                <input type="file" name="display_image" class="form-control">
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="create" class="btn btn-success">Send</button>
        <button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
      </form>
    </div>
  </div>
</div>
</div>




<script type="text/javascript">
  $(".form_submit").submit(function(e) {
    $('#myModal').modal('hide');

    e.preventDefault();

    swal({
      html:   '<div class="loader-block">'+
      '<img src="'+'{{ asset("design/website/img/loading.gif")}}'+'" width="150" height="150">'+
      '</div>'+
      '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
      '<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
      allowOutsideClick: false,
      showConfirmButton:false,
    });

    setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
    setTimeout(function(){

      $('.form_submit').ajaxSubmit({
        beforeSubmit: function(){
          $('.progress-bar').width('0%')
        },
        uploadProgress: function(event, position, total, percentComplete){

          $('#progress-status').text(percentComplete+'%');
        },            
        error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
            title: "Error!",
            text: "You`ve insert wrong file type! Please upload an image file!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);
          
          console.log(xhr.responseText);
        },
        success: function(results){

          console.log(results);

          if(results.success == true){

            $('.progress-bar').width('0%').html('');

            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });

            var id = $('.form_submit').attr('data-value');
            setTimeout(function(){
              window.location.href = '{{ url("/barangays/view")}}/'+id;
            }, 1000);
          }else{

            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1000);
          }

        },
        resetForm: true
      });

    }, 1500);
  });
</script>



<script type="text/javascript">
  function deleteConfirmation(id) {
    swal({
      title:"Are you sure you want to delete this?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: "Cancel",
      confirmButtonText: "Yes, delete it!",
    }).then(function (e) {

      if (e.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'POST',
          url: "{{url('/deletebarangayofficial')}}/" + id,
          data: {_token: CSRF_TOKEN},
          dataType: 'JSON',
          error: function (xhr, status, errorThrown) {
              //Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
            },
            success: function (results) {
              if (results.success === true) {
                swal({
                  title: "Done!",
                  text: results.message,
                  type: "success",
                  showConfirmButton: false
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);

              } else {
                swal({
                  title: "Error!",
                  text: results.message,
                  type: "error"
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);           
              }
            }
          });

      } else {
        e.dismiss;
      }

    }, function (dismiss) {
      return false;
    })
  }
</script>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
@endsection
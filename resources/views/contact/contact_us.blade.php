@extends('user.header')

@section('title')
Send Email to Devs
@endsection

@section('content')

<div class="pcoded-content">
  <div class="pcoded-inner-content">

    @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      @foreach($errors->all() as $error)
      <strong>{{$error}}</strong>
      @endforeach
    </div>
    @endif

    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong>{{$message}}</strong>
    </div>
    @endif

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">


                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                    <a href="index.html"> <i class="feather icon-home"></i> </a>
                  </li>
                  <li class="breadcrumb-item"><a href="#!">Form Components</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#!">Form Components</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>


        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="card">
                <div class="card-header">

                  <div class="card-header-right">

                  </div>
                </div>

                <div class="card-block">
                  <h4 class="sub-title">Send Message to Developer for Any Concern</h4>
                  <form id="contact_form" method="post" action="{{url('contact_us/submit')}}" enctype="multipart/form-data">
                    {{csrf_field()}}


                    <input type="hidden" name="name" class="form-control" value="{{Auth::user()->name}}" id="name">


    <!-- <div class="form-group row">
        <label class="col-sm-2 col-form-label">Developer E-mail</label>
        <div class="col-sm-10">
          <input type="text" name="email" class="form-control" value="stackoverflowjm@gmail.com" id="email" required="True"> -->
          <input name="email" class="form-control" value="stackoverflowjm@gmail.com" id="email" hidden>
 <!--        </div>
 </div> -->

 <div class="form-group row">
  <label class="col-sm-2 col-form-label">Message</label>
  <div class="col-sm-10">
    <textarea name="message" class="form-control" id="message" cols="71" rows="6"></textarea>
  </div>
</div>





<button type="create" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</div>
</div>

<div id="styleSelector">
</div>


</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

<script>
  $(document).ready(function()
  {
    if($("#contact_form").length > 0)
    {
      $('#contact_form').validate({
        rules:{
          name : 
          {
            required : true,
            maxlength : 50
          },
          email : 
          {
            required : true,
            maxlength : 50,
            email : true
          },
          message : 
          {
            required : true,
            minlength : 50,
            maxlength : 500
          }
        },
        message : 
        {
          name : 
          {
            required : 'Enter Name Detail',
            maxlength : 'Name should not be more than 50 character'
          },
          email : 
          {
            required : 'Enter Email Detail',
            email : 'Enter Valid Email Detail',
            maxlength : 'Email should not be more than 50 character'
          },
          message : 
          {
            required : 'Enter Message Detail',
            minlength : 'Message Detail must be minimum 50 character long',
            maxlength : 'Message Detail must be maximum 500 character long'
          }
        }
      });
    }
  });
</script>

@endsection
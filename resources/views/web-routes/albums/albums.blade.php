@extends('welcome')

@section('title')
Municipality of Kauswagan - Albums
@endsection

@section('content')
<div class="main-container">

	<section class="height-100 imagebg parallax" data-overlay="5">
		<div class="background-image-holder">
			<img alt="background" src="{{ asset('design/website/img/gallery_cover.jpg')}}" />
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-md-12" align="center">
					<h1>Kauswagan Albums</h1>
				</div>
			</div>
		</div>
	</section>





	@if(count($albums_paginate) != 0)
	<section>
		<div class="container">
			<form method="POST" action="{{url('/albums/search')}}" class="form--horizontal row m-0">
				{{ csrf_field() }}
				<div class="col-md-8">
					<input type="text" name="q" placeholder="Type search keywords here" required="true"/>
				</div>
				<div class="col-md-4">
					<button type="submit" class="btn btn--primary type--uppercase">Search</button>
				</div>
			</form>
			@if(isset($details))
			<p>The Search results for your query <br>
				<b> {{$query}} </b> are:</p>
				@endif

				<hr>

				<div class="row">
					<div class="col-md-12">
						<div class="masonry">
							<div class="masonry__container row">
								<div class="masonry__item col-lg-4 col-md-6"></div>

								@foreach($albums_paginate as $key => $album_paginate)
								<div class="masonry__item col-lg-4 col-md-6" data-masonry-filter="Marketing">
									<article class="feature feature-1">
										<a href="{{url('/album-detail', $album_paginate->album_id)}}" class="block">
											<img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/galleries/albums')}}/{{$album_paginate->display_image}}" />
										</a>
										<div class="feature__body boxed boxed--border">

											<span>
												<?php 
												$orig_date =  explode('-', $album_paginate->date_input);
												$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
												echo date("M d, Y", strtotime($con_date));
												?>
											</span>

											<h5>{{$album_paginate->title}}</h5>


										</div>



									</article>
								</div>
								@endforeach


							</div>
							
							<div class="pagination">
								<ol>
									{!! $albums_paginate->render() !!}
								</ol>
							</div>

						</div>
						<!--end masonry-->
					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</section>

		@else
		<section>
			<div class="col-md-12" align="center">
				<h2 style="text-align: center; color: grey"> ---- no albums yet ----</h2>
			</div>
		</section>
		@endif






		<section class="space--xs bg--primary">
			<div class="container">
				<div class="cta cta--horizontal text-center-xs row">
					<div class="col-md-4">
						<h4>Uswag Turismo Kauswaganon</h4>
					</div>
					<div class="col-md-5">
						<p class="lead">
							Home of the annual HUGYAW SA KADAGATAN
						</p>
					</div>
					<div class="col-md-3 text-right text-center-xs">
						<a
						class="btn btn--primary type--uppercase"
						href="https://www.facebook.com/uswagturismokauswaganon/"
						>
						<span class="btn__text">
							Learn More
						</span>
					</a>
				</div>
			</div>
		</div>
		<!--end of container-->
	</section>

	@endsection

	@section('footer')
	@endsection
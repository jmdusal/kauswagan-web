@extends('welcome')

@section('title')
Municipality of Kauswagan - Gallery
@endsection

@section('content')
<div class="main-container">


  <section class="height-100 imagebg parallax" data-overlay="5">

    @foreach($album->homesgalleries as $key => $data)
    @if($key == 0)
    <div class="background-image-holder">

      <img alt="background" src="{{ asset('storage/galleries/albums')}}/{{$data->display_image}}" />
      
    </div>
    <div class="container pos-vertical-center">
      <div class="row">
        <div class="col-md-12" align="center">
          <h1>{{$album->title}}</h1>

          <span>
            <?php 
            $orig_date =  explode('-', $album->date_input);
            $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
            echo date("M d, Y", strtotime($con_date));
            ?>

          </span>

        </div>
      </div>
    </div>
    @else
    @endif
    @endforeach
  </section>





  <section class=" ">
    <div class="container">
      <div class="masonry">
        <hr />
        <div class="masonry__container row">

          @foreach($album->homesgalleries as $data)
          <div class="masonry__item col-md-6 col-12" data-masonry-filter="Print" >
            <div class="project-thumb hover-element border--round hover--active">

              <a href="{{ asset('storage/galleries/albums')}}/{{$data->display_image}}" data-lightbox="gallery">
                <div class="hover-element__initial">
                  <div class="background-image-holder">

                    <img alt="background" src="{{ asset('storage/galleries/albums')}}/{{$data->display_image}}" />

                  </div>
                </div>
                <div class="hover-element__reveal" data-scrim-top="5">
                  <div class="project-thumb__title">
                    <h4></h4>
                    <span>



                    </span>
                  </div>
                </div>
              </a>
            </div>
          </div>
          @endforeach



        </div>
      </div>
      <!--end masonry-->
    </div>
    <!--end of container-->
  </section>

  <section class="space--xs bg--primary">
    <div class="container">
      <div class="cta cta--horizontal text-center-xs row">
        <div class="col-md-4">
          <h4>Uswag Turismo Kauswaganon</h4>
        </div>
        <div class="col-md-5">
          <p class="lead">
            Home of the annual HUGYAW SA KADAGATAN
          </p>
        </div>
        <div class="col-md-3 text-right text-center-xs">
          <a
          class="btn btn--primary type--uppercase"
          href="https://www.facebook.com/uswagturismokauswaganon/"
          >
          <span class="btn__text">
            Learn More
          </span>
        </a>
      </div>
    </div>
  </div>
  <!--end of container-->
</section>



@endsection



@section('footer')
@endsection
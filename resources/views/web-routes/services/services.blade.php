
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Municipality of Kauswagan - Office Of The Mayor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="{{ asset('design/website/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/stack-interface.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/flickity.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/iconsmind.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/jquery.steps.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/theme-greensea.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('design/website/css/font-roboto.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" type="image/gif" sizes="16x16">
    </head>
    <body class=" ">
        <a id="start"></a>
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <a href="index.html">
                                <img class="logo logo-dark" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" />
                                <img class="logo logo-light" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" />
                            </a>
                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <section class="bar bar-3 bar--sm bg--primary">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="bar__module">
                                <ul class="social-list list-inline list--hover">
                                    <li>
                                        <a href="#">
                                            <i class="socicon socicon-google icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="socicon socicon-twitter icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="socicon socicon-instagram icon icon--xs"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 text-center">
                            <span class="type--fade">Municipality of Kauswagan</span>
                        </div>
                        <div class="col-lg-4 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal">
                                    <li>
                                        <div class="modal-instance">
                                            <a href="#" class="modal-trigger"><span>Login</span></a>
                                            <div class="modal-container">
                                                <div class="modal-content section-modal">
                                                    <section class="unpad ">
                                                        <div class="container">
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-6">
                                                                    <div class="boxed boxed--lg bg--white text-center feature">
                                                                        <div class="modal-close modal-close-cross"></div>
                                                                        <h3>Login to Your Account</h3>
                                                                        <a class="btn block btn--icon bg--facebook type--uppercase" href="#">
                                                                            <span class="btn__text">
                                                                                <i class="socicon-facebook"></i>
                                                                                Login with Facebook
                                                                            </span>
                                                                        </a>
                                                                        <a class="btn block btn--icon bg--twitter type--uppercase" href="#">
                                                                            <span class="btn__text">
                                                                                <i class="socicon-twitter"></i>
                                                                                Login with Twitter
                                                                            </span>
                                                                        </a>
                                                                        <hr data-title="OR">
                                                                        <div class="feature__body">
                                                                            <form>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <input type="text" placeholder="Username" />
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <input type="password" placeholder="Password" />
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <button class="btn btn--primary type--uppercase" type="submit">Login</button>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end of row-->
                                                                            </form>
                                                                            <span class="type--fine-print block">Dont have an account yet?
                                                                                <a href="#">Create account</a>
                                                                            </span>
                                                                            <span class="type--fine-print block">Forgot your username or password?
                                                                                <a href="#">Recover account</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end of row-->
                                                        </div>
                                                        <!--end of container-->
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <!--end bar-->
            
            <nav id="menu6" class="bar bar--sm bar-2 hidden-xs" data-scroll-class='50vh:pos-fixed'>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-md-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" />
                                    <img class="logo logo-light" alt="logo" src="{{ asset('design/website/img/kauswagan/kauswagan-logo.png')}}" />
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-lg-10 col-md-12 text-center text-left-xs text-left-sm mt-3 mb-3">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="index.html">
                                            <span class="dropdown__trigger">Home</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="about.html">
                                            <span class="dropdown__trigger">About</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">Stories & News</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                        <ul class="menu-vertical">
                                                            <li class="dropdown">
                                                                <span class="dropdown__trigger">Articles</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="stories.html">
                                                                                            Stories
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="news.html">
                                                                                            News
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                            <li class="dropdown">
                                                                <span class="dropdown__trigger">Gallery</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="gallery.html">
                                                                                            Photos
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="videos.html">
                                                                                            Videos
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                        <!--end dropdown container-->
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="index.html">
                                            <span class="dropdown__trigger">Bids & Awards</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="services.html">
                                            <span class="dropdown__trigger">Services</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="index.html">
                                            <span class="dropdown__trigger">Barangays</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="office-mayor.html">
                                            <span class="dropdown__trigger">Office of the Mayor</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="covid-updates.html">
                                            <span class="dropdown__trigger">COVID-19 Updates</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="transparency.html">
                                            <span class="dropdown__trigger">Transparency</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-lg-1 col-md-2 hidden-xs">
                            <div class="bar__module">
                                <a href="transparency.html">
                                    <img class="logo logo-dark" alt="Transparency Seal" src="{{ asset('design/website/img/kauswagan/logo-transparency-seal.png')}}" />
                                    <img class="logo logo-light" alt="Transparency Seal" src="{{ asset('design/website/img/kauswagan/logo-transparency-seal.png')}}" />
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>

            <footer class="space--sm footer-2 bg--secondary ">
                <div class="container">
                    <div class="row">
                        <div class="col-3">
                            <h6 class="type--uppercase">Republic of the Philippines</h6>
                            <ul class="list--hover">
                                <ul class="list--hover">
                                <li>
                                    <p>All content is in the public domain unless otherwise stated.</p>
                                </li>
                            </ul>
                            </ul>
                        </div>
                        <div class="col-3">
                            <h6 class="type--uppercase">Municipality of Kauswagan</h6>
                            <ul class="list--hover">
                                <li>
                                    <p>Learn more about the Municipality of Kauswagan, its structure, how government works and the people behind it.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <h6 class="type--uppercase">Government Links</h6>
                            <ul class="list--hover">
                                <li>
                                    <a href="https://www.bir.gov.ph/">BIR</a>
                                </li>
                                <li>
                                    <a href="https://www.dilg.gov.ph/">DILG</a>
                                </li>
                                <li>
                                    <a href="https://www.dswd.gov.ph/">DSWD</a>
                                </li>
                                <li>
                                    <a href="https://www.dpwh.gov.ph/">DPWH</a>
                                </li>
                                <li>
                                    <a href="https://www.deped.gov.ph/">DepEd</a>
                                </li>
                                <li>
                                    <a href="https://www.doh.gov.ph/">DOH</a>
                                </li>
                                <li>
                                    <a href="https://ncr.denr.gov.ph/">DENR</a>
                                </li>
                                <li>
                                    <a href="https://www.da.gov.ph/">DA</a>
                                </li>
                                <li>
                                    <a href="https://www.gov.ph/">GOV</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <h6 class="type--uppercase">Quick Links</h6>
                            <ul class="list--hover">
                                <li>
                                    <a href="about.html">About</a>
                                </li>
                                <li>
                                    <a href="stories.html">Stories</a>
                                </li>
                                <li>
                                    <a href="news.html">News</a>
                                </li>
                                <li>
                                    <a href="gallery.html">Photos</a>
                                </li>
                                <li>
                                    <a href="videos.html">Videos</a>
                                </li>
                                <li>
                                    <a href="bids_awards.html">Bids & Awards</a>
                                </li>
                                <li>
                                    <a href="services.html">Services</a>
                                </li>
                                <li>
                                    <a href="barangays.html">Barangays</a>
                                </li>
                                <li>
                                    <a href="office-mayor.html">Office of the Mayor</a>
                                </li>
                                <li>
                                    <a href="covid-updates.html">COVID-19 Updates</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="wrapper wrapper-3">
                        <div class="container">
                            <div class="row">
                                <div class="col widget-area">
                                    <div class="widget widget-html widget-ph">
                                        <div class="widget-content">
                                            <p class="text-center mb-2"><img width="80" src="{{ asset('design/website/img/kauswagan/coat-of-arms-ph-cl.svg')}}" alt="Coat of Arms of Philippines"></p>
                                            <p class="text-center mb-0">REPUBLIC OF THE PHILIPPINES</p>
                                            <p class="text-center small mb-0">All content is in the public domain unless otherwise stated.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <span class="type--fine-print">&copy;
                                <span class="update-year"></span> Municipality of Kauswagan</span>
                            <a class="type--fine-print" href="#">Privacy Policy</a>
                            <a class="type--fine-print" href="#">Legal</a>
                        </div>
                        <div class="col-md-6 text-right text-left-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ asset('design/website/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{ asset('design/website/js/flickity.min.js')}}"></script>
        <script src="{{ asset('design/website/js/easypiechart.min.js')}}"></script>
        <script src="{{ asset('design/website/js/parallax.js')}}"></script>
        <script src="{{ asset('design/website/js/typed.min.js')}}"></script>
        <script src="{{ asset('design/website/js/datepicker.js')}}"></script>
        <script src="{{ asset('design/website/js/isotope.min.js')}}"></script>
        <script src="{{ asset('design/website/js/ytplayer.min.js')}}"></script>
        <script src="{{ asset('design/website/js/lightbox.min.js')}}"></script>
        <script src="{{ asset('design/website/js/granim.min.js')}}"></script>
        <script src="{{ asset('design/website/js/jquery.steps.min.js')}}"></script>
        <script src="{{ asset('design/website/js/countdown.min.js')}}"></script>
        <script src="{{ asset('design/website/js/twitterfetcher.min.js')}}"></script>
        <script src="{{ asset('design/website/js/spectragram.min.js')}}"></script>
        <script src="{{ asset('design/website/js/smooth-scroll.min.js')}}"></script>
        <script src="{{ asset('design/website/js/scripts.js')}}"></script>
    </body>
</html>
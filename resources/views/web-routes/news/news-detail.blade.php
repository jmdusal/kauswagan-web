@extends('welcome')

@section('title')
Municipality of Kauswagan - News Detail
@endsection

@section('stylesheet')

<style>
.gallery_image{
    object-fit: contain;
    width: 100%;
    height: 450px;

}
.gallery{
    background-color: black;
}
</style>
@endsection

@section('content')
<div class="main-container">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <article class="masonry__item" data-masonry-filter="Web Design">
                        <!--end article title-->
                        <div class="article__body">
                            <div class="slider mb-3 gallery" data-arrows="true">
                                <ul class="slides">

                                    <li>
                                        <a href="{{ asset('storage/news_images')}}/{{$new->display_image}}" data-lightbox="display_image">
                                            <img class="gallery_image" alt="Image" src="{{ asset('storage/news_images')}}/{{$new->display_image}}" />
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="article__title text-center">
                                <h2>{{$new->title}}</h2>
                                <span>
                                    <?php 
                                    $orig_date =  explode('-', $new->date_input);
                                    $con_date = $orig_date[2].'-'.$orig_date[0].'-'.$orig_date[1];
                                    echo date("M d, Y", strtotime($con_date));
                                    ?>
                                </span>
                            </div>
                            <p>
                                {!!$new->description!!}
                            </p>
                            <br>

                            <div class="row">
                                @foreach($new->newsgalleries as $data)
                                <div class="col-md-4" style="padding-bottom: 20px">
                                    <a href="{{ asset('storage/galleries/news')}}/{{$data->display_image}}" data-lightbox="gallery">
                                        <img src="{{ asset('storage/galleries/news')}}/{{$data->display_image}}" alt="Avatar" class="img-thumbnail" style=" object-fit: cover; width: 100%; height: 150px;" />
                                    </a>
                                </div>
                                @endforeach
                            </div>


                        </div>
                    </article>
                </div>
                <div class="col-md-pull-8 col-md-4">
                    <div class="boxed boxed--border boxed--lg bg--secondary">
                        <div class="sidebar__widget">

                            <h5>Author</h5>
                            @foreach($authors as $author)

                            <a href="{{ asset('storage/authors_images')}}/{{$new->author->display_image}}" data-lightbox="author">
                                <img alt="Image" src="{{ asset('storage/authors_images')}}/{{$new->author->display_image}}" />
                            </a>

                        </div>
                        <!--end widget-->
                        <div class="sidebar__widget">
                            <h5 {{$author->id == $new->author_id}}>{{$new->author->name}}</h5>
                            <p {{$author->id == $new->author_id}}>
                                {!!$new->author->description!!}
                            </p>
                        </div>

                        <!--end widget-->
                        <div class="sidebar__widget">
                            <div class="col-md-12">
                                <ul class="social-list list-inline list--hover">

                                    @if($new->author->facebook_link !=  "")
                                    <li>
                                        <a href="{{$new->author->facebook_link}}" {{$author->id == $new->author_id}}>

                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                        </a>
                                    </li>
                                    @endif

                                    @if($new->author->twitter_link !=  "")
                                    <li>
                                        <a href="{{$new->author->twitter_link}}" {{$author->id == $new->author_id}}>
                                            <i class="socicon socicon-twitter icon icon--xs"></i>
                                        </a>
                                    </li>
                                    @endif



                                </ul>
                            </div>
                        </div>
                        <!--end widget-->
                        @endforeach
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>



    <section class="space--xs bg--primary">
        <div class="container">
            <div class="cta cta--horizontal text-center-xs row">
                <div class="col-md-4">
                    <h4>Uswag Turismo Kauswaganon</h4>
                </div>
                <div class="col-md-5">
                    <p class="lead">
                        Home of the annual HUGYAW SA KADAGATAN
                    </p>
                </div>
                <div class="col-md-3 text-right text-center-xs">
                    <a class="btn btn--primary type--uppercase" href="https://www.facebook.com/uswagturismokauswaganon/">
                        <span class="btn__text">
                            Learn More
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end of container-->
    </section>





    @endsection



    @section('footer')
    @endsection
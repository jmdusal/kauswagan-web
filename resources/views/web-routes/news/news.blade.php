@extends('welcome')

@section('title')
Municipality of Kauswagan - News
@endsection

@section('content')
<div class="main-container">
  <section class="unpad">
    <div class="slider" data-arrows="true">
      <ul class="slides slides--gapless">

        @if(isset($news_paginate))
        @foreach($news_flagship as $key => $new_flagship)
        <li class="col-md-6 col-12">
          <a class="block" href="{{url('/news-detail', $new_flagship->id)}}">
            <article class="imagebg" data-scrim-bottom="8">
              <div class="background-image-holder">
                <img alt="background" src="{{ asset('storage/news_images')}}/{{$new_flagship->display_image}}" />
              </div>
              <div class="article__title">
                <span>
                  <?php 
                  $orig_date =  explode('-', $new_flagship->date_input);
                  $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                  echo date("M d, Y", strtotime($con_date));
                  ?>
                </span>
                <h4>{{$new_flagship->title}}</h4>
              </div>
            </article>
          </a>
        </li>
        @endforeach
        @endif

      </ul>
    </div>
  </section>

  @if(count($news_paginate) != 0)

  <section>
    <div class="container">
      <form method="POST" action="{{url('/news/search')}}" class="form--horizontal row m-0">
        {{ csrf_field() }}
        <div class="col-md-8">
          <input type="text" name="q" placeholder="Type search keywords here" required="true"/>
        </div>
        <div class="col-md-4">
          <button type="submit" class="btn btn--primary type--uppercase">Search</button>
        </div>
      </form>
      @if(isset($details))
      <p>The Search results for your query <br>
        <b> {{$query}} </b> are:</p>
        @endif

        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="masonry">
              <div class="masonry__container row">
                <div class="masonry__item col-lg-4 col-md-6"></div>

                @foreach($news_paginate as $key => $new_paginate)
                <div class="masonry__item col-lg-4 col-md-6" data-masonry-filter="Marketing">
                  <article class="feature feature-1">
                    <a href="{{url('/news-detail', $new_paginate->id)}}" class="block">
                      <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/news_images')}}/{{$new_paginate->display_image}}" />
                    </a>
                    <div class="feature__body boxed boxed--border">
                      <span>
                        <?php 
                        $orig_date =  explode('-', $new_paginate->date_input);
                        $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                        echo date("M d, Y", strtotime($con_date));
                        ?>
                      </span>
                      <h5>{{$new_paginate->title}}</h5>
                      <a href="{{url('/news-detail', $new_paginate->id)}}">
                        Read More
                      </a>
                    </div>
                  </article>
                </div>
                @endforeach


              </div>
              <!--end of masonry container-->
              <div class="pagination">
                <ol>
                  {!! $news_paginate->render() !!}
                </ol>
              </div>

            </div>
            <!--end masonry-->
          </div>
        </div>
        <!--end of row-->
      </div>
      <!--end of container-->
    </section>

    @else

    <section>
      <div class="col-md-12" align="center">
        <h2 style="text-align: center; color: grey"> ---- no news yet ----</h2>
      </div>
    </section>
    @endif



    <section class="space--xs bg--primary">
      <div class="container">
        <div class="cta cta--horizontal text-center-xs row">
          <div class="col-md-4">
            <h4>Uswag Turismo Kauswaganon</h4>
          </div>
          <div class="col-md-5">
            <p class="lead">
              Home of the annual HUGYAW SA KADAGATAN
            </p>
          </div>
          <div class="col-md-3 text-right text-center-xs">
            <a
            class="btn btn--primary type--uppercase"
            href="https://www.facebook.com/uswagturismokauswaganon/"
            >
            <span class="btn__text">
              Learn More
            </span>
          </a>
        </div>
      </div>
    </div>
    <!--end of container-->
  </section>
  @endsection
  @section('footer')
  @endsection

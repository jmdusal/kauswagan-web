@extends('welcome')

@section('title')
Municipality of Kauswagan - Covid News
@endsection

@section('content')
<div class="main-container">
    <section class="unpad">

        <div class="slider" data-arrows="true">
            <ul class="slides slides--gapless">


                @if(isset($covids_paginate))
                @foreach($covids_flagship as $key => $covid_flagship)
                <li class="col-md-6 col-12">
                    <a class="block" href="{{url('/covid-detail', $covid_flagship->id)}}">
                        <article class="imagebg" data-scrim-bottom="8">
                            <div class="background-image-holder">
                                <img alt="background" src="{{ asset('storage/covids_images')}}/{{$covid_flagship->display_image}}" />
                            </div>
                            <div class="article__title">
                                <span>

                                    <?php 
                                    $orig_date =  explode('-', $covid_flagship->date_created);
                                    $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                                    echo date("M d, Y", strtotime($con_date));
                                    ?>
                                </span>
                                <h4>{{$covid_flagship->title}}</h4>
                            </div>
                        </article>
                    </a>
                </li>
                @endforeach
                @endif

            </ul>
        </div>
    </section>



    <section>
        <div class="container">
            <form method="POST" action="{{url('/covids/search')}}" class="form--horizontal row m-0">
                {{ csrf_field() }}
                <div class="col-md-8">
                    <input type="text" name="q" placeholder="Type search keywords here" required="true" />
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn--primary type--uppercase">Search</button>
                </div>
            </form>
            <hr>

            @if(isset($details))
            <br>
            <!-- @php
            echo count($details)
            @endphp -->
            <div class="row">
                <h3>Search Result for <span style="font-weight: 50px;">"{{$query}}"</span> </h3>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="masonry">
                        <div class="masonry__container row">
                            <div class="masonry__item col-lg-4 col-md-6"></div>

                            @foreach($details as $key => $result)
                            <div class="masonry__item col-lg-4 col-md-6" data-masonry-filter="Marketing">
                                <article class="feature feature-1">
                                    <a href="{{url('/covid-detail', $result->id)}}" class="block">
                                        <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/covids_images')}}/{{$result->display_image}}" />
                                    </a>
                                    <div class="feature__body boxed boxed--border">
                                        <span>
                                            <?php 
                                            $orig_date =  explode('-', $result->date_created);
                                            $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                                            echo date("M d, Y", strtotime($con_date));
                                            ?>
                                        </span>
                                        <h5>{{$result->title}}</h5>
                                        <a href="{{url('/covid-detail', $result->id)}}">
                                            Read More
                                        </a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                        </div>

                        @if( count($details) != 0)
                        <div class="pagination">
                            <ol>
                                {!! $details->render() !!}
                            </ol>
                        </div>
                        @endif
                    </div>
                    @elseif(isset($message))
                    <p> {{$message}}</p>
                    @endif
                    <!--end masonry-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>


    <section class="space--xs bg--primary">
        <div class="container">
          <div class="cta cta--horizontal text-center-xs row">
            <div class="col-md-4">
              <h4>Uswag Turismo Kauswaganon</h4>
          </div>
          <div class="col-md-5">
              <p class="lead">
                Home of the annual HUGYAW SA KADAGATAN
            </p>
        </div>
        <div class="col-md-3 text-right text-center-xs">
          <a
          class="btn btn--primary type--uppercase"
          href="https://www.facebook.com/uswagturismokauswaganon/"
          >
          <span class="btn__text">
            Learn More
        </span>
    </a>
</div>
</div>
</div>
<!--end of container-->
</section>
@endsection

@section('footer')
@endsection

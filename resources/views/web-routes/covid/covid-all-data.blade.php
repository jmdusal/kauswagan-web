@extends('welcome')

@section('title')
Municipality of Kauswagan - Covid News
@endsection

@section('content')
<div class="main-container">
  <section class="unpad">
    <div class="slider" data-arrows="true">

      <ul class="slides slides--gapless">

        @if(isset($covids_paginate))
        @foreach($covids_flagship as $key => $covid_flagship)
        <li class="col-md-6 col-12">
          <a class="block" href="{{url('/covid-detail', $covid_flagship->id)}}">
            <article class="imagebg" data-scrim-bottom="8">
              <div class="background-image-holder">
                <img alt="background" src="{{ asset('storage/covids_images')}}/{{$covid_flagship->display_image}}" />
              </div>
              <div class="article__title">
                <span>
                  <?php 
                  $orig_date =  explode('-', $covid_flagship->date_created);
                  $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                  echo date("M d, Y", strtotime($con_date));
                  ?>
                </span>
                <h4>{{$covid_flagship->title}}</h4>
              </div>
            </article>
          </a>
        </li>
        @endforeach
        @endif

      </ul>
    </div>
  </section>

  


  @if(count($covids_paginate) != 0)
  <section>
    <div class="container">
      <form method="POST" action="{{url('/covids/search')}}" class="form--horizontal row m-0">
        {{ csrf_field() }}
        <div class="col-md-8">
          <input type="text" name="q" placeholder="Type search keywords here" required="true"/>
        </div>
        <div class="col-md-4">
          <button type="submit" class="btn btn--primary type--uppercase">Search</button>
        </div>
      </form>
      @if(isset($details))
      <p>The Search results for your query <br>
        <b> {{$query}} </b> are:</p>
        @endif

        <hr>

        <div class="row">
          <div class="col-md-12">
            <div class="masonry">
              <div class="masonry__container row">
                <div class="masonry__item col-lg-4 col-md-6"></div>

                @foreach($covids_paginate as $key => $covid_paginate)
                <div class="masonry__item col-lg-4 col-md-6" data-masonry-filter="Marketing">
                  <article class="feature feature-1">
                    <a href="{{url('/covid-detail', $covid_paginate->id)}}" class="block">
                      <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/covids_images')}}/{{$covid_paginate->display_image}}" />
                    </a>
                    <div class="feature__body boxed boxed--border">

                      <span>
                        <?php 
                        $orig_date =  explode('-', $covid_paginate->date_created);
                        $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                        echo date("M d, Y", strtotime($con_date));
                        ?>
                      </span>

                      <h5>{{$covid_paginate->title}}</h5>
                      
                      <a href="{{url('/covid-detail', $covid_paginate->id)}}">

                        Read More
                      </a>
                    </div>

                    

                  </article>
                </div>
                @endforeach
                


              </div>

              <!--end of masonry container-->
              <div class="pagination">
                <ol>
                  {!! $covids_paginate->render() !!}
                </ol>
              </div>

            </div>
            <!--end masonry-->
          </div>
        </div>
        <!--end of row-->
      </div>
      <!--end of container-->
    </section>

    @else
    <section>
      <div class="col-md-12" align="center">
        <h2 style="text-align: center; color: grey"> ---- no covid news yet ----</h2>
      </div>
    </section>
    @endif





    <section class="space--xs bg--primary">
      <div class="container">
        <div class="cta cta--horizontal text-center-xs row">
          <div class="col-md-4">
            <h4>Uswag Turismo Kauswaganon</h4>
          </div>
          <div class="col-md-5">
            <p class="lead">
              Home of the annual HUGYAW SA KADAGATAN
            </p>
          </div>
          <div class="col-md-3 text-right text-center-xs">
            <a
            class="btn btn--primary type--uppercase"
            href="https://www.facebook.com/uswagturismokauswaganon/"
            >
            <span class="btn__text">
              Learn More
            </span>
          </a>
        </div>
      </div>
    </div>
    <!--end of container-->
  </section>
  @endsection
  @section('footer')
  @endsection

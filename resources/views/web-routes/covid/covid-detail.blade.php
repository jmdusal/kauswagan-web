@extends('welcome')

@section('title')
Municipality of Kauswagan - Covid Detail
@endsection

@section('stylesheet')
<style>
    .gallery_image{
        object-fit: contain;
        width: 100%;
        height: 450px;

    }
    .gallery{
        background-color: black;
    }
</style>
@endsection

@section('content')
<div class="main-container">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <article class="masonry__item" data-masonry-filter="Web Design">
                        <!--end article title-->
                        <div class="article__body">
                            <div class="slider mb-3 gallery" data-arrows="true">
                                <ul class="slides">

                                    <li>
                                        <a href="{{ asset('storage/covids_images')}}/{{$covid->display_image}}" data-lightbox="display_image">
                                            <img class="gallery_image" alt="Image" src="{{ asset('storage/covids_images')}}/{{$covid->display_image}}" />
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="article__title text-center">
                                <h2>{{$covid->title}}</h2>

                                <span>

                                    <?php 

                                    $orig_date =  explode('-', $covid->date_created);

                                    $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];

                                    echo 'Created on '.date("M jS, Y", strtotime($con_date));
                                    ?>

                                </span>

                            </div>
                            <p>
                                {!!$covid->description!!}
                            </p>
                            <br>

                            <div class="row">
                                @foreach($covid->covidsgalleries as $data)

                                <div class="col-md-4" style="padding-bottom: 20px">
                                    <a href="{{ asset('storage/galleries/covids')}}/{{$data->display_image}}" data-lightbox="gallery">
                                        <img src="{{ asset('storage/galleries/covids')}}/{{$data->display_image}}" alt="Avatar" class="img-thumbnail" style=" object-fit: cover; width: 100%; height: 150px;" />

                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </article>
                </div>


                <div class="col-md-pull-8 col-md-4">
                    <div class="boxed boxed--border boxed--lg bg--secondary">
                        <div class="sidebar__widget">

                            <h5>Author</h5>
                            @foreach($authors as $author)

                            <a href="{{ asset('storage/authors_images')}}/{{$covid->author->display_image}}" data-lightbox="author">
                                <img alt="Image" src="{{ asset('storage/authors_images')}}/{{$covid->author->display_image}}" />
                            </a>
                        </div>
                        

                        <div class="sidebar__widget">
                            <h5 {{$author->id == $covid->author_id}}>{{$covid->author->name}}</h5>
                            <p {{$author->id == $covid->author_id}}>
                                {!!$covid->author->description!!}
                            </p>
                        </div>

                        <div class="sidebar__widget">
                            <div class="col-md-12">
                                <ul class="social-list list-inline list--hover">
                                	@if($covid->author->facebook_link !=  "")
                                    <li>
                                    	<a href="{{$covid->author->facebook_link}}" {{$author->id == $covid->author_id}}>

                                            <i class="socicon socicon-facebook icon icon--xs"></i>
                                        </a>
                                    </li>
                                    @endif
                                    @if($covid->author->twitter_link !=  "")
                                    <li>
                                    	<a href="{{$covid->author->twitter_link}}" {{$author->id == $covid->author_id}}>
                                            <i class="socicon socicon-twitter icon icon--xs"></i>
                                        </a>
                                    </li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="space--xs bg--primary">
        <div class="container">
            <div class="cta cta--horizontal text-center-xs row">
                <div class="col-md-4">
                    <h4>Uswag Turismo Kauswaganon</h4>
                </div>
                <div class="col-md-5">
                    <p class="lead">
                        Home of the annual HUGYAW SA KADAGATAN
                    </p>
                </div>
                <div class="col-md-3 text-right text-center-xs">
                    <a class="btn btn--primary type--uppercase" href="https://www.facebook.com/uswagturismokauswaganon/">
                        <span class="btn__text">
                            Learn More
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end of container-->
    </section>





    @endsection



    @section('footer')
    @endsection
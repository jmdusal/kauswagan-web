@extends('welcome')

@section('title')
Municipality of Kauswagan - Transparency
@endsection

@section('content')
<div class="main-container">
  <section class="space--sm">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Transparency</h2>
          <hr />
      </div>
  </div>
  <!--end of row-->
</div>
<!--end of container-->
</section>
<section class="unpad--top">
    <div class="container">
      <!--end article title-->
      <div class="">
        <div class="text-center mb-5">
          <img
          alt="Image"
          width="18%"
          src="{{ asset('design/website/img/kauswagan/Transparency_seal-01.png')}}"/>
      </div>
      <h3 class="type--bold">S Y M B O L I S M</h3>
      <p>
          A pearl buried inside a tightly-shut shell is practically
          worthless. Government information is a pearl, meant to be shared
          with the public in order to maximize its inherent value.
      </p>
      <p>
          The Transparency Seal, depicted by a pearl shining out of an open
          shell, is a symbol of a policy shift towards openness in access to
          government information. On the one hand, it hopes to inspire
          Filipinos in the civil service to be more open to citizen
          engagement; on the other, to invite the Filipino citizenry to
          exercise their right to participate in governance.
      </p>
      <p>
          This initiative is envisioned as a step in the right direction
          towards solidifying the position of the Philippines as the Pearl
          of the Orient – a shining example for democratic virtue in the
          region.
      </p>
      <p>
          DBM Compliance with Sec. 91 (Transparency Seal) R.A. No. 10633
          (General Appropriations Act FY 2014)
      </p>
            <!-- <ul class="accordion accordion-1 accordion--oneopen"">
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">Department of Information and Communications technology</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5 mb-2">
                                            a. The agency’s <a href="#!">Mandate</a>, <a href="#!">Powers</a>, and <a href="#!">Functions</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            c. Position, designation and Contact Information
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">II. Financial Accountability Reports</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5">
                                            OOOOOOOOOOOOOOOOOOOOOOO
                                        </li>
                                        <li class="ml-5">
                                            OOOOOOOOOOOOOOOOOOOOOOO
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">III. Approved budgets and Corresponding Targets</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">DICT-2018</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">NTC-2018</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">NPC-2018</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">CICC-2018</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2017 DICT GAA-Office of the Secretary</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">CICC-2017 GAA</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">NTC-2017 GAA</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">NPC-2017 GAA</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2019-GAA Targets</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2016-GAA Targets</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2015 GAA Targets</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2014 GAA Targets</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2013 GAA Targets</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">2012 GAA Targets</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">IV. Major Projects and Programs, Beneficiaries, and Status of Implementation</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">Major program/projects categorized in accordance with the five key results areas under E.O. No. 43, s.2011</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">CY 2014, As of Second Semester</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">V.DICT Procurement Documents</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5">
                                            <a href="#!">OOOOOOOO</a>
                                        </li>
                                        <li class="ml-5">
                                            <a href="#!">OOOOOOOO</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">VI.Quality Management System (QMS) Certification to ISO 9001:2015</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5">
                                            <a href="#!">DICT-ISO-aligned QMS Manual</a>
                                        </li>
                                        <li class="ml-5">
                                            <a href="#!">DICT-Work Instruction Manual</a>
                                        </li>
                                        <li class="ml-5">
                                            <a href="#!">DICT-Resource Management</a>
                                        </li>
                                        <li class="ml-5">
                                            <a href="#!">DICT-Product Realization Manual</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">VII. System of Agency Ranking of Delivery Units for PBB</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">Guidelines/Mechanics in Ranking Offices/Delivery Units For The Grant of 2019 Performance Based Bonus</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">Guidelines/Mechanics in Ranking Offices/Delivery Units For The Grant of 2018 Performance Based Bonus</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!"> 2016 Guidelines on the Ranking of Delivery Units  and Individual Employees for Purposes of Granting the 2016 Performance Based Bonus.</a>
                                        </li>
                                        <li class="ml-5 mb-2">
                                            <a href="#!">Guidelines on the Ranking of Delivery Units  and Individual Employees for Purposes of Granting the 2017 Performance Based Bonus.</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">VIII.The Agency Review and Compliance Procedure of Statements and Financial Disclosures</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <li class="ml-5">
                                            <a href="#!">DICT-DO-005-2018 GUIDELINES ESTABLISHING A STANDARD REVIEW AND COMPLIANCE PROCEDURE FOR THE SUBMISSION OF STATEMENT OF ASSETS , LIABILITIES AND NETWORTH (SALN) </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">IX.Freedom of Information</span>
                                </div>
                                <div class="accordion__content">
                                    <ul>
                                        <ul class="mb-3">
                                            <li class="ml-5">
                                                <a href="#!">DICT’s Peoples FOI manual</a>
                                            </li>
                                        </ul>
                                        <ul class="mb-3">
                                            <li class="ml-5">
                                                <h6>FOI Registry</h6>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Registry-2017</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Registry-2018</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Registry-2019</a>
                                            </li>
                                        </ul>
                                        <ul class="mb-3">
                                            <li class="ml-5">
                                                <h6>FOI Inventory</h6>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Inventory-2017</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Inventory-2018</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Inventory-2019</a>
                                            </li>
                                        </ul>
                                        <ul class="mb-3">
                                            <li class="ml-5">
                                                <h6>FOI Summary Report</h6>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Summary Report-2017</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Summary Report-2018</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Summary Report-2019</a>
                                            </li>
                                            <li class="ml-5">
                                                <a href="#!">FOI Summary-Report</a>
                                            </li>
                                        </ul>
                                    </ul>
                                    
                                </div>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </section>

            <section class="space--xs bg--primary">
                <div class="container">
                  <div class="cta cta--horizontal text-center-xs row">
                    <div class="col-md-4">
                      <h4>Uswag Turismo Kauswaganon</h4>
                  </div>
                  <div class="col-md-5">
                      <p class="lead">
                        Home of the annual HUGYAW SA KADAGATAN
                    </p>
                </div>
                <div class="col-md-3 text-right text-center-xs">
                  <a
                  class="btn btn--primary type--uppercase"
                  href="https://www.facebook.com/uswagturismokauswaganon/"
                  >
                  <span class="btn__text">
                      Learn More
                  </span>
              </a>
          </div>
      </div>
  </div>
  <!--end of container-->
</section>
@endsection

@section('footer')
@endsection

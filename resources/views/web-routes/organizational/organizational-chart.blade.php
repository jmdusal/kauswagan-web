 @extends('welcome')



@section('title')
Municipality of Kauswagan - Organizational Chart
@endsection




@section('content')
<div class="main-container">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <article class="masonry__item" data-masonry-filter="Web Design">
                                <!--end article title-->
                                <div class="article__body">
                                    <div class="slider mb-3" data-arrows="true">
                                        <ul class="slides">
                                            <li>
                                                <img alt="Image" src="{{ asset('design/website/img/work-2.jpg')}}" />
                                            </li>
                                            <li>
                                                <img alt="Image" src="{{ asset('design/website/img/work-3.jpg')}}" />
                                            </li>
                                            <li>
                                                <img alt="Image" src="{{ asset('design/website/img/work-4.jpg')}}" />
                                            </li>
                                            <li>
                                                <img alt="Image" src="{{ asset('design/website/img/work-5.jpg')}}" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="article__title text-center">
                                        <a href="#">
                                            <h2>Organizational Chart</h2>
                                        </a>
                                        <span>April 10th 2016 in </span>
                                    </div>
                                    <p>
                                        Co-working SpaceTeam fund big data prototype prototype long shadow latte big data. Innovate affordances personas user centered design paradigm user centered design innovate quantitative vs. qualitative pivot thought leader viral paradigm cortado affordances.
                                    </p>
                                </div>
                            </article>
                        </div>
                        <!-- <div class="col-md-pull-8 col-md-4">
                            <div class="boxed boxed--border boxed--lg bg--secondary">
                                <div class="sidebar__widget">
                                    <h5>Author</h5>
                                    <a href="{{ asset('design/website/img/blog-1.jpg')}}" data-lightbox="gallery">
                                        <img alt="Image" src="{{ asset('design/website/img/blog-1.jpg')}}" />
                                    </a>
                                </div>
                               
                                <div class="sidebar__widget">
                                    <h5>John Doe</h5>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </p>
                                </div>
                                
                                <div class="sidebar__widget">
                                    <div class="col-md-12">
                                        <ul class="social-list list-inline list--hover">
                                            <li>
                                                <a href="#">
                                                    <i class="socicon socicon-google icon icon--xs"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="socicon socicon-twitter icon icon--xs"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="socicon socicon-facebook icon icon--xs"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="socicon socicon-instagram icon icon--xs"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                   
                </div> -->
               
            </section>
            <section class="space--xs bg--primary">
                <div class="container">
                    <div class="cta cta--horizontal text-center-xs row">
                        <div class="col-md-4">
                            <h4>Uswag Turismo Kauswaganon</h4>
                        </div>
                        <div class="col-md-5">
                            <p class="lead">
                                Home of the annual HUGYAW SA KADAGATAN
                            </p>
                        </div>
                        <div class="col-md-3 text-right text-center-xs">
                            <a class="btn btn--primary type--uppercase" href="https://www.facebook.com/uswagturismokauswaganon/">
                                <span class="btn__text">
                                    Learn More
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end of container-->
            </section>









@endsection



@section('footer')
@endsection
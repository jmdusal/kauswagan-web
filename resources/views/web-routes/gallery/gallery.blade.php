@extends('welcome')

@section('title')
Municipality of Kauswagan - Gallery
@endsection

@section('content')
<div class="main-container">
  <section class="height-100 imagebg parallax" data-overlay="5">
    <div class="background-image-holder">
      <img alt="background" src="{{ asset('design/website/img/gallery_cover.jpg')}}" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row">
        <div class="col-md-12" align="center">
          <h1>Kauswagan Photos</h1>

        </div>
      </div>
    </div>
  </section>
  <section class=" ">
    <div class="container">
      <div class="masonry">

        <hr />
        
        <div class="masonry__container row">
         @foreach($homegalleries as $key => $homegallery)

         <div class="masonry__item col-md-6 col-12" data-masonry-filter="Print" >
          <div class="project-thumb hover-element border--round hover--active">
            <a href="{{ asset('storage/home_galleries_images')}}/{{$homegallery->display_image}}" data-lightbox="gallery">
              <div class="hover-element__initial">
                <div class="background-image-holder">
                  <img alt="background" src="{{ asset('storage/home_galleries_images')}}/{{$homegallery->display_image}}" />
                </div>
              </div>
              <div class="hover-element__reveal" data-scrim-top="5">
                <div class="project-thumb__title">
                  <h4>{{$homegallery->title}}</h4>
                  <span>{{$homegallery->date_taken}}



                  </span>
                </div>
              </div>
            </a>
          </div>
        </div>
        @endforeach
        <!--end item-->
        
      </div>
    </div>
    <!--end masonry-->
  </div>
  <!--end of container-->
</section>

<section class="space--xs bg--primary">
  <div class="container">
    <div class="cta cta--horizontal text-center-xs row">
      <div class="col-md-4">
        <h4>Uswag Turismo Kauswaganon</h4>
      </div>
      <div class="col-md-5">
        <p class="lead">
          Home of the annual HUGYAW SA KADAGATAN
        </p>
      </div>
      <div class="col-md-3 text-right text-center-xs">
        <a
        class="btn btn--primary type--uppercase"
        href="https://www.facebook.com/uswagturismokauswaganon/"
        >
        <span class="btn__text">
          Learn More
        </span>
      </a>
    </div>
  </div>
</div>
<!--end of container-->
</section>



@endsection



@section('footer')
@endsection
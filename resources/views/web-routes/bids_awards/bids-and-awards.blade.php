@extends('welcome')

@section('title')
Municipality of Kauswagan - Bids and Awards
@endsection

@section('content')
<div class="main-container">
  <section class="text-center height-50">
    <div class="container pos-vertical-center">
      <div class="row">
        <div class="col-md-8">
          <h1>Bids and Awards</h1>
              <!--               <p class="lead">
                From a team of passionate creators working side-by-side with our
                partners to deliver engaging digital and physical campaigns.
              </p> -->
            </div>
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>

      @if(count($awards) != 0)

      @foreach($awards as $key => $award)
      <section class="switchable feature-large">
        <div class="container">

          <div class="row justify-content-around">
            <div class="col-md-6" >

              <a href="{{ asset('storage/awards_images')}}/{{$award->display_image}}" data-lightbox="gallery">
                <img alt="Image" class="border--round box-shadow-wide" src="{{ asset('storage/awards_images')}}/{{$award->display_image}}"/>
              </a>

            </div>
            <div class="col-md-6 col-lg-5">
              <h2>{{$award->title}}</h2>
              <p class="lead">
                {!!$award->description!!}
              </p>
              @if($award->link != '')
              <a href="{{$award->link}}" target="__blank">View more &raquo;</a>
              @endif
            </div>
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      @endforeach

      @else

      <div class="col-md-12" align="center">
        <h2 style="text-align: center; color: grey"> ---- No data yet ----</h2>
        <br>
        <br>
      </div>

      @endif


      <section class="space--xs bg--primary">
        <div class="container">
          <div class="cta cta--horizontal text-center-xs row">
            <div class="col-md-4">
              <h4>Uswag Turismo Kauswaganon</h4>
            </div>
            <div class="col-md-5">
              <p class="lead">
                Home of the annual HUGYAW SA KADAGATAN
              </p>
            </div>
            <div class="col-md-3 text-right text-center-xs">
              <a
              class="btn btn--primary type--uppercase"
              href="https://www.facebook.com/uswagturismokauswaganon/"
              >
              <span class="btn__text">
                Learn More
              </span>
            </a>
          </div>
        </div>
      </div>
      <!--end of container-->
    </section>
    @endsection

    @section('footer')
    @endsection
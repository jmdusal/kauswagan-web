@extends('welcome')



@section('title')
Municipality of Kauswagan - COVID updates
@endsection




@section('content')

<div class="main-container">
            <section class="space--sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>COVID-19 Updates</h1>
                            <hr>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="switchable switchable--switch space--xs text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="height-50 imagebg border--round" data-overlay="2">
                                <div class="background-image-holder">
                                    <img alt="background" src="{{ asset('design/website/img/landing-covid.jpg')}}" />
                                </div>
                                <div class="pos-vertical-center col-md-6 col-lg-5 pl-5">
                                    <h2 class="text-left">COVID-19</h2>
                                    <p class="lead text-left">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="switchable">
                <div class="container">
                    <div class="row justify-content-around">
                        <div class="col-md-8 col-lg-7">
                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            <p>
                                Those were the words used by mayor Rommel Arnado of Kauswagan when he addressed the annual congress of the Association of Netherlands Municipalities (VNG). As the mayor of the local government that won the first edition of the UCLG Peace Prize, he was in the Netherlands on invitation of the VNG to share his experience on dealing with conflict between different groups in his municipality. In front of over 2000 Dutch mayors and aldermen, mayor Arnado was interviewed by the host mayor of the congress René Verhulst of Goes Municipality.
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="text-block">
                                <h5>Responsibilities</h5>
                                <ul class="bullets">
                                    <li>Reporting to Project Manager</li>
                                    <li>Converting designs to code</li>
                                    <li>Writing SEO performant code</li>
                                    <li>Integrating React / Angular</li>
                                </ul>
                            </div>
                            <div class="text-block">
                                <h5>Experience</h5>
                                <p>
                                    Ideally 2 &mdash; 4 years experience in an agency environment working in mult-tiered teams
                                </p>
                            </div>
                            <div class="text-block">
                                <h5>Employment Terms</h5>
                                <p>
                                    Full time, on site at our HQ in Melbourne, AU
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            
            <!-- <section class="text-center bg--primary">
                <div class="container">
                    <div class="row justify-content-center">
                        <h2>Get our daily news direct to your inbox</h2>
                        <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                            <div class="col-lg-3 col-md-4">
                                <input class="validate-required" type="text" name="NAME" placeholder="Your Name" />
                            </div>
                            <div class="col-lg-3 col-md-4">
                                <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Email Address" />
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <button type="submit" class="btn btn--primary type--uppercase">Subscribe</button>
                            </div>
                            <div class="col-md-12">
                                <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                <span>I have read and agree to the
                                    <a href="#">terms and conditions</a>
                                </span>
                            </div>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                            </div>
                        </form>
                    </div>
                   
                </div>
                
            </section> -->
            @endsection



@section('footer')
@endsection

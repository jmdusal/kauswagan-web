@extends('welcome')

@section('title')
Municipality of Kauswagan - Stories
@endsection

@section('content')
<div class="main-container">
    <section>
        <div class="container">
            <form method="POST" action="{{url('/videos/search')}}" class="form--horizontal row m-0">
                {{ csrf_field() }}
                <div class="col-md-8">
                    <input type="text" name="q" placeholder="Type search keywords here" required="true" />
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn--primary type--uppercase">Search</button>
                </div>
            </form>
            <hr>

            @if(isset($details))
            <br>
            <!-- @php
            echo count($details)
            @endphp -->
            <div class="row">
                <h3>Search Result for <span style="font-weight: 50px;">"{{$query}}"</span> </h3>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="masonry">
                        <div class="masonry__container row">
                            <div class="masonry__item col-lg-4 col-md-6"></div>

                            @foreach($details as $key => $result)


                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Television">
                              <div class="video-cover border--round">
                                <div class="background-image-holder">

                                    <img alt="image" src="{{ asset('storage/home_videos_images')}}/{{$result->display_image}}"/>
                              </div>
                              <div class="video-play-icon"></div>

                              {!!$result->embed_video!!}

                          </div>
                          <!--end video cover-->
                          <span class="h4 inline-block">{{$result->title}}</span>
                      </div>


                      @endforeach
                  </div>

                  @if( count($details) != 0)
                  <div class="pagination">
                    <ol>
                        {!! $details->render() !!}
                    </ol>
                </div>
                @endif
            </div>
            @elseif(isset($message))
            <p> {{$message}}</p>
            @endif
            <!--end masonry-->
        </div>
    </div>
    <!--end of row-->
</div>
<!--end of container-->
</section>


<section class="space--xs bg--primary">
    <div class="container">
      <div class="cta cta--horizontal text-center-xs row">
        <div class="col-md-4">
          <h4>Uswag Turismo Kauswaganon</h4>
      </div>
      <div class="col-md-5">
          <p class="lead">
            Home of the annual HUGYAW SA KADAGATAN
        </p>
    </div>
    <div class="col-md-3 text-right text-center-xs">
      <a
      class="btn btn--primary type--uppercase"
      href="https://www.facebook.com/uswagturismokauswaganon/"
      >
      <span class="btn__text">
        Learn More
    </span>
</a>
</div>
</div>
</div>
<!--end of container-->
</section>
@endsection

@section('footer')
@endsection

@extends('welcome')

@section('title')
Municipality of Kauswagan - Videos
@endsection

@section('content')
<div class="main-container">
  <section class="height-90 imagebg parallax" data-overlay="5">
    <div class="background-image-holder">
      <img alt="background" src="{{ asset('design/website/img/videos_cover.jpg')}}" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row">
        <div class="col-md-12" align="center">
          <h1>Kauswagan Videos</h1>
        </div>
      </div>
      <!--end of row-->
    </div>
    <!--end of container-->
  </section>




  <section>
    <div class="container">
      <div class="masonry">
        <form method="POST" action="{{url('/videos/search')}}" class="form--horizontal row m-0">
          {{ csrf_field() }}
          <div class="col-md-8">
            <input type="text" name="q" placeholder="Type search keywords here" required="true"/>
          </div>
          <div class="col-md-4">
            <button type="submit" class="btn btn--primary type--uppercase">Search</button>
          </div>
        </form>
        @if(isset($details))
        <p>The Search results for your query <br>
          <b> {{$query}} </b> are:</p>
          @endif

          <hr />
          <!--end masonry filters-->
          <div class="masonry__container row">


            @foreach($videos as $key=> $video)
            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Television">
              <div class="video-cover border--round">
 <!--                <div class="background-image-holder">

                  <img alt="image" src="{{ asset('storage/home_videos_images')}}/{{$video->display_image}}"/>
                </div>
                <div class="video-play-icon"></div> -->

                {!!$video->embed_video!!}

              </div>
              <!--end video cover-->
              <span class="h4 inline-block">{{$video->title}}</span>
            </div>
            @endforeach
          </div>

          <div class="pagination">
            <ol>
              {!! $videos->render() !!}
            </ol>
          </div>




        </div>
        <!--end masonry-->
      </div>
      <!--end of container-->
    </section>
    <section class="space--xs bg--primary">
      <div class="container">
        <div class="cta cta--horizontal text-center-xs row">
          <div class="col-md-4">
            <h4>Uswag Turismo Kauswaganon</h4>
          </div>
          <div class="col-md-5">
            <p class="lead">
              Home of the annual HUGYAW SA KADAGATAN
            </p>
          </div>
          <div class="col-md-3 text-right text-center-xs">
            <a
            class="btn btn--primary type--uppercase"
            href="https://www.facebook.com/uswagturismokauswaganon/"
            >
            <span class="btn__text">
              Learn More
            </span>
          </a>
        </div>
      </div>
    </div>
    <!--end of container-->
  </section>
  @endsection

  @section('footer')
  @endsection
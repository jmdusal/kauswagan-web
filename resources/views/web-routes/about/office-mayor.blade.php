@extends('welcome')

@section('title')
Municipality of Kauswagan - Office of the Mayor
@endsection




@section('content')

<div class="main-container">
    <section class="space--sm" style="padding-bottom: 0px">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Office of the Mayor</h1>
                    <hr>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>

    @foreach($mayoroffice as $key => $mayor)

    <section class="switchable switchable--switch space--xs text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-11">
                    <div class="height-80 imagebg border--round" data-overlay="2">
                        <div class="background-image-holder">
                            <img alt="background" src="{{ asset('storage/mayor_images')}}/{{$mayor->display_image}}" />
                        </div>
                        <div class="pos-vertical-center col-md-6 col-lg-5 pl-5">
                            <h2 class="text-left">{{$mayor->mayor_name}}</h2>
                            <p class="lead text-left">
                                {{$mayor->mini_title}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="switchable">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-md-12 col-lg-12">
                    {!! $mayor->description !!}
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>

    @endforeach


    @endsection



    @section('footer')
    @endsection




    @section('footer')
    @endsection
@extends('welcome')

@section('title')
Municipality of Kauswagan - Barangays
@endsection



@section('content')

<div class="main-container">
    <section class="text-center height-50">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        Kauswagan is a coastal municipality in the province of Lanao del
                        Norte.
                    </h3>
                    <p>
                        The municipality has a land area of 60.37 square kilometers or
                        23.31 square miles which constitutes 1.80% of Lanao del Norte's
                        total area. Its population as determined by the 2015 Census was
                        26,278. This represented 3.89% of the total population of Lanao
                        del Norte province, or 0.56% of the overall population of the
                        Northern Mindanao region. Based on these figures, the population
                        density is computed at 435 inhabitants per square kilometer or
                        1,127 inhabitants per square mile.
                    </p>
                </div>
            </div>
        </div>
    </section>

    
    @if(count($barangays) != 0)
    <section class=" ">
        <div class="container">
            <div class="masonry">

                <div class="col-md-12 text-center mt-5 mb-5">
                    <h3>Barangays</h3>
                    <hr />
                    <h4>Kauswagan is politically subdivided into 13 barangays.</h4>
                </div>

                <div class="masonry__container row">
                    @foreach($barangays as $key => $barangay)
                    <div class="masonry__item col-md-3 col-6">

                        <div class="project-thumb hover-element hover-element2 border--round hover--active">
                            <a href="{{url('/barangays-detail', $barangay->id)}}">
                                <div class="hover-element__initial">
                                    <div class="background-image-holder">
                                        <img alt="background" src="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}"/>
                                    </div>
                                </div>
                                <div class="hover-element__reveal" data-scrim-top="5">
                                    <div class="project-thumb__title">
                                        <h4>{{$barangay->barangay_name}}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @else

    @endif






    <section class="space--xs bg--primary">
        <div class="container">
            <div class="cta cta--horizontal text-center-xs row">
                <div class="col-md-4">
                    <h4>Uswag Turismo Kauswaganon</h4>
                </div>
                <div class="col-md-5">
                    <p class="lead">
                        Home of the annual HUGYAW SA KADAGATAN
                    </p>
                </div>
                <div class="col-md-3 text-right text-center-xs">
                    <a
                    class="btn btn--primary type--uppercase"
                    href="https://www.facebook.com/uswagturismokauswaganon/"
                    >
                    <span class="btn__text">
                        Learn More
                    </span>
                </a>
            </div>
        </div>
    </div>
    <!--end of container-->
</section>





@endsection



@section('footer')
@endsection
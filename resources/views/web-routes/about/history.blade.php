@extends('welcome')

@section('title')
Municipality of Kauswagan - History
@endsection



@section('content')
<div class="main-container">
  <section class="text-center imagebg space--lg" data-overlay="3">
    <div class="background-image-holder">
      <img alt="background" src="{{ asset('design/website/img/history.jpg')}}" />
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-lg-8">
          <h1 class="type--bold">History of Kauswagan</h1>
          <p class="lead">
            Kauswagan is known as the
            <span class="type--bold">"Coconut industry of the interior areas"</span>. It also produce Fishing, Rice, Corns and basic vegetables.
            The Christian settlers renamed DALICANAN into KAUSWAGAN meaning
            <span class="type--bold">"Progress"</span> or
            <span class="type--bold">"Improvement"</span>
          </p>
        </div>
      </div>
      <!--end of row-->
    </div>
    <!--end of container-->
  </section>
  <section class="text-center height-50">
    <div class="container pos-vertical-center">
      <div class="row">
        <div class="col-md-8 col-lg-6">
          <h1>Our History</h1>
              <!--           <p class="lead">
            The Christian settlers renamed DALICANAN into KAUSWAGAN meaning <span class="type--bold">"Progress"</span> or <span class="type--bold">"Improvement"</span>
          </p> -->
        </div>
      </div>
      <!--end of row-->
    </div>
    <!--end of container-->
  </section>

















  <section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="process-1">

            @foreach($histories as $history)
            <div class="process__item">
              <h4>
                {{$history->title}}
              </h4>
              {!! $history->description !!}
            </div>
            @endforeach
          </div>
          <!--end process-->
        </div>
      </div>
      <!--end of row-->
    </div>
    <!--end of container-->
  </section>


















  <section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
          <!--           <h1 class="type--bold">History of Kauswagan</h1> -->
              <!--           <p class="lead text-justify" style="text-indent: 50px">
            Kauswagan is the second coastal municipality   of the Province of Lanao del Norte located near the Southern boundary. It lies on the mid-central portion of the Northwestern Mindanao coastline with a nautical grid coordinate off 8*deg 12” longitude and 124 deg.. 5” east latitude. It is located twenty (20) kilometers away from Iligan City; bounded on the North by Iligan Bay; on the East by larapan River, which is the common boundary  with the Municipality of Linamon; on the South by the Municipality of Poona Piaagapo and on the West by the Municipalitt of Bacolod with Rupagan River as the common boundary.
          </p> -->

          <!--           <br> -->
          <h3>
            Those who served as Municipal Mayors of Kauswagan from the time
            it was created to the present were :
          </h3>
          <br />

          <div class="row justify-content-center">
            <table
            style="height: 467px; width: 700px; border-color: black;"
            >
            <tbody>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;1.&nbsp; Mr. Jose Q. Balazo&nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  April 25,1948- March 1950&nbsp;
                </td>
                <td style="width: 119px; height: 33px;">
                  &nbsp;Appointed
                </td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;2.&nbsp; Mr. Santiago Ramirez&nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;March 1950 to April 20,1950&nbsp;
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;- do -</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;3.&nbsp; Capt. Joseph T. Sanguila,Sr
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;May 1950 to August 1951
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;- do -</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;4.&nbsp; Mr. Vicctoriano Rafols &nbsp;&nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;Sept. 1951 to Dec. 31,1951
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;- do -</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;5.&nbsp; Capt. Joseph T. Sanguila,Sr.
                </td>
                <td style="width: 282px; height: 33px;">
                  Jan. 1952 to 1955&nbsp;
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;6.&nbsp; Mr. Teodulfo D. Maslog,Sr. &nbsp;&nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;Jan. 1956-1959&nbsp;
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;7.&nbsp; Capt. Joseph T. Sanguila, Sr. &nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;Jan. 1960 to 1963 &nbsp;
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;8.&nbsp; Dr. Maximo P. Arnado, Sr. &nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;Feb. 15,11979 to Apr.15,1986
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  &nbsp;9.&nbsp;Maj. Valentine E. Tarroza, Sr.&nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;May 1974 to Feb. 12, 1979&nbsp;
                </td>
                <td style="width: 119px; height: 33px;">
                  &nbsp;Appointed
                </td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  10. Dr. maximo P. Arnado Sr.
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;Feb.15,1979 to Apr. 115,1986
                </td>
                <td style="width: 119px; height: 33px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 33px;">
                <td style="width: 298px; height: 33px;">
                  11. Atty. Myron B. Rico &nbsp;
                </td>
                <td style="width: 282px; height: 33px;">
                  &nbsp;April 15,1986 to Dec. 1, 1987
                </td>
                <td style="width: 119px; height: 33px;">
                  &nbsp;Des. OIC
                </td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  12. Mr. Joseph M. Sanguila,Jr. &nbsp;&nbsp;
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;Dec. 2, 1987 to Jan. 3,1988&nbsp;
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;Desig.</td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  13. Mr. Pantaleon T. Hontiveros,Sr.
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;Jan. 4, 1988 to Feb. 10, 1988
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;- do -</td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  14. Atty. Myron B. Rico &nbsp;
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;Feb. 11,1988 to June 1998&nbsp;
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  <p>15. Hon. Mohammad Moamar &nbsp;Jack S. Maruhom</p>
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;July 1, 1998 &ndash; May 11, 2007
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  16. Hon. Yasser Hadji Hasan Samporna&nbsp;
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;June 30, 2007 - June 30, 2010
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;Elected</td>
              </tr>
              <tr style="height: 34px;">
                <td style="width: 298px; height: 34px;">
                  17. Hon. Rommel C. Arnado &nbsp;
                </td>
                <td style="width: 282px; height: 34px;">
                  &nbsp;July 1, 2010 to present
                </td>
                <td style="width: 119px; height: 34px;">&nbsp;Elected</td>
              </tr>
            </tbody>
          </table>
          <!-- DivTable.com -->
        </div>
      </div>
    </div>
    <!--end of row-->
  </div>
  <!--end of container-->
</section>

<section class="space--xs bg--primary">
  <div class="container">
    <div class="cta cta--horizontal text-center-xs row">
      <div class="col-md-4">
        <h4>Uswag Turismo Kauswaganon</h4>
      </div>
      <div class="col-md-5">
        <p class="lead">
          Home of the annual HUGYAW SA KADAGATAN
        </p>
      </div>
      <div class="col-md-3 text-right text-center-xs">
        <a
        class="btn btn--primary type--uppercase"
        href="https://www.facebook.com/uswagturismokauswaganon/"
        >
        <span class="btn__text">
          Learn More
        </span>
      </a>
    </div>
  </div>
</div>
<!--end of container-->
</section>


@endsection



@section('footer')
@endsection
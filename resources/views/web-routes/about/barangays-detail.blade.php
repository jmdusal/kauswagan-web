@extends('welcome')

@section('title')
Municipality of Kauswagan - Barangays Details
@endsection

@section('content')
<div class="main-container">
  <section class="text-center">
    <div class="container">
      <div class="col-md-8 col-lg-10">
        <article class="masonry__item">
          <!--end article title-->
          <div class="article__body">


            <img alt="Image" src="{{ asset('storage/barangays_images')}}/{{$barangaydata->display_image}}"/>
            
            
          </div>

          <div class="article__title text-center mb-0">
            <h2>{{$barangaydata->barangay_name}}</h2>
          </div>
          <p>{!!$barangaydata->description!!}</p>
        </div>
      </article>
    </div>
  </div>
  <!--end of container-->
</section>





@if(count($barangaydata->barangaysofficials) != 0)
<section class="text-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8">
        <h2>Barangay Officials</h2>
      </div>
    </div>
  </div>
</section>
@else

@endif
<section class="text-center">
  <div class="container">
    <div class="row">
      @foreach($barangaydata->barangaysofficials as $data)
      <div class="col-md-4">
        <div class="feature feature-8">
          <img alt="Image" src="{{ asset('storage/barangay_officials_images')}}/{{$data->display_image}}"/>
          <h5> {{$data->name}}</h5>
          <span>{{$data->position}}</span>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>





<section class="space--xs bg--primary">
  <div class="container">
    <div class="cta cta--horizontal text-center-xs row">
      <div class="col-md-4">
        <h4>Uswag Turismo Kauswaganon</h4>
      </div>
      <div class="col-md-5">
        <p class="lead">
          Home of the annual HUGYAW SA KADAGATAN
        </p>
      </div>
      <div class="col-md-3 text-right text-center-xs">
        <a
        class="btn btn--primary type--uppercase"
        href="https://www.facebook.com/uswagturismokauswaganon/"
        >
        <span class="btn__text">
          Learn More
        </span>
      </a>
    </div>
  </div>
</div>
<!--end of container-->
</section>

@endsection



@section('footer')
@endsection
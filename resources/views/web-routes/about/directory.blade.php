@extends('welcome')

@section('title')
Municipality of Kauswagan - Directories
@endsection



@section('content')

<div class="main-container">
  <section class="cover imagebg height-80 text-center" data-overlay="3" style="padding: 0px">

    <div id="googleMap" style="width:100%;height:100%;"></div>
</section>

<section>
    <div class="row justify-content-center">
      <div class="col-md-4" align="center">
        <a href="https://www.google.com/maps/place/Kauswagan+Municipal+Hall,+Kauswagan,+Lanao+del+Norte/@8.1894212,124.0844983,17z/data=!3m1!4b1!4m5!3m4!1s0x3255a0e006194cc9:0x8994e3755f9c3779!8m2!3d8.1893921!4d124.0866743"  style="text-decoration: none; color: inherit;"  target="__blank">
          <i class="icon--lg icon-Map-Marker" style="color: green"></i>  <h3>&nbsp;Kauswagan, Lanao del Norte </h3>
      </a>

  </div>

  <div class="col-md-4" align="center">
    <a href="mailto: info.kauswagan@gmail.com" style="text-decoration: none; color: inherit;">
      <i class="icon--lg icon-Email" style="color: green"></i><h3>info.kauswagan@gmail.com</h3>
  </a>


</div>

<div class="col-md-4" align="center">
    <a href="https://www.facebook.com/kauswaganmunicipality/" target="__blank" style="text-decoration: none; color: inherit;">
      <i class="icon--lg icon-Facebook" style="color: green"></i><h3>@kauswaganmunicipality</h3>
  </a>


</div>
</div>
</section>

<section class="text-center space--xs">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Directories</h2>
      </div>
  </div>
  <!--end of row-->
</div>
<!--end of container-->
</section>

<section>
    <div class="row justify-content-center" style="padding-left: 20px; padding-right: 20px">

        @foreach($directories as $key => $directory)

        <div class="col-md-3">
            <div class="feature boxed boxed--border border--round text-center">
                <h3 class="color--primary" style="font-weight: 500; margin-bottom: 6px">{{ $directory->directory_name}}</h3>
                {!! $directory->description !!}
            </div>
        </div>

        @endforeach


    </div>
</section>
<section class="space--xs bg--primary">
    <div class="container">
        <div class="cta cta--horizontal text-center-xs row">
            <div class="col-md-4">
                <h4>Uswag Turismo Kauswaganon</h4>
            </div>
            <div class="col-md-5">
                <p class="lead">
                    Home of the annual HUGYAW SA KADAGATAN
                </p>
            </div>
            <div class="col-md-3 text-right text-center-xs">
                <a class="btn btn--primary type--uppercase" href="https://www.facebook.com/uswagturismokauswaganon/">
                    <span class="btn__text">
                        Learn More
                    </span>
                </a>
            </div>
        </div>
    </div>
    <!--end of container-->
</section>

<script>
  function myMap() {


    var mapProp= {
      center:new google.maps.LatLng( 8.1894,124.0867 ),
      zoom:17,
  };
  var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);


  var marker = new google.maps.Marker({
      position: { lat: 8.1894, lng: 124.0867 },
      animation:google.maps.Animation.BOUNCE,
      title: "Hello World!"
  });

  marker.setMap(map);
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDii5Ge6RRt0XB710mymDHA-uK3TfgE3wQ&callback=myMap"></script>




@endsection

@section('footer')


@endsection

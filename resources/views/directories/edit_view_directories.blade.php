@extends('user.header')

@section('title')
Edit Directory
@endsection

@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      @foreach($errors->all() as $error)
      <strong>{{$error}}</strong>
      @endforeach
    </div>
    @endif 
    

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">

                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                               <!--  <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href=""> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ asset('/awards/table')}}">Awards Table</a>
                                    </li>
                                    <li class="breadcrumb-item">Edit</li>
                                </li>
                              </ul> -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="page-body">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="card">
                              <div class="card-header">
                                <div class="card-header-right">
                                </div>
                              </div>

                              <div class="card-block">
                                <h4 class="sub-title">Edit Directory Data</h4>
                                
                                <form method="post" action="{{url('/directories', $data->id)}}" enctype="multipart/form-data" class="form_submit">
                                  {{csrf_field()}}
                                  <input type="hidden" name="_method" value="PATCH">
                                  

                                  <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Directory Name</label>
                                    <div class="col-sm-10">
                                      <input type="text" name="directory_name" class="form-control" value="{{$data->directory_name}}" required="True">
                                    </div>
                                  </div>



                                  <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                      <textarea name="description" rows="12" cols="7" class="form-control">{{$data->description}}</textarea>
                                    </div>
                                  </div>



                                  <br>

                                  <div class="form-group">
                                    <input type="submit" class="btn btn-success" value="Edit">
                                  </div>
                                </form>
                                


                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




  <script type="text/javascript">

    $(".form_submit").submit(function(e) {

      e.preventDefault();

      swal({
        html:   '<div class="loader-block">'+
        '<img src="'+'{{ asset("design/website/img/loading.gif")}}'+'" width="150" height="150">'+
        '</div>'+
        '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
        '<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
        allowOutsideClick: false,
        showConfirmButton:false,
      });

      setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);

      setTimeout(function(){

        $('.form_submit').ajaxSubmit({
          beforeSubmit: function(){
            $('.progress-bar').width('0%')
          },
          uploadProgress: function(event, position, total, percentComplete){

            $('#progress-status').text(percentComplete+'%');
          },            
          error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          
          swal({
            title:"Error!",
            text: "Please check some input fields!",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#d33',
            confirmButtonText: "Ok",
          }).then(function (e) {

            if (e.value === true) {
              setTimeout(function(){
               location.reload(true);
             }, 1000);

            } else {
              e.dismiss;
            }

          }, function (dismiss) {
            return false;
          })

          console.log(xhr.responseText);
        },
        success: function(results){

          console.log(results);

          if(results.success == true){

            $('.progress-bar').width('0%').html('');

            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });
            setTimeout(function(){
              window.location.href = '{{ url("/directories/table")}}';
            }, 1000);
          }else{

            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1000);
          }

        },
        resetForm: true
      });

      }, 1500);
    });

    </script
    @endsection
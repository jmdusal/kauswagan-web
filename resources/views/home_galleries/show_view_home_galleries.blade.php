@extends('user.header')

@section('title')
View Home Gallery
@endsection

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        @if(count($errors) > 0)
        <div class="alert alert-danger background-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="icofont icofont-close-line-circled text-white"></i>
            </button>
            @foreach($errors->all() as $error)
            <strong>{{$error}}</strong>
            @endforeach
        </div>
        @endif
        
        @if($message = Session::get('success'))
        <div class="alert alert-success background-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="icofont icofont-close-line-circled text-white"></i>
            </button>
            <strong>{{$message}}</strong>
        </div>
        @endif

        
        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
<!-- <h4>User Profile</h4>
    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
</div>
</div>
</div>
<div class="col-lg-4">
    <div class="page-header-breadcrumb">
        <!-- <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <i class="feather icon-home"></i>
            </li>
            <li class="breadcrumb-item">Home Galleries Table
            </li>
            <li class="breadcrumb-item">View</li>
        </li>
    </ul> -->
</div>
</div>
</div>
</div>






<div class="page-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="cover-profile">
               <div class="profile-bg-img">
                <img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('storage/home_galleries_images')}}/{{$homegallery->display_image}}" alt="bg-img">

                <div class="card-block user-info">
                    <div class="col-md-12">

                        <div class="media-body row">
                            <div class="col-lg-12">
                                <div class="user-title">
                                    <h1 style="color: white;">{{$homegallery->title}}</h1>
                                    <h5 style="color: white;">{{$homegallery->date_taken}}</h5>

                                </div>
                            </div>
                            <div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">

        <!-- <div class="card business-info services m-b-20"> -->
            <div class="tab-header card business-info services m-b-20">
                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Home Gallery Info</a>
                        <div class="slide"></div>
                    </li>

                </li>
            </ul>
        </div>



        <div class="tab-content">
            <div class="tab-pane active" id="personal" role="tabpanel">


                <div class="card business-info services m-b-20">
                    <div class="card-header">
                        <h5 class="card-header-text">Details</h5>
                    </div>
                    <div class="card-block">
                        <div class="view-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>

                                                            <tr>
                                                                <th scope="row">Title</th>
                                                                <td>{{$homegallery->title}}</td>
                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Date</th>
                                                                <td>{{$homegallery->date_taken}}</td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>



                                            <div class="col-lg-12 col-xl-6">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>

                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    




                    <div class="edit-info">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="general-info">
                                    <div class="row">
                                        <div class="col-lg-6">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

@endsection
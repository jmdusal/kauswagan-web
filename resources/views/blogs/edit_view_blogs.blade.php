@extends('user.header')

@section('title')
Edit Blogs
@endsection

@section('content')
<div class="pcoded-content">
<div class="pcoded-inner-content">
    @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        @foreach($errors->all() as $error)
        <strong>{{$error}}</strong>
        @endforeach
    </div>
    @endif 
        

    <div class="main-body">
        <div class="page-wrapper">
        
        <div class="page-header">
        <div class="row align-items-end">
        <div class="col-lg-8">
        <div class="page-header-title">
        <div class="d-inline">
        
        </div>
        </div>
        </div>
        <div class="col-lg-4">
        <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
        <li class="breadcrumb-item">
        <a href=""> <i class="feather icon-home"></i> </a>
        </li>
        <li class="breadcrumb-item"><a href="{{ asset('/blogs')}}">Stories Table</a>
        </li>
        <li class="breadcrumb-item">Edit</li>
        </li>
        </ul>
        </div>
        </div>
        </div>
        </div>
        <div class="page-body">
        <div class="row">
        <div class="col-sm-12">
        <div class="card">
        <div class="card-header">
        <div class="card-header-right">
        </div>
        </div>

        <div class="card-block">
        <h4 class="sub-title">Edit Story Data</h4>
       
        <form method="post" action="{{ url('/blogs', $data->id) }}" enctype="multipart/form-data">
            {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH">
        

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
                <input type="text" name="title" class="form-control" value="{{$data->title}}" required="True">
            </div>
        </div>
    
    
    
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Categories</label>
            <div class="col-sm-10">
                <select name="category_id" class="form-control">
                    <option value="">Select One Value Only</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" {{$category->id == $data->category_id ? 'selected' : ''}}>{{$category->category_name}}</option>
                    @endforeach
                    </select>
                </div>
            </div>



        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Authors</label>
            <div class="col-sm-10">
                <select name="author_id" class="form-control">
                    <option value="">Select One Value Only</option>
                    @foreach($authors as $author)
                    <option value="{{$author->id}}" {{$author->id == $data->author_id ? 'selected' : ''}}>{{$author->name}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
    
    
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Upload Image File</label>
            <div class="col-sm-10">
                <input type="file" name="display_image" class="form-control">
                <img src="{{ asset('storage/blogs_images')}}/{{$data->display_image}}" class="img-thumbnail" width="100" />
                <input type="hidden" name="hidden_image" value="{{ $data->display_image}}" />
            </div>
        </div>
    


        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea name="description" required="True" rows="12" cols="7" class="form-control">{{$data->description}}</textarea>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Edit">
        </div>
        </form>
        


        </div>
        </div>
        </div>
        </div>
        
        
        
        
        </div>
        </div>
        </div>
        
        </div>
        </div>
        
        <div id="styleSelector">
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>

@endsection
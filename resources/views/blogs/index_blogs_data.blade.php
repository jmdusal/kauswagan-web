@extends('user.header')

@section('title')
Blogs Table
@endsection

@section('content')


<div class="pcoded-content">
  <div class="pcoded-inner-content">
    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong>{{$message}}</strong>
    </div>
    @endif  
    
    

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">
                  <a href="{{url('/blogs/add')}}" class="btn btn-primary">Add Blog</a>

                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                    <a href="index.html"> <i class="feather icon-home"></i> </a>
                  </li>
                  <li class="breadcrumb-item">Data Table</li>
                  <li class="breadcrumb-item">Blogs Table</li>
                </ul>
              </div>
            </div>
          </div>
        </div>


        <div class="page-body">


          <div class="card">
            <div class="card-header">
              <h4>Blogs Data</h4>

            </div>
            <div class="card-block">
              <div class="dt-responsive table-responsive">
                <table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
                  <thead>
                    <tr>

                      <th width="1%">Image</th> 
                      <th>Title</th>
                      <th>Flagship</th>
                      <th>Category</th>
                      <th>Author</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $row)
                    <tr>
                      <!-- <td><img src="{{ URL::to('/') }}/images/story_images/{{ $row->display_image }}" class="img-thumbnail" width="50"/></td> -->

                      <td><img src="{{ asset('storage/blogs_images')}}/{{$row->display_image}}" class="img-thumbnail" width="50"></td>
                      <td>{{$row->title}}</td>
                      <td>{{$row->is_flagship}}</td>
                      <td>{{$row->category_name}}</td>
                      <td>{{$row->name}}</td>
                      <td>
                        <div class="dropdown">
                          <button class="btn btn-primary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action&nbsp;<i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/blogs/view', $row->id)}}">View</a>
                            <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/blogs/edit', $row->id)}}" class="btn btn-primary">Edit</a>
                            <a style="color:black; font-weight: bold;" data-toggle="modal" data-target="#myModal" class="dropdown-item" href="">Delete</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                    

                    <form role="form" method="post" action="" class="delete_form">
                      {{csrf_field()}}
                      <div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <center>
                                <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete this data?</h4>
                              </center>
                            </button>
                          </div>

                          
                          <div class="modal-footer">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">Confirm</button>
                            <button type="button" class="btn btn-process" data-dismiss="modal">Cancel</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <script>
                    $($document).ready(function(){
                      $('.delete_form').on('submit', function(){
                        if(confirm("Are you sure you want to delete this data?"))
                        {
                          return true;
                        }
                        else
                        {
                          return false;
                        }
                      });
                    });
                  </script>
                  @endforeach
                </tbody>


              </table>

              <script>
                $(document).ready( function () {
                  $('.simpletable').DataTable();
                });
              </script>

            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
  <div id="styleSelector">
  </div>
</div>
</div>
</div>
</div>
</div>
</div>





@endsection
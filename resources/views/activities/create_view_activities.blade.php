@extends('user.header')

@section('title')
Add Activity
@endsection

@section('content')

@section('stylesheets')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init([
        selector: 'textarea'
    ]);
</script>
@endsection




<div class="pcoded-content">
    <div class="pcoded-inner-content">
        @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        @foreach($errors->all() as $error)
        <strong>{{$error}}</strong>
        @endforeach
    </div>
    @endif

    <div class="main-body">
    <div class="page-wrapper">
    
    <div class="page-header">
    <div class="row align-items-end">
    <div class="col-lg-8">
    <div class="page-header-title">
    <div class="d-inline">
    
    
    </div>
    </div>
    </div>
    <div class="col-lg-4">
    <div class="page-header-breadcrumb">
    <ul class="breadcrumb-title">
    <li class="breadcrumb-item">
    <a href="index.html"> <i class="feather icon-home"></i> </a>
    </li>
     <li class="breadcrumb-item"><a href="{{ asset('/activities')}}">Activities Table</a>
    </li>
    <li class="breadcrumb-item">Create</li>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    
    
    <div class="page-body">
    <div class="row">
    <div class="col-sm-12">
    
    <div class="card">
    <div class="card-header">
    
    <div class="card-header-right">
    
    </div>
    </div>
    
    <div class="card-block">
    <h4 class="sub-title">Add Activities</h4>
    <form role="form" method="post" id="submit" action="{{url('/activities/activitiesdata/submit')}}" enctype="multipart/form-data">
        {{csrf_field()}}


    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-10">
    <input type="text" name="title" class="form-control" required="True">
    </div>
    </div>


    
    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Flagship</label>
     <div class="col-sm-10">
    <select name="is_flagship" class="form-control" required="True">
    <option value="">Select One Value Only</option>
    <option value="True">True</option>
    <option value="False">False</option>
    </select>
    </div>
    </div>


    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Categories</label>
         <div class="col-sm-10">
    <select name="category_id" class="form-control" required="True">
    <option value="">Select One Value Only</option>
    @foreach ($categories as $loop => $value)
    <option value="{{$value->id}}">{{$value->category_name}}</option>
    @endforeach
    </select>
    </div>
    </div>


    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Authors</label>
        <div class="col-sm-10">
    <select name="author_id" class="form-control" required="True">
    <option value="">Select One Value Only</option>
    @foreach ($authors as $loop => $value)
    <option value="{{$value->id}}">{{$value->name}}</option>
    @endforeach
    </select>
    </div>
    </div>


    <input type="hidden" name="input_by" value="{{Auth::user()->name}}" class="form-control">
    
    
    
    
    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Upload Image File</label>
    <div class="col-sm-10">
    <input type="file" name="display_image" class="form-control">
    </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea name="description" rows="12" cols="7" class="form-control"></textarea>
        </div>
        <br>
    </div>
    <button type="create" class="btn btn-primary">Submit</button>
    </form>
    </div>
    </div>
    </div>
    </div>
    
    <div id="styleSelector">
    </div>
    
    
    </div>
    </div>
    </div>
    
    </div>
    </div>
    
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection

<!-- <script>
    $("#submit").submit(function(e) {
            e.preventDefault();

          Swal.fire({
              html:   '<div class="loader-block">'+
              '<img src="'+site_url+'/assets_for_new_admin/loading_spinner.gif" width="150" height="150">'+
              '</div>'+
              '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
              '<small><span id="TakeWhile" hidden> This may take a while, depends on image size and internet speed</span></small>',    
              allowOutsideClick: false,
              showConfirmButton:false,
          });
          setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
          setTimeout(function(){
            $('#submit').ajaxSubmit({
                beforeSubmit: function(){
                    $('.progress-bar').width('0%')
                },
                uploadProgress: function(event, position, total, percentComplete){
                // $('.progress-bar').width(percentComplete+'%');
                // $('.progress-bar').html('<div id="progress-status">'+percentComplete+'%</div>');
                $('#progress-status').text(percentComplete+'%');
            },
            success: function(data){
                if(data == '0'){
                    Swal.fire({
                        title: 'Error',
                        html: 'Book is not added. Please check the fields and try again',
                        type: 'error'
                    });
                }else{
                    $('.progress-bar').width('0%').html('');
                    Swal.fire({
                        title: 'Success',
                        html: 'Book is added successfully.',
                        type: 'success'
                    });
                }
                $('#logo_image').prop('hidden',true);
            },
            resetForm: true
        });
        }, 1500);
      });
</script> -->
@extends('user.header')

@section('title')
View Activity
@endsection

@section('content')

<div class="pcoded-content">
<div class="pcoded-inner-content">

    @if(count($errors) > 0)
    <div class="alert alert-danger background-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        @foreach($errors->all() as $error)
        <strong>{{$error}}</strong>
        @endforeach
    </div>
    @endif
            
    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong>{{$message}}</strong>
    </div>
    @endif

        
<div class="main-body">
<div class="page-wrapper">



<div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<!-- <h4>User Profile</h4>
<span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="index.html"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="{{ asset('/activities')}}">Activities Table</a>
</li>
<li class="breadcrumb-item">View</li>
</li>
</ul>
</div>
</div>
</div>
</div>






<div class="page-body">
<div class="row">
<div class="col-lg-12">
<div class="cover-profile">
 <div class="profile-bg-img">
<img style="height: 350px; object-fit: cover; width: 100%;" class="profile-bg-img img-fluid" src="{{ asset('storage/activities_images')}}/{{$activity->display_image}}" alt="bg-img">

<div class="card-block user-info">
<div class="col-md-12">


<div class="media-body row">
<div class="col-lg-12">
<div class="user-title">
<h1 style="color: white;">{{$activity->title}}</h1>
<h5 style="color: white;">{{$activity->created_at}}</h5>

</div>
</div>
<div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">

<div class="tab-header card">
<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
<li class="nav-item">
<a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Activity Info</a>
<div class="slide"></div>
</li>
<li class="nav-item">
<a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Gallery</a>
<div class="slide"></div>
</li>
</ul>
</div>


<div class="tab-content">
<div class="tab-pane active" id="personal" role="tabpanel">

<div class="card">
<div class="card-header">
<h5 class="card-header-text">Details</h5>
</div>
<div class="card-block">
<div class="view-info">
<div class="row">
<div class="col-lg-12">
<div class="general-info">
<div class="row">
<div class="col-lg-12 col-xl-6">
<div class="table-responsive">
<table class="table m-0">
<tbody>
<tr>
<th scope="row">Title</th>
<td>{{$activity->title}}</td>
</tr>
<tr>
<th scope="row">Feature</th>
<td>{{$activity->is_flagship}}</td>
</tr>
<tr>
<th scope="row">Category</th>
@foreach($categories as $category)
<td {{$category->id == $activity->category_id}}>{{$activity->category->category_name}}</td>
@endforeach
</tr>
<tr>
<th scope="row">Author</th>
@foreach($authors as $author)
<td {{$author->id == $activity->author_id}}>{{$activity->author->name}}</td>
@endforeach
</tr>

</tbody>
</table>
</div>
</div>



<div class="col-lg-12 col-xl-6">
<div class="table-responsive">
<table class="table">
<tbody>
<th scope="row">Created at</th>
<td>{{$activity->created_at}}</td>
</tr>
<tr>
<th scope="row">Updated at</th>
<td>{{$activity->updated_at}}</td>
</tr>


</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="edit-info">
<div class="row">
<div class="col-lg-12">
<div class="general-info">
<div class="row">
<div class="col-lg-6">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h5 class="card-header-text">Description</h5>

</div>
<div class="card-block user-desc">
<div class="view-desc">
<p>{!! $activity->description !!}</p>
</div>

</div>
</div>
</div>
</div>

</div>


<div class="tab-pane" id="binfo" role="tabpanel">

<div class="card">
<div class="card-header">
<h5 class="card-header-text">Galleries</h5><br><br>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Image</button>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div class="card b-l-success business-info services m-b-20">
<div class="card-header">
<div class="service-header">
<a href="#">

</a>
</div>


</div>
<div class="card-block">
<div class="row">
<div class="col-sm-12">
@foreach($activity->activitiesgalleries as $data)
<img src="{{ asset('storage/galleries/activities')}}/{{$data->display_image}}" class="img-thumbnail" width="150"/>
@endforeach



</div>
</div>
</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
 </div>
</div>
</div>



</div>
</div>
</div>

</div>
</div>

<div id="styleSelector">
</div>
</div>
</div>
</div>
</div>
</div>
</div>

    <form role="form" method="post" action="{{url('/activitiesgallery/submit')}}" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <center>
    <h4 class="modal-title" id="myModalLabel">Upload File</h4>
    </center>
    </button>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12 text-center">
    <div class="icon-list-demo">
    <!-- <i id="icon" class="fa fa-wpbeginner fa-lg"></i> -->
    </div>
    </div>
    <div class="col-md-12">
    
    <div class="form-group row">
    <!-- <label class="col-sm-2 col-form-label">Stories</label> -->
    <div class="col-sm-10">
    <input type="hidden" name="activity_id" value="{{$activity->id}}" class="form-control">
    <input name="invisible" type="hidden" value="secret">
    </div>
    </div>

    <div class="form-group row">
    <label class="col-sm-2 col-form-label">Upload Image</label>
    <div class="col-sm-10">
    <input type="file" name="display_image" class="form-control">
    </div>
    </div>

    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="create" class="btn btn-primary">Send</button>
    <button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
     </form>
    </div>
    </div>
    </div>
    </div>
@endsection
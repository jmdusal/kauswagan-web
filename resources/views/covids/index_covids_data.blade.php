@extends('user.header')
@section('title')
Covid Table
@endsection

@section('stylesheet')
<style type="text/css">
  .table td{
    max-width: 200px; 
    min-width: 70px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

  }
</style>
@endsection

@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong>{{$message}}</strong>
    </div>
    @endif
    
    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">
                  <a href="{{url('/covid-news/add')}}" class="btn btn-success">Add Covid News</a>

                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                <!-- <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                    <i class="feather icon-home"></i>
                  </li>
                  <li class="breadcrumb-item">Data Table</li>
                  <li class="breadcrumb-item">Stories Table</li>
                </ul> -->
              </div>
            </div>
          </div>
        </div>



        <div class="page-body">
          <div class="card">
            <div class="card-header">
              <h4>Covid</h4>
              
            </div>
            <div class="card-block">
              <div class="dt-responsive table-responsive">
                <table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
                  <thead>
                    <tr>
                      <th width="1%">Image</th> 
                      <th>Title</th>
                      <th>Author</th>
                      <th class="text-center">Date Created</th>
                      <th class="text-center">Action</th>
                      <!-- <th>Delete</th> -->
                    </tr>
                  </thead>
                  <tbody>

                    @foreach($data as $row)
                    <tr>

                      <td>
                        <a href="{{ asset('storage/covids_images')}}/{{$row->display_image}}" data-lightbox="gallery">
                          <img src="{{ asset('storage/covids_images')}}/{{$row->display_image}}" class="img-thumbnail" width="200">
                        </a>
                      </td>
                      <td>{{$row->title}}</td>
                      <td>{{$row->name}}</td>
                      <td class="text-center">                                     
                        <!-- {{$row->date_created}} -->
                        <?php
                        $orig_date =  explode('-', $row->date_created);
                        $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
                        echo date("M d, Y", strtotime($con_date));
                        ?>

                      </td>



                      <td class="text-center">
                        <button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="feather icon-chevron-down"></i></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/covids/view', $row->id)}}">View</a>
                          <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/covids/edit', $row->id)}}" class="btn btn-success">Edit</a>
                          <a onclick="deleteConfirmation({{$row->id}})" style="color:black; font-weight: bold;" class="dropdown-item">Delete</a>
                        </div>
                      </td>

                    </tr>
                    @endforeach

                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>

        <br>





      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>
</div>



<script type="text/javascript">
  function deleteConfirmation(id) {
    swal({
      title:"Are you sure you want to delete this?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      cancelButtonText: "No, cancel!",
      confirmButtonText: "Yes, delete it!",
    }).then(function (e) {
      if (e.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'POST',
          url: "{{url('/deletecovid')}}/" + id,
          data: {_token: CSRF_TOKEN},
          dataType: 'JSON',
          error: function (xhr, status, errorThrown) {
              //Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
            },
            success: function (results) {
              if (results.success === true) {
                swal({
                  title: "Done!",
                  text: results.message,
                  type: "success",
                  showConfirmButton: false
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);
              } else {
                swal({
                  title: "Error!",
                  text: results.message,
                  type: "error"
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);
              }
            }
          });

      } else {
        e.dismiss;
      }

    }, function (dismiss) {
      return false;
    })
  }
</script>

@endsection
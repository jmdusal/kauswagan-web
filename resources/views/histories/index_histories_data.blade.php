@extends('user.header')
@section('title')
Histories Table
@endsection
@section('stylesheet')
<style type="text/css">
.table td{
  max-width: 200px; 
  min-width: 20px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

td, th {
  text-align: center;
  vertical-align: middle;

}
</style>
@endsection
@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    @if($message = Session::get('success'))
    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong>{{$message}}</strong>
    </div>
    @endif

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">
                  <a href="{{url('/histories/add')}}" class="btn btn-success">Add History</a>

                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                <!-- <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                    <a href="index.html"> <i class="feather icon-home"></i> </a>
                  </li>
                  <li class="breadcrumb-item">Data Table</li>
                  <li class="breadcrumb-item">Awards Table</li>
                </ul> -->
              </div>
            </div>
          </div>
        </div>

        <div class="page-body">
          <div class="card">
            <div class="card-header">
              <h4>Histories</h4>

            </div>
            <div class="card-block">
              <div class="dt-responsive table-responsive">
                <table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
                  <thead>
                    <tr>

                      <th>Title</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>


                    @foreach($data as $row)
                    <tr>
                      <td style="min-width: 100%">{{$row->title}}</td>
                      <td>{!!$row->description!!}</td>

                      <td class="text-center">
                        <button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="feather icon-chevron-down"></i></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a style="color:black; font-weight: bold;" class="dropdown-item" href="{{url('/histories/edit', $row->id)}}" class="btn btn-success">Edit</a>
                          <a onclick="deleteConfirmation({{$row->id}})" style="color:black; font-weight: bold;" class="dropdown-item">Delete</a>
                        </div>
                        
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
</div>
</div>
</div>
</div>





<script type="text/javascript">
  function deleteConfirmation(id) {
    swal({
      title:"Are you sure you want to delete this?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      cancelButtonText: "No, cancel!",
      confirmButtonText: "Yes, delete it!",
    }).then(function (e) {

      if (e.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'POST',
          url: "{{url('/deletehistory')}}/" + id,
          data: {_token: CSRF_TOKEN},
          dataType: 'JSON',
          error: function (xhr, status, errorThrown) {
              //Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
            },
            success: function (results) {
              if (results.success === true) {
                swal({
                  title: "Done!",
                  text: results.message,
                  type: "success",
                  showConfirmButton: false
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);
              } else {
                swal({
                  title: "Error!",
                  text: results.message,
                  type: "error"
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1000);
              }
            }
          });

      } else {
        e.dismiss;
      }

    }, function (dismiss) {
      return false;
    })
  }
</script>

@endsection
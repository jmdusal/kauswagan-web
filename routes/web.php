<?php

// use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Input;
// use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Story;


// Route::get('/', function(){

//     return view('welcome-body');
// });


Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('userdashboard')->middleware('auth');
// Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Route::get('logout', 'Auth\LoginController@logout');

Route::get('/logout', 'Auth\LoginController@logout');




// Route::group(['middleware' => ['guest']], function ()
// Route::middleware(['guest'])->group(function ()
// Route::group(['middleware' => ['guest']], function ()
// Route::group(['middleware' => ['auth', 'guest']], function() 


// Route::group(['middleware' => ['web','auth']], function ()
// Route::middleware(['guest'])->group(function ()
// {


// Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
// Route::get('/dashboard', 'HomeController@index')->name('userdashboard')->middleware('auth');


// About dropdown pages route
Route::get('/', 'WebController@homepage')->name('homepage');

Route::get('/office-mayor', 'WebController@officemayor_about_page')->name('mayor.office.page');
Route::get('/organizational-chart', 'WebController@organizationalchartpage')->name('organizational.chart.page');
Route::get('/directory', 'WebController@directorypagedata')->name('directory.page');
Route::get('/barangays', 'WebController@barangayspagedata')->name('barangays.page');
Route::get('/barangays-detail/{id}', 'WebController@barangaydetailpage')->name('barangays.detail.page');


Route::get('/history', 'WebController@historypagedata')->name('history.page');
// Stories & News dropdown pages route
Route::get('/stories', 'WebController@homepagestoryalldata')->name('stories.page');
Route::any('/stories/search', 'WebController@searchpaginatestories')->name('search.stories.paginate');
Route::get('/story-detail/{id}', 'WebController@homepagestorydetail')->name('homepage.story.detail');


// Route::get('/stories-detail', 'WebController@storiesdetailspage');
Route::get('/news', 'WebController@homepagenewalldata')->name('homepage.news.details');
Route::any('/news/search', 'WebController@searchpaginatenews')->name('search.news.paginate');
Route::get('/news-detail/{id}', 'WebController@homepagenewsdetail')->name('homepage.news.detail');
// Route::get('/news-detail', 'WebController@newsdetailspage');

Route::get('/gallery', 'WebController@homepagegalleriesalldata')->name('homepage.galleries');


Route::get('/albums', 'WebController@homepagealbumsalldata')->name('homepage.albums');
Route::any('/albums/search', 'WebController@searchpaginatealbums')->name('search.albums.paginate');

Route::get('/covid-updates', 'WebController@homepagecovidupdatesalldata')->name('homepage.covid.updates');
Route::get('/covid-detail/{id}', 'WebController@coviddetailpage')->name('covid.details');
Route::any('/covids/search', 'WebController@searchpaginatecovid')->name('search.covids.paginate');


Route::get('/bids-and-awards', 'WebController@homepagebidsandawardsalldata')->name('homepage.awards');
Route::get('/videos', 'WebController@videosalldata')->name('homepage.video');
Route::any('/videos/search', 'WebController@searchpaginatevideos')->name('search.video.paginate');
// Transparency route
Route::get('/transparency', 'WebController@transparencypage')->name('transparency.page');
// Covid-19 update route
// Route::get('/covid-updates', 'WebController@covidupdatespage')->name('covidupdates.page');
// });



Route::get('/change-password', 'AllSystemController@getuserdata');
Route::post('/change-password', 'AllSystemController@userchangepassword')->name('changePassword');

// Route::group(['middleware' => ['auth']], function ()
    // Route::middleware(['auth'])->group(function ()
    // {
Route::get('authors/table', 'AllSystemController@indexauthorsdata')->name('authors.table');
Route::get('/authors/add', 'AllSystemController@createviewauthors')->name('create.authors');
Route::post('/authors/authorsdata/submit', 'AllSystemController@submitauthors')->name('submit.authors.data');
Route::get('/authors/view/{id}', 'AllSystemController@showviewauthors')->name('view.authors.data');
Route::get('/authors/edit/{id}', 'AllSystemController@editviewauthors')->name('edit.authors');
Route::patch('/authors/{id}', 'AllSystemController@updateauthordata')->name('update.authors.data');
Route::post('/deleteauthor/{id}', 'AllSystemController@deleteauthordata')->name('delete.author.data');


Route::get('stories/table', 'AllSystemController@indexstoriesdata')->name('stories.table');
Route::get('/stories/add', 'AllSystemController@createviewstories')->name('create.stories');
Route::post('/stories/storiesdata/submit', 'AllSystemController@submitstories')->name('submit.stories.data');
Route::get('/stories/view/{id}', 'AllSystemController@showviewstories')->name('view.story.data');
Route::get('/stories/edit/{id}', 'AllSystemController@editviewstories')->name('edit.stories');
Route::patch('/stories/{id}', 'AllSystemController@updatestorydata')->name('update.story.data');
Route::post('/deletestory/{id}', 'AllSystemController@deletestorydata')->name('delete.story.data');
Route::any('/stories/unfeature/{id}', 'AllSystemController@unfeaturestories')->name('unfeature.stories');







Route::get('covids/table', 'AllSystemController@indexcovidsdata')->name('covids.table');
Route::get('/covid-news/add', 'AllSystemController@createviewcovids')->name('create.covids');
Route::post('/covids/covidsdata/submit', 'AllSystemController@submitcovids')->name('submit.covids.data');
Route::get('/covids/view/{id}', 'AllSystemController@showviewcovids')->name('view.covid.data');
Route::get('/covids/edit/{id}', 'AllSystemController@editviewcovids')->name('edit.stories');
Route::patch('/covids/{id}', 'AllSystemController@updatecoviddata')->name('update.covid.data');
Route::post('/deletecovid/{id}', 'AllSystemController@deletecoviddata')->name('delete.covid.data');



Route::post('/covidsgallery/submit', 'AllSystemController@submitcovidsgallery')->name('add.covids.galleries');

Route::post('/deletecovidsgallery/{id}', 'AllSystemController@deletecovidsgallery')->name('delete.covid.gallery.data');





Route::get('albums/table', 'AllSystemController@indexalbumsdata')->name('albums.table');
Route::get('/albums/add', 'AllSystemController@createviewalbums')->name('create.albums');
Route::post('/albums/albumsdata/submit', 'AllSystemController@submitalbums')->name('submit.albums.data');
Route::get('/albums/view/{id}', 'AllSystemController@showviewalbums')->name('view.album.data');
Route::get('/albums/edit/{id}', 'AllSystemController@editviewalbums')->name('edit.albums');
Route::patch('/albums/{id}', 'AllSystemController@updatealbumdata')->name('update.album.data');
Route::post('/deletealbum/{id}', 'AllSystemController@deletealbumdata')->name('delete.album.data');


Route::post('/albumsgallery/submit', 'AllSystemController@submitalbumsgallery')->name('add.albums.galleries');

Route::post('/deletealbumsgallery/{id}', 'AllSystemController@deletealbumsgallery')->name('delete.album.gallery.data');


Route::get('/album-detail/{id}', 'WebController@albumdetailpage')->name('homepage.album.detail');













Route::get('news/table', 'AllSystemController@indexnewsdata')->name('news.table');
Route::get('/news/add', 'AllSystemController@createviewnews')->name('create.news');
Route::post('/news/newsdata/submit', 'AllSystemController@submitnews')->name('submit.news.data');
Route::get('/news/view/{id}', 'AllSystemController@showviewnews')->name('view.new.data');
Route::get('/news/edit/{id}', 'AllSystemController@editviewnews')->name('edit.news');
Route::patch('/news/{id}', 'AllSystemController@updatenewdata')->name('update.new.data');
Route::post('/deletenew/{id}', 'AllSystemController@deletenewdata')->name('delete.new.data');
Route::any('/news/unfeature/{id}', 'AllSystemController@unfeaturenews')->name('unfeature.news');



Route::get('barangays/table', 'AllSystemController@indexbarangaysdata')->name('barangays.table');
Route::get('/barangays/add', 'AllSystemController@createviewbarangays')->name('create.barangays');
Route::post('/barangays/barangaysdata/submit', 'AllSystemController@submitbarangays')->name('submit.barangays.data');
Route::get('/barangays/view/{id}', 'AllSystemController@showviewbarangays')->name('view.barangay.data');
Route::get('/barangays/edit/{id}', 'AllSystemController@editviewbarangays')->name('edit.barangays');
Route::patch('/barangays/{id}', 'AllSystemController@updatebarangaydata')->name('update.barangays.data');
Route::post('/deletebarangay/{id}', 'AllSystemController@deletebarangaydata')->name('delete.barangays.data');




Route::post('/barangayofficials/submit', 'AllSystemController@submitbarangayofficials')->name('add.barangay.official');
Route::get('/barangayofficial/edit/{id}', 'AllSystemController@editbarangayofficials')->name('edit.barangays');
Route::patch('/barangayofficial/{id}', 'AllSystemController@updatebarangayofficialdata')->name('barangayofficial.update');
Route::post('/deletebarangayofficial/{id}', 'AllSystemController@deletebarangayofficial')->name('delete.barangay.official.data');


Route::get('awards/table', 'AllSystemController@indexawardsdata')->name('awards.table');
Route::get('/awards/add', 'AllSystemController@createviewawards')->name('create.awards');
Route::post('/awards/awardsdata/submit', 'AllSystemController@submitawards')->name('submit.awards.data');
Route::get('/awards/view/{id}', 'AllSystemController@showviewawards')->name('view.award.data');
Route::get('/awards/edit/{id}', 'AllSystemController@editviewawards')->name('edit.awards');
Route::patch('/awards/{id}', 'AllSystemController@updateawarddata')->name('update.award.data');
Route::post('/deleteaward/{id}', 'AllSystemController@deleteawarddata')->name('delete.award.data');


Route::get('histories/table', 'AllSystemController@indexhistoriesdata')->name('histories.table');
Route::get('/histories/add', 'AllSystemController@createviewhistories')->name('create.histories');
Route::post('/histories/historiesdata/submit', 'AllSystemController@submithistories')->name('submit.histories.data');
Route::get('/histories/edit/{id}', 'AllSystemController@editviewhistories')->name('edit.histories');
Route::patch('/histories/{id}', 'AllSystemController@updatehistorydata')->name('update.history.data');
Route::post('/deletehistory/{id}', 'AllSystemController@deletehistorydata')->name('delete.history.data');



Route::get('directories/table', 'AllSystemController@indexdirectoriesdata')->name('directories.table');
Route::get('/directories/add', 'AllSystemController@createviewdirectories')->name('create.directories');
Route::post('/directories/directoriesdata/submit', 'AllSystemController@submitdirectories')->name('submit.directories.data');
Route::get('/directories/edit/{id}', 'AllSystemController@editviewdirectories')->name('edit.directories');
Route::patch('/directories/{id}', 'AllSystemController@updatedirectorydata')->name('update.directory.data');
Route::post('/deletedirectory/{id}', 'AllSystemController@deletedirectorydata')->name('delete.directory.data');


Route::get('/mayor/view/{id}', 'AllSystemController@showviewmayoroffice')->name('mayor.view.page');
Route::post('/mayor/{id}', 'AllSystemController@updatemayordata')->name('mayor.update');


Route::get('/homewelcome/view/{id}', 'AllSystemController@showviewhomewelcome')->name('homewelcome.view.page');
Route::post('/homewelcome/{id}', 'AllSystemController@updatehomewelcomedata')->name('homewelcome.update');



Route::get('homegalleries/table', 'AllSystemController@indexhomegalleriesdata')->name('homegalleries.table');
Route::get('/homegalleries/add', 'AllSystemController@createviewhomegalleries')->name('create.homegalleries');
Route::post('/homegalleries/homegalleriesdata/submit', 'AllSystemController@submithomegalleries')->name('submit.homegalleries.data');
Route::get('/homegalleries/view/{id}', 'AllSystemController@showviewhomegalleries')->name('view.homegallery.data');
Route::get('/homegalleries/edit/{id}', 'AllSystemController@editviewhomegalleries')->name('edit.homegalleries');
Route::patch('/homegalleries/{id}', 'AllSystemController@updatehomegallerydata')->name('update.homegallery.data');
Route::post('/deletehomegallery/{id}', 'AllSystemController@deletehomegallerydata')->name('delete.homegallery.data');



Route::get('homevideos/table', 'AllSystemController@indexhomevideosdata')->name('homevideos.table');
Route::get('/homevideos/add', 'AllSystemController@createviewhomevideos')->name('create.homevideos');
Route::post('/homevideos/homevideosdata/submit', 'AllSystemController@submithomevideos')->name('submit.homevideos.data');

// Route::get('/homevideos/view/{id}', 'AllSystemController@showviewhomevideos')->name('view.homevideo.data');
Route::get('/homevideos/edit/{id}', 'AllSystemController@editviewhomevideos')->name('edit.homevideos');
Route::patch('/homevideos/{id}', 'AllSystemController@updatehomevideodata')->name('update.homevideo.data');
Route::post('/deletehomevideo/{id}', 'AllSystemController@deletehomevideodata')->name('delete.homevideo.data');




Route::get('activities/table', 'AllSystemController@indexactivitiesdata')->name('activities.table');
Route::get('/activities/add', 'AllSystemController@createviewactivities')->name('create.activities');
Route::post('/activities/activitiesdata/submit', 'AllSystemController@submitactivities')->name('submit.activities.data');
Route::get('/activities/view/{id}', 'AllSystemController@showviewactivities')->name('view.activity.data');
Route::get('/activities/edit/{id}', 'AllSystemController@editviewactivities')->name('edit.activities');
Route::patch('/activities/{id}', 'AllSystemController@updateactivitydata')->name('update.activity.data');
Route::post('/deleteactivity/{id}', 'AllSystemController@deleteactivitydata')->name('delete.activity.data');




Route::get('blogs/table', 'AllSystemController@indexblogsdata')->name('blogs.table');
Route::get('/blogs/add', 'AllSystemController@createviewblogs')->name('create.blogs');
Route::post('/blogs/blogsdata/submit', 'AllSystemController@submitblogs')->name('submit.blogs.data');
Route::get('/blogs/view/{id}', 'AllSystemController@showviewblogs')->name('view.blog.data');
Route::get('/blogs/edit/{id}', 'AllSystemController@editviewblogs')->name('edit.blogs');
Route::patch('/blogs/{id}', 'AllSystemController@updateblogdata')->name('update.blog.data');




Route::post('/storiesgallery/submit', 'AllSystemController@submitstoriesgallery')->name('add.stories.galleries');
// Route::get('/storiesgallery/edit/{id}', 'AllSystemController@editstoriesgallery')->name('edit.stories.gallery');
// Route::get('/storiesgallery/view/{id}', 'AllSystemController@showviewstoriesgallery')->name('view.stories.gallery');
Route::post('/deletestoriesgallery/{id}', 'AllSystemController@deletestoriesgallery')->name('delete.story.gallery.data');



Route::post('/newsgallery/submit', 'AllSystemController@submitnewsgallery')->name('add.news.galleries');
Route::get('/newsgallery/edit/{id}', 'AllSystemController@editnewsgallery')->name('edit.news.gallery');
Route::post('/deletenewsgallery/{id}', 'AllSystemController@deletenewsgallery')->name('delete.new.gallery.data');






Route::post('/activitiesgallery/submit', 'AllSystemController@submitactivitiesgallery')->name('add.activities.galleries');
Route::post('/blogsgallery/submit', 'AllSystemController@submitblogsgallery')->name('add.blogs.galleries');


Route::get('contact_us', 'AllSystemController@indexcontactus')->name('sending.mail');
Route::post('contact_us/submit', 'AllSystemController@submitcontactus')->name('sending.mail');







// Route::group(['middleware' => ['auth', 'guest']], function() {

//   Route::resource('blog','BlogController');
// });

// Route::middleware(['auth', 'guest'])
//     ->group(function () {
//         Route::get('/test', 'TestController@test');
//     });
// Route::get('/allgalleries', 'AllGalleriesController@index');




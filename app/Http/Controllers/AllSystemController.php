<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

use App\Models\Activity;
use App\Models\ActivitiesGallery;
use App\Models\Authors;
use App\Models\Blog;
use App\Models\BlogsGallery;
use App\Models\Category;
use App\Models\Contactus;
use App\Models\Newy;
use App\Models\NewsGallery;
use App\Models\Story;
use App\Models\StoriesGallery;
use App\Models\Author;
use App\Models\Award;
use App\Models\HomesGallery;
use App\Models\HomesVideo;
use App\Models\Mayoroffice;
use App\Models\MayorofficesGallery;
use App\Models\Directory;
use App\Models\HomesWelcome;
use App\Models\History;
use App\Models\Barangay;
use App\Models\BarangaysOfficial;
use App\Models\Covid;
use App\Models\CovidsGallery;
use App\Models\Album;
use App\User;
use Redirect;
use Response;
use Mail;
// use File;
use Validate;
use Auth;
use DB;

class AllSystemController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function indexauthorsdata()
	{   
		$data = DB::select(DB::raw('SELECT id,name,display_image,facebook_link,twitter_link FROM authors'));

		$sidebar = "authors";
		return view('authors.index_authors_data', compact('data','sidebar'));
	}

	public function createviewauthors(Request $request)
	{   
		$sidebar = "authors";
		return view('authors.create_view_authors', compact('sidebar'));
	}

	public function submitauthors(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'

		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/authors_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$author = new Author;
		$author->name = $request->input('name');
		$author->display_image = $fileNameToStore;
		$author->description = $request->input('description');
		$author->facebook_link = $request->input('facebook_link');
		$author->twitter_link = $request->input('twitter_link');
		$author->save();

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function showviewauthors($id, Request $request)
	{
		$author = $request->get('id');
		$author = Author::findorFail($id);
		$sidebar = "authors";

		return view('authors.show_view_authors', compact('author','sidebar'));
	}

	public function editviewauthors($id)
	{
		$data = Author::findOrFail($id);
		$sidebar = "authors";

		return view('authors.edit_view_authors', compact('data','sidebar'));
	}

	public function updateauthordata(Request $request, $id)
	{
		$this->validate($request, [
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/authors_images', $fileNameToStore);
		}
		$author = Author::find($id);
		$author->name = $request->input('name');
		$author->description = $request->input('description');
		$author->facebook_link = $request->input('facebook_link');
		$author->twitter_link = $request->input('twitter_link');
		if($request->hasFile('display_image'))
		{
			$authordata = Author::findorFail($id);
			$image_path = "public/storage/authors_images/{$authordata->display_image}";
			unlink($image_path);
			$author->display_image = $fileNameToStore;
		}
		$author->save();
        // return redirect('/authors/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function deleteauthordata($id)
	{	
		// $cascade = Author::find($request->all())

		$author = Author::findorFail($id);
		$authordelete = Author::where('id', $id)->delete();
		$image_path = "public/storage/authors_images/{$author->display_image}";
		if ($authordelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Author deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Author not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexbarangaysdata()
	{
		$data = DB::table(DB::raw('barangays'))->get();

		$sidebar = "barangays";
		return view('barangays.index_barangays_data', compact('data','sidebar'));
	}

	public function createviewbarangays(Request $request)
	{   
		$sidebar = "barangays";
		return view('barangays.create_view_barangays', compact('sidebar'));
	}

	public function submitbarangays(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangays_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$barangay = new Barangay;
		$barangay->barangay_name = $request->input('barangay_name');
		$barangay->description = $request->input('description');
		$barangay->display_image = $fileNameToStore;
		$barangay->save();

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function showviewbarangays(Request $request)
	{   
		$barangay_id = $request->id;
		$barangay = Barangay::findorFail($barangay_id);
		$sidebar = "barangays";

		$official = DB::table('barangays_official')->where('id', $barangay_id)->get();

		return view('barangays.show_view_barangays', compact('barangay','sidebar', 'official'));
	}


	public function editviewbarangays($id, Request $request)
	{
		$data = Barangay::findOrFail($id);
		$sidebar = "barangays";


		return view('barangays.edit_view_barangays', compact('data', 'sidebar'));

	}

	public function updatebarangaydata(Request $request, $id)
	{
		$this->validate($request, [
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangays_images', $fileNameToStore);
		}
		$barangay = Barangay::find($id);
		$barangay->barangay_name = $request->input('barangay_name');
		$barangay->description = $request->input('description');

		if($request->hasFile('display_image'))
		{
			$barangaydata = Barangay::findorFail($id);
			$image_path = "public/storage/barangays_images/{$barangaydata->display_image}";
			unlink($image_path);
			$barangay->display_image = $fileNameToStore;
		}
		$barangay->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}

	public function deletebarangaydata($id)
	{
		$barangay = Barangay::findorFail($id);
		$barangaydelete = Barangay::where('id', $id)->delete();
		$image_path = "public/storage/barangays_images/{$barangay->display_image}";
		if ($barangaydelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Barangay deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Barangay not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function submitbarangayofficials(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_officials_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$barangayofficials = new BarangaysOfficial;
		$barangayofficials->name = $request->input('name');
		$barangayofficials->position = $request->input('position');
		$barangayofficials->display_image = $fileNameToStore;
		$barangayofficials->barangay_id = $request->input('barangay_id');
		$barangayofficials->save();


		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function editbarangayofficials($id, Request $request)
	{
		$data = BarangaysOfficial::findOrFail($id);
		$sidebar = "barangays";

		return view('barangayofficials.edit_view_barangayofficials', compact('data', 'sidebar'));
	}


	public function updatebarangayofficialdata(Request $request, $id)
	{
		$this->validate($request, [
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_officials_images', $fileNameToStore);
		}
		$barangayofficial = BarangaysOfficial::find($id);
		$barangayofficial->name = $request->input('name');
		$barangayofficial->position = $request->input('position');
		if($request->hasFile('display_image'))
		{
			$barangayofficialdata = BarangaysOfficial::findorFail($id);
			$image_path = "public/storage/barangay_officials_images/{$barangayofficialdata->display_image}";
			unlink($image_path);
			$barangayofficial->display_image = $fileNameToStore;
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$barangayofficial->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}


	public function deletebarangayofficial($id)
	{
		$barangayofficial = BarangaysOfficial::findorFail($id);
		$barangayofficials = BarangaysOfficial::where('id', $id)->delete();
		$image_path = "public/storage/barangay_officials_images/{$barangayofficial->display_image}";

		if ($barangayofficials == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Deleted successfully";
		} 
		else
		{
			$success = false;
			$message = "Barangay Official Not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexstoriesdata()
	{
		$data = Story::select(DB::raw('stories'))
		->join('categories', 'categories.id', '=', 'stories.category_id')
		->join('authors', 'authors.id', '=', 'stories.author_id')
		->select('stories.id','stories.title','stories.description','stories.display_image','stories.is_flagship','stories.input_by','stories.date_input', 'stories.is_covid','categories.category_name','authors.name')
		->orderBy('id', 'asc')
		->get();

		$data2 = Story::select(DB::raw('stories'))
		->join('categories', 'categories.id', '=', 'stories.category_id')
		->join('authors', 'authors.id', '=', 'stories.author_id')
		->select('stories.id','stories.title','stories.description','stories.display_image','stories.is_flagship','stories.input_by','stories.date_input', 'is_covid','categories.category_name','authors.name')
		->where('is_flagship', "Yes")
		->orderBy('id', 'asc')
		->get();
		$sidebar = "stories";

		return view('stories.index_stories_data',compact('data','data2' ,'sidebar'));

	}

	public function unfeaturestories(Request $request)
	{
		$stories_id = $request->id;
		$unfeaturestories = DB::table('stories')->where('id', $stories_id)->update(['is_flagship' => "No"]);

		// $success = true;
		// $message = "Unfeature successfully";
		// return response()->json([
		// 	'success' => $success,
		// 	'message' => $message,
		// ]);

		return redirect('/stories/table')->with('success', 'Data is successfully unfeatured.');

	}

	public function createviewstories(Request $request)
	{
		$categories = DB::select(DB::raw('SELECT * FROM categories WHERE status = 3'));
		$authors = DB::select(DB::raw('SELECT * from authors'));
		$sidebar = "stories";

		return view('stories.create_view_stories', compact('categories','authors','sidebar'));
	}

	public function submitstories(Request $request)
	{
		$this->validate($request, [
            // 'title'         =>  'required',
            // 'description'   =>  'required',
			'display_image' =>  'required|image'
            // 'is_flagship'   =>  'required',
            // 'input_by'      =>  'required',
            // 'category_id'   =>  'required',
            // 'author_id'     =>  'required',
            // 'date_input'    =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/stories_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}

		if($request->input('is_flagship') != "No")
		{
			$story = DB::table('stories')->update(['is_flagship' => "No"]);
			$new = DB::table('news')->update(['is_flagship' => "No"]);
		}

		$story = new Story;
		$story->title = $request->input('title');
		$story->description = $request->input('description');
		$story->display_image = $fileNameToStore;
		$story->is_flagship = $request->input('is_flagship');
		$story->input_by = $request->input('input_by');
		$story->category_id = $request->input('category_id');
		$story->author_id = $request->input('author_id');
		$story->date_input = $request->input('date_input');
		$story->is_covid = $request->input('is_covid');
		$story->save();

		$success = true;
		$message = "Added successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
// return redirect('/stories/table')->with('success', 'Data Added successfully.');
	}

	public function showviewstories($id, Request $request)
	{
		$story = $request->get('id');
		$story = Story::findorFail($id);
		$categories = DB::select(DB::raw('SELECT id,category_name FROM categories LIMIT 1'));
		$authors = DB::select(DB::raw('SELECT id,name FROM authors LIMIT 1'));
		$sidebar = "stories";

		return view('stories.show_view_stories', compact('story', 'categories', 'authors', 'sidebar'));

	}

	public function editviewstories($id)
	{
		$categories = DB::select(DB::raw('SELECT id,category_name FROM categories WHERE status = 3'));
		$authors = DB::select(DB::raw('SELECT * FROM authors'));
		$data = Story::findOrFail($id);
		$sidebar = "stories";

		return view('stories.edit_view_stories', compact('data', 'categories', 'authors','sidebar'));

	}

	public function updatestorydata(Request $request, $id)
	{
		$this->validate($request, [
            // 'title'         =>  'required',
            // 'description'   =>  'required',
            // 'category_id'   =>  'required',
            // 'author_id'     =>  'required',
			'display_image'     =>  'nullable|image'
		]);

		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/stories_images', $fileNameToStore);
		}

		// if($request->input('is_covid') != "No")
		// {
		// 	$story = DB::table('stories')->update(['is_covid' => "No"]);
		// 	$new = DB::table('news')->update(['is_covid' => "No"]);
		// }

		if($request->input('is_flagship') != "No")
		{
			$story = DB::table('stories')->update(['is_flagship' => "No"]);
			$new = DB::table('news')->update(['is_flagship' => "No"]);
		}

		$story = Story::find($id);
		$story->title = $request->input('title');
		$story->description = $request->input('description');
		$story->is_flagship = $request->input('is_flagship');
		$story->date_input = $request->input('date_input');
		$story->category_id = $request->input('category_id');
		$story->author_id = $request->input('author_id');
		$story->is_covid = $request->input('is_covid');
		if($request->hasFile('display_image'))
		{
			$storydata = Story::findorFail($id);
			$image_path = "public/storage/stories_images/{$storydata->display_image}";
			unlink($image_path);
			$story->display_image = $fileNameToStore;
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}

		$story->save();


		$success = true;
		$message = "Updated successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
        // return redirect('/stories/table')->with('success', 'Data is successfully updated');
	}


	public function deletestorydata($id)
	{   

		$story = Story::findorFail($id);
		$storydelete = Story::where('id', $id)->delete();
		$image_path = "public/storage/stories_images/{$story->display_image}";
		if($storydelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Story deleted successfully";
		}
		else
		{
			$success = false;
			$message = "Story not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexnewsdata()
	{
		$data = Newy::select(DB::raw('news'))
		->join('categories', 'categories.id', '=', 'news.category_id')
		->join('authors', 'authors.id', '=', 'news.author_id')
		->select('news.id','news.title','news.description','news.display_image','news.is_flagship','news.input_by','news.date_input','categories.category_name','authors.name')
		->orderBy('id', 'asc')
		->get();

		$data2 = Newy::select(DB::raw('news'))
		->join('categories', 'categories.id', '=', 'news.category_id')
		->join('authors', 'authors.id', '=', 'news.author_id')
		->select('news.id','news.title','news.description','news.display_image','news.is_flagship','news.input_by','news.date_input','news.is_covid','categories.category_name','authors.name')
		->where('is_flagship', "Yes")
		->orderBy('id', 'asc')
		->get();

		$sidebar = "news";
		return view('news.index_news_data', compact('data','data2','sidebar'));

	}


	public function unfeaturenews(Request $request)
	{
		$news_id = $request->id;
		$unfeaturenews = DB::table('news')->where('id', $news_id)->update(['is_flagship' => "No"]);

		return redirect('/news/table')->with('success', 'Data is successfully unfeatured.');

	}


	public function createviewnews(Request $request)
	{
		$categories = DB::select(DB::raw('SELECT * FROM categories WHERE status = 2'));
		$authors = DB::select(DB::raw('SELECT * from authors'));
		$sidebar = "news";

		return view('news.create_view_news', compact('categories','authors', 'sidebar'));

	}

	public function submitnews(Request $request)
	{
		$this->validate($request, [
            // 'title'         =>  'required',
            // 'description'   =>  'required',
			'display_image' =>  'required|image'
            // 'is_flagship'   =>  'required',
            // 'input_by'      =>  'required',
            // 'date_input'    =>  'required',
            // 'category_id'   =>  'required',
            // 'author_id'     =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/news_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		// if($request->input('is_covid') != "No")
		// {
		// 	$story = DB::table('stories')->update(['is_covid' => "No"]);
		// 	$new = DB::table('news')->update(['is_covid' => "No"]);
		// }

		if($request->input('is_flagship') != "No")
		{
			$story = DB::table('stories')->update(['is_flagship' => "No"]);
			$new = DB::table('news')->update(['is_flagship' => "No"]);
		}


		$new = new Newy;
		$new->title = $request->input('title');
		$new->description = $request->input('description');
		$new->display_image = $fileNameToStore;
		$new->is_flagship = $request->input('is_flagship');
		$new->input_by = $request->input('input_by');
		$new->date_input = $request->input('date_input');
		$new->is_covid = $request->input('is_covid');
		$new->category_id = $request->input('category_id');
		$new->author_id = $request->input('author_id');
		$new->save();

		$success = true;
		$message = "Added successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
        // return redirect('/news/table')->with('success', 'Data Added successfully.');
	}

	public function showviewnews($id, Request $request)
	{
		$new = $request->get('id');
		$new = Newy::findorFail($id);
		$categories = DB::select(DB::raw('SELECT id,category_name FROM categories LIMIT 1'));
		$authors = DB::select(DB::raw('SELECT id,name FROM authors LIMIT 1'));
		$sidebar = "news";

		return view('news.show_view_news', compact('new', 'categories', 'authors', 'sidebar'));

	}

	public function editviewnews($id)
	{
		$categories = DB::select(DB::raw('SELECT id,category_name FROM categories WHERE status = 2'));
		$authors = DB::select(DB::raw('SELECT * FROM authors'));
		$data = Newy::findOrFail($id);
		$sidebar = "news";

		return view('news.edit_view_news', compact('data', 'categories', 'authors', 'sidebar'));

	}


	public function updatenewdata(Request $request, $id)
	{
		$this->validate($request, [
            // 'title'         =>  'required',
            // 'description'   =>  'required',
            // 'category_id'   =>  'required',
            // 'author_id'     =>  'required'
			'display_image' =>  'nullable|image'
		]);

		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/news_images', $fileNameToStore);
		}

		if($request->input('is_flagship') != "No")
		{
			$new = DB::table('news')->update(['is_flagship' => "No"]);
			$story = DB::table('stories')->update(['is_flagship' => "No"]);
		}


		$new = Newy::find($id);
		$new->title = $request->input('title');
		$new->description = $request->input('description');
		$new->is_flagship = $request->input('is_flagship');
		$new->category_id = $request->input('category_id');
		$new->date_input = $request->input('date_input');
		$new->is_covid = $request->input('is_covid');
		$new->author_id = $request->input('author_id');
		if($request->hasFile('display_image'))
		{
			$newsdata = Newy::findorFail($id);
			$image_path = "public/storage/news_images/{$newsdata->display_image}";
			unlink($image_path);
			$new->display_image = $fileNameToStore;
		}
		$new->save();

		$success = true;
		$message = "Updated successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
    // return redirect('/news/table')->with('success', 'Data is successfully updated');
	}


	public function deletenewdata($id)
	{   
		$new = Newy::findorFail($id);
		$newdelete = Newy::where('id', $id)->delete();
		$image_path = "public/storage/news_images/{$new->display_image}";
		if ($newdelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "News deleted successfully";
		} else {
			$success = false;
			$message = "News not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}











	public function indexhistoriesdata()
	{   
    // $data = DB::select(DB::raw('SELECT id,title,description FROM histories'));
		$sidebar = "histories";

		$data = DB::table('histories')->orderBy('id', 'asc')->get();

		return view('histories.index_histories_data', compact('data','sidebar'));

	}


	public function createviewhistories(Request $request)
	{

		$sidebar = "histories";

		return view('histories.create_view_histories', compact('sidebar'));

	}

	public function submithistories(Request $request)
	{
//     $this->validate($request, [
//         'title'         =>  'required',
//         'description'   =>  'required'
// ]);
		$history = new History;
		$history->title = $request->input('title');
		$history->description = $request->input('description');
		$history->save();
    // return redirect('/histories/table')->with('success', 'Data Added successfully.');

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);


	}


	public function editviewhistories($id)
	{

		$data = History::findOrFail($id);
		$sidebar = "histories";

		return view('histories.edit_view_histories', compact('data', 'sidebar'));

	}

	public function updatehistorydata(Request $request, $id)
	{
    // $this->validate($request, [
        // 'title'         =>  'required',
        // 'description'   =>  'required'
    // ]);
		$history = History::find($id);
		$history->title = $request->input('title');
		$history->description = $request->input('description');
		$history->save();
    // return redirect('/histories/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);



	}


	public function deletehistorydata($id)
	{   

		$historydelete = History::where('id', $id)->delete();

		if ($historydelete == 1) 
		{
			$success = true;
			$message = "History deleted successfully";
		} else {
			$success = false;
			$message = "History not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}










	public function showviewmayoroffice($id, Request $request)
	{
		$mayoroffice = $request->get('id');
		$mayoroffice = Mayoroffice::findorFail($id);
		$sidebar = "mayoroffices";

		return view('mayoroffice.show_view_mayoroffice', compact('mayoroffice', 'sidebar'));
	}


	public function updatemayordata(Request $request, $id)
	{
		$this->validate($request, [
    //     'mayor_name'    =>  'required',
    //     'mini_title'    =>  'required',
    //     'description'   =>  'required'
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/mayor_images', $fileNameToStore);
		}
		$mayoroffice = Mayoroffice::find($id);
		$mayoroffice->mayor_name = $request->input('mayor_name');
		$mayoroffice->mini_title = $request->input('mini_title');
		$mayoroffice->description = $request->input('description');
		if($request->hasFile('display_image'))
		{
			$mayordata = Mayoroffice::findorFail($id);
			$image_path = "public/storage/mayor_images/{$mayordata->display_image}";
			unlink($image_path);
			$mayoroffice->display_image = $fileNameToStore;
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$mayoroffice->save();

    // return redirect()->back()->with('success', 'Data updated Successfully.');
		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}










	public function showviewhomewelcome($id, Request $request)
	{
		$homewelcome = $request->get('id');
		$homewelcome = HomesWelcome::findorFail($id);
		$sidebar = "homewelcome";

		return view('home_welcome.show_view_home_welcome', compact('homewelcome', 'sidebar'));
	}



	public function updatehomewelcomedata(Request $request, $id)
	{
		$this->validate($request, [
    //     'title'         =>  'required',
    //     'description'   =>  'required'
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/home_welcome_images', $fileNameToStore);
		}
		$homewelcome = HomesWelcome::find($id);
		$homewelcome->title = $request->input('title');
		$homewelcome->description = $request->input('description');
		if($request->hasFile('display_image'))
		{
			$welcomedata = HomesWelcome::findorFail($id);
			$image_path = "public/storage/home_welcome_images/{$welcomedata->display_image}";
			unlink($image_path);
			$homewelcome->display_image = $fileNameToStore;
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$homewelcome->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}















	public function indexhomegalleriesdata()
	{
		$data = DB::select(DB::raw('SELECT id,title,display_image,date_taken FROM home_galleries'));
		$sidebar = "homegalleries";

		return view('home_galleries.index_home_galleries_data', compact('data', 'sidebar'));
        // return response()->view('errors.404', [], 404);
	}

	public function createviewhomegalleries(Request $request)
	{
		$sidebar = "homegalleries";

		return view('home_galleries.create_view_home_galleries', compact('sidebar'));
	}


	public function submithomegalleries(Request $request)
	{
		$this->validate($request, [
    //     'title'         =>  'required',
			'display_image' =>  'required|image'
    //     'date_taken'    =>  'required'

		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/home_galleries_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$homegallery = new HomesGallery;
		$homegallery->title = $request->input('title');
		$homegallery->display_image = $fileNameToStore;
		$homegallery->date_taken = $request->input('date_taken');
		$homegallery->save();
    // return redirect('/homegalleries/table')->with('success', 'Data Added successfully.');

		$success = true;
		$message = "Image uploaded successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function showviewhomegalleries($id, Request $request)
	{
		$homegallery = $request->get('id');
		$homegallery = HomesGallery::findorFail($id);
		$sidebar = "homegalleries";

		return view('home_galleries.show_view_home_galleries', compact('homegallery', 'sidebar'));

	}


	public function editviewhomegalleries($id)
	{
		$data = HomesGallery::findOrFail($id);
		$sidebar = "homegalleries";

		return view('home_galleries.edit_view_home_galleries', compact('data', 'sidebar'));
	}


	public function updatehomegallerydata(Request $request, $id)
	{
		$this->validate($request, [
    //     'title'         =>  'required'
			'display_image' =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/home_galleries_images', $fileNameToStore);
		}
		$homegallery = HomesGallery::find($id);
		$homegallery->title = $request->input('title');
		$homegallery->date_taken = $request->input('date_taken');
		if($request->hasFile('display_image'))
		{
			$homegallerydata = HomesGallery::findorFail($id);
			$image_path = "public/storage/home_galleries_images/{$homegallerydata->display_image}";
			unlink($image_path);

			$homegallery->display_image = $fileNameToStore;
		}
		$homegallery->save();
    // return redirect('/homegalleries/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function deletehomegallerydata($id)
	{
		$homegallery = HomesGallery::findorFail($id);
		$homegallerydelete = HomesGallery::where('id', $id)->delete();
		$image_path = "public/storage/home_galleries_images/{$homegallery->display_image}";
		if ($homegallerydelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Image deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Image not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}














	public function indexhomevideosdata()
	{
		$data = DB::select(DB::raw('SELECT id,title,display_image,embed_video FROM home_videos'));
		$sidebar = "homevideos";

		return view('home_videos.index_home_videos_data', compact('data', 'sidebar'));
        // return response()->view('errors.404', [], 404);
	}


	public function createviewhomevideos(Request $request)
	{   
		$sidebar = "homevideos";
		return view('home_videos.create_view_home_videos', compact('sidebar'));
	}



	public function submithomevideos(Request $request)
	{
		$this->validate($request, [
        // 'title'         =>  'required',
        // 'display_image' =>  'required|image',
			'embed_video'   =>  'required'
        // 'is_flagship'   =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/home_videos_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}

		if($request->input('is_flagship') != "No")
		{
			$homevideo = DB::table('home_videos')->update(['is_flagship' => "No"]);
		}


		$homevideo = new HomesVideo;
		$homevideo->title = $request->input('title');
		$homevideo->display_image = $fileNameToStore;
		$homevideo->embed_video = $request->input('embed_video');
		$homevideo->is_flagship= $request->input('is_flagship');
		$homevideo->save();
    // return redirect('/homevideos/table')->with('success', 'Data Added successfully.');

		$success = true;
		$message = "Added successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}

	// public function showviewhomevideos($id, Request $request)
	// {
	//     $homevideo = $request->get('id');
	//     $homevideo = HomesVideo::findorFail($id);
	//     $sidebar = "homevideos";

	//     return view('home_videos.show_view_home_videos', compact('homevideo', 'sidebar'));

	// }


	public function editviewhomevideos($id)
	{
		$data = HomesVideo::findOrFail($id);
		$sidebar = "homevideos";

		return view('home_videos.edit_view_home_videos', compact('data', 'sidebar'));

	}


	public function updatehomevideodata(Request $request, $id)
	{
		$this->validate($request, [
			'title'         =>  'required',
			'is_flagship'   =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/home_videos_images', $fileNameToStore);
		}
		if($request->input('is_flagship') != "No")
		{
			$homevideo = DB::table('home_videos')->update(['is_flagship' => "No"]);
		}

		$homevideo = HomesVideo::find($id);
		$homevideo->title = $request->input('title');
		$homevideo->embed_video = $request->input('embed_video');
		$homevideo->is_flagship = $request->input('is_flagship');
		if($request->hasFile('display_image'))
		{
			$homevideo->display_image = $fileNameToStore;
		}
		$homevideo->save();
    // return redirect('/homevideos/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function deletehomevideodata($id)
	{
		$homevideodelete = HomesVideo::where('id', $id)->delete();
		if ($homevideodelete == 1) 
		{
			$success = true;
			$message = "Video deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Video not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexawardsdata()
	{
		$data = DB::select(DB::raw('SELECT id,title,description,display_image,link,date_award_taken FROM awards'));
		$sidebar = "awards";

		return view('awards.index_awards_data', compact('data', 'sidebar'));
	}

	public function createviewawards(Request $request)
	{
		$sidebar = "awards";
		return view('awards.create_view_awards', compact('sidebar'));
	}


	public function submitawards(Request $request)
	{
		$this->validate($request, [
    //     'title'             =>  'required',
    //     'description'       =>  'required',
			'display_image'     =>  'required|image'
    //     'date_award_taken'  =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/awards_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$award = new Award;
		$award->title = $request->input('title');
		$award->display_image = $fileNameToStore;
		$award->description = $request->input('description');
		$award->link = $request->input('link');
		$award->date_award_taken = $request->input('date_award_taken');
		$award->save();
    // return redirect('/awards/table')->with('success', 'Data Added successfully.');

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function showviewawards($id, Request $request)
	{
		$award = $request->get('id');
		$award = Award::findorFail($id);
		$sidebar = "awards";

		return view('awards.show_view_awards', compact('award', 'sidebar'));
	}


	public function editviewawards($id)
	{
		$data = Award::findOrFail($id);
		$sidebar = "awards";

		return view('awards.edit_view_awards', compact('data', 'sidebar'));

	}


	public function updateawarddata(Request $request, $id)
	{
		$this->validate($request, [
    //     'title'         =>  'required',
    //     'description'   =>  'required'
			'display_image'     =>  'nullable|image'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/awards_images', $fileNameToStore);
		}
		$award = Award::find($id);
		$award->title = $request->input('title');
		$award->description = $request->input('description');
		$award->link = $request->input('link');
		$award->date_award_taken = $request->input('date_award_taken');

		if($request->hasFile('display_image'))
		{   
			$awarddata = Award::findorFail($id);
			$image_path = "public/storage/awards_images/{$awarddata->display_image}";
			unlink($image_path);
			$award->display_image = $fileNameToStore;

		}
		$award->save();
    // return redirect('/awards/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}



	public function deleteawarddata($id)
	{
		$award = Award::findorFail($id);
		$awarddelete = Award::where('id', $id)->delete();
		$image_path = "public/storage/awards_images/{$award->display_image}";

		if ($awarddelete == 1)  
		{
			unlink($image_path);
			$success = true;
			$message = "Award deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Award not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}












	public function indexdirectoriesdata()
	{
		$data = DB::select(DB::raw('SELECT id,directory_name,description FROM directories'));
		$sidebar = "directories";

		return view('directories.index_directories_data', compact('data', 'sidebar'));
	}

	public function createviewdirectories(Request $request)
	{
		$sidebar = "directories";
		return view('directories.create_view_directories', compact('sidebar'));
	}


	public function submitdirectories(Request $request)
	{
    // $this->validate($request, [
    //     'directory_name'    =>  'required',
    //     'description'       =>  'required'
    // ]);
		$directory = new Directory;
		$directory->directory_name = $request->input('directory_name');
		$directory->description = $request->input('description');
		$directory->save();
    // return redirect('/directories/table')->with('success', 'Data Added successfully.');

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}


	public function editviewdirectories($id)
	{
		$data = Directory::findOrFail($id);
		$sidebar = "directories";

		return view('directories.edit_view_directories', compact('data', 'sidebar'));
	}


	public function updatedirectorydata(Request $request, $id)
	{
    // $this->validate($request, [
    //     'directory_name'    =>  'required',
    //     'description'       =>  'required'
    // ]);
		$directory = Directory::find($id);
		$directory->directory_name = $request->input('directory_name');
		$directory->description = $request->input('description');
		$directory->save();
    // return redirect('/directories/table')->with('success', 'Data is successfully updated');

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function deletedirectorydata($id)
	{
		$directorydelete = Directory::where('id', $id)->delete();

		if ($directorydelete == 1)  
		{
			$success = true;
			$message = "Directory deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Directory not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexstoriesgallery()
	{
		$tr = DB::select(DB::raw('SELECT id,display_image,,story_id FROM stories_gallery'));
		compact($tr);
	}

	public function submitstoriesgallery(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
        // 'date_input'    =>  'required',
        // 'story_id'      =>  'required'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/galleries/stories', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$storiesgalleries = new StoriesGallery;
		$storiesgalleries->display_image = $fileNameToStore;
		$storiesgalleries->date_input = $request->input('date_input');
		$storiesgalleries->story_id = $request->input('story_id');
		$storiesgalleries->save();
// return redirect()->back()->with('success', 'File Added Successfully.');

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}


	public function deletestoriesgallery($id)
	{
		$storygallery = StoriesGallery::findorFail($id);
		$storiesgallery = StoriesGallery::where('id', $id)->delete();
		$image_path = "public/storage/galleries/stories/{$storygallery->display_image}";

		if ($storiesgallery == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Image deleted successfully";
		} 
		else
		{
			$success = false;
			$message = "Image Not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}











	public function submitnewsgallery(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
        // 'date_input'    =>  'required',
        // 'newy_id'       =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/galleries/news', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$newsgalleries = new NewsGallery;
		$newsgalleries->display_image = $fileNameToStore;
		$newsgalleries->date_input = $request->input('date_input');
		$newsgalleries->newy_id = $request->input('newy_id');
		$newsgalleries->save();
    // return redirect()->back()->with('success', 'File Added Successfully.');

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}



	public function deletenewsgallery($id)
	{

		$newgallery = NewsGallery::findorFail($id);
		$newsgallery = NewsGallery::where('id', $id)->delete();
		$image_path = "public/storage/galleries/news/{$newgallery->display_image}";
		if ($newsgallery == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Image deleted successfully";
		} 
		else
		{
			$success = false;
			$message = "Image Not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}












	public function indexcovidsdata()
	{
		$data = Covid::select(DB::raw('covids'))
		->join('authors', 'authors.id', '=', 'covids.author_id')
		->select('covids.id','covids.title','covids.description','covids.display_image','covids.date_created','covids.is_flagship','authors.name')
		->orderBy('id', 'asc')
		->get();		
		$sidebar = "covids";

		return view('covids.index_covids_data',compact('data','sidebar'));

	}

	public function unfeaturecovids(Request $request)
	{
		$covids = $request->id;
		$unfeaturecovids = DB::table('covids')->where('id', $covids)->update(['is_flagship' => "No"]);

		// $success = true;
		// $message = "Unfeature successfully";
		// return response()->json([
		// 	'success' => $success,
		// 	'message' => $message,
		// ]);

		return redirect('/covids/table')->with('success', 'Data is successfully unfeatured.');

	}

	public function createviewcovids(Request $request)
	{
		// $categories = DB::select(DB::raw('SELECT * FROM categories WHERE status = 3'));
		$authors = DB::select(DB::raw('SELECT * from authors'));
		$sidebar = "covids";

		return view('covids.create_view_covids', compact('authors','sidebar'));

	}

	public function submitcovids(Request $request)
	{
		$this->validate($request, [
            // 'title'         =>  'required',
            // 'description'   =>  'required',
			'display_image' =>  'required|image'
            // 'is_flagship'   =>  'required',
            // 'input_by'      =>  'required',
            // 'category_id'   =>  'required',
            // 'author_id'     =>  'required',
            // 'date_input'    =>  'required'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/covids_images', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$covid = new Covid;
		$covid->title = $request->input('title');
		$covid->description = $request->input('description');
		$covid->display_image = $fileNameToStore;
		$covid->date_created = $request->input('date_created');
		$covid->is_flagship = $request->input('is_flagship');
		$covid->author_id = $request->input('author_id');
		$covid->save();

		$success = true;
		$message = "Added successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function showviewcovids($id, Request $request)
	{
		$covid = $request->get('id');
		$covid = Covid::findorFail($id);
		// $categories = DB::select(DB::raw('SELECT id,category_name FROM categories LIMIT 1'));
		$authors = DB::select(DB::raw('SELECT id,name FROM authors LIMIT 1'));
		$sidebar = "covids";

		return view('covids.show_view_covids', compact('covid', 'authors', 'sidebar'));

	}

	public function editviewcovids($id)
	{
		// $categories = DB::select(DB::raw('SELECT id,category_name FROM categories WHERE status = 3'));
		$authors = DB::select(DB::raw('SELECT * FROM authors'));
		$data = Covid::findOrFail($id);
		$sidebar = "covids";

		return view('covids.edit_view_covids', compact('data', 'authors','sidebar'));

	}

	public function updatecoviddata(Request $request, $id)
	{
		$this->validate($request, [
			'display_image'     =>  'nullable|image'
		]);

		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/covids_images', $fileNameToStore);
		}
		$covid = Covid::find($id);
		$covid->title = $request->input('title');
		$covid->description = $request->input('description');
		$covid->date_created = $request->input('date_created');
		$covid->is_flagship = $request->input('is_flagship');
		$covid->author_id = $request->input('author_id');
		if($request->hasFile('display_image'))
		{
			$coviddata = Covid::findorFail($id);
			$image_path = "public/storage/covids_images/{$coviddata->display_image}";
			unlink($image_path);
			$covid->display_image = $fileNameToStore;
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$covid->save();

		$success = true;
		$message = "Updated successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
        // return redirect('/stories/table')->with('success', 'Data is successfully updated');
	}


	public function deletecoviddata($id)
	{   

		$covid = Covid::findorFail($id);
		$coviddelete = Covid::where('id', $id)->delete();
		$image_path = "public/storage/covids_images/{$covid->display_image}";
		if($coviddelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Deleted successfully";
		}
		else
		{
			$success = false;
			$message = "Not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}











	public function submitcovidsgallery(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/galleries/covids', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$covidsgalleries = new CovidsGallery;
		$covidsgalleries->display_image = $fileNameToStore;
		$covidsgalleries->date_input = $request->input('date_input');
		$covidsgalleries->covid_id = $request->input('covid_id');
		$covidsgalleries->save();

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}

	public function deletecovidsgallery($id)
	{
		$covidgallery = CovidsGallery::findorFail($id);
		$covidsgallery = CovidsGallery::where('id', $id)->delete();
		$image_path = "public/storage/galleries/covids/{$covidgallery->display_image}";

		if ($covidsgallery == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Image deleted successfully";
		} 
		else
		{
			$success = false;
			$message = "Image Not found";
		}

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function indexalbumsdata()
	{
		$data = DB::table(DB::raw('albums'))->orderBy('id', 'asc')->get();
		$sidebar = "albums";

		return view('albums.index_albums_data',compact('data','sidebar'));
	}


	public function createviewalbums(Request $request)
	{
		$sidebar = "albums";
		return view('albums.create_view_albums', compact('sidebar'));
	}


	public function submitalbums(Request $request)
	{
		// $this->validate($request, [
		// 'title'         =>  'required'
		// ]);
		$album = new Album;
		$album->title = $request->input('title');
		$album->date_input = $request->input('date_input');
		$album->save();

		$success = true;
		$message = "Added successfully";

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}


	public function showviewalbums($id, Request $request)
	{
		$album = $request->get('id');
		$album = Album::findorFail($id);
		
		$sidebar = "albums";
		return view('albums.show_view_albums', compact('album', 'sidebar'));

	}


	public function editviewalbums($id)
	{
		$data = Album::findOrFail($id);
		$sidebar = "albums";

		return view('albums.edit_view_albums', compact('data','sidebar'));

	}

	public function updatealbumdata(Request $request, $id)
	{
		// $this->validate($request, [
		// 	'display_image'     =>  'nullable|image'
		// ]);
		$album = Album::find($id);
		$album->title = $request->input('title');
		$album->date_input = $request->input('date_input');
		$album->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}


	public function deletealbumdata($id)
	{
		$albumdelete = Album::where('id', $id)->delete();

		if($albumdelete == 1) 
		{
			$success = true;
			$message = "Deleted successfully";
		}
		else
		{
			$success = false;
			$message = "Not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function submitalbumsgallery(Request $request)
	{
		$this->validate($request, [
			'display_image' =>  'required|image'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/galleries/albums', $fileNameToStore);
		}
		else
		{
			$fileNameToStore = 'noimage.jpg';
		}
		$homegalleries = new HomesGallery;
		$homegalleries->display_image = $fileNameToStore;
		$homegalleries->date_taken = $request->input('date_taken');
		$homegalleries->album_id = $request->input('album_id');
		$homegalleries->save();

		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}

	public function deletealbumsgallery($id)
	{
		$homegallery = HomesGallery::findorFail($id);
		$homesgallery = HomesGallery::where('id', $id)->delete();
		$image_path = "public/storage/galleries/albums/{$homegallery->display_image}";

		if ($homesgallery == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Image deleted successfully";
		} 
		else
		{
			$success = false;
			$message = "Image Not found";
		}

		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}










	public function getuserdata()
	{
		$sidebar = "dashboard";
		return view('changepassword.changepassword', compact('sidebar'));
	}

	public function userchangepassword(Request $request)
	{
		if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
		}
		if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){	
		}
		$this->validate($request, [
			'current-password' 	=> 'required',
			'new-password' 		=> 'required|string|min:5|confirmed',
		]);
		$user = Auth::user();
		$user->password = bcrypt($request->get('new-password'));
		$user->save();

		$success = true;
		$message = "Password changed successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}








	public function indexcontactus()
	{
		$sidebar = "mails";
		return view('contact.contact_us', compact('sidebar'));
	}



	public function submitcontactus(Request $request)
	{
		$arr = array(
			'name_arr'      =>  $request->name,
			'email_arr'     =>  $request->email,
			'message_arr'   =>  $request->message
		);
		Mail::send('mail', $arr, function($message) use ($request){
			$message->to('stackoverflowjm@gmail.com', 'Johnmichael')->subject('New Message received'. $request->name);
			$message->from($request->email, $request->name);
		});
		Contactus::create($request->all());
		return redirect()->back()->with('success', 'Message has been Sent... Thanks for contacting us!');
	}

}
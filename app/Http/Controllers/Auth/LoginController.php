<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{


    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    public function __construct()
    {   
        $this->middleware('guest')->except('logout');
    }


    // Request $request
    public function logout(Request $request)
    {
        // dd($request);
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        // Auth::logout();
        return redirect('/login');
    }
}

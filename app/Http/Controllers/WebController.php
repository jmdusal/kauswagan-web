<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\Story;
use App\Models\Newy;
use App\Models\HomesGallery;
use App\Models\HomesVideo;
use App\Models\Author;
use App\Models\Award;
use App\Models\Barangay;
use App\Models\BarangaysOfficial;
use App\Models\Album;
use App\Models\Covid;
use DB;

class WebController extends Controller
{
	public function homepage()
	{

		// $stories = DB::select(DB::raw('SELECT id,title,description,display_image,is_flagship,input_by,date_input,is_covid,author_id FROM stories WHERE date_input IN (SELECT max(date_input) FROM stories)'));
		$stories = Story::where('date_input', Story::max('date_input'))->orderBy('date_input','desc')->limit(4)->get();

		$news = Newy::where('date_input', Newy::max('date_input'))->orderBy('date_input','desc')->limit(6)->get();

		$albums = Album::select('*')
		->join('home_galleries', 'home_galleries.album_id', '=', 'albums.id', 'left outer')
		// ->orderBy('id', 'asc')
		// ->select('albums.id', 'albums.title','albums.date_input', 'home_galleries.id', 'home_galleries.display_image','home_galleries.date_taken', 'home_galleries.album_id')
		->groupBy('album_id')
		->limit(6)
		->get();

		$homegalleries = DB::table(DB::raw('home_galleries'))->get();
		$homevideo = DB::table(DB::raw('home_videos'))->where('is_flagship', "Yes")->get();
		$homewelcome = DB::table(DB::raw('home_welcome'))->where('id', "1")->get();
		$homeuswag = DB::table(DB::raw('home_welcome'))->where('id', "2")->get();

		return view('welcome-body', compact('stories','news', 'homevideo','homegalleries', 'homewelcome','homeuswag','albums'));
	}






	public function homepagestorydetail($id, Request $request)
	{
		$story = $request->get('id');
		$story = Story::findorFail($id);

		$authors = DB::table(DB::raw('authors'))->limit(1)->get();

		return view('web-routes.stories.stories-detail', compact('story', 'authors'));
	}

	public function homepagestoryalldata()
	{
		$stories_paginate = DB::table('stories')->paginate(9);
		// $stories_flagship = DB::table('stories')->where('is_flagship', "yes")->get();

		$stories_flagship = Story::where('date_input', Story::max('date_input'))->orderBy('date_input','desc')->get();

		$stories = DB::table('stories')->where('is_covid', "Yes")->get();

		return view('web-routes.stories.stories', compact('stories_paginate','stories_flagship', 'stories'));
	}

	public function searchpaginatestories(Request $request)
	{
		$q = $request->get('q');
		if($q != ""){
			$stories_paginate = Story::where ( 'title', 'LIKE', '%' . $q . '%')
			->orWhere('date_input', 'LIKE', '%' .$q. '%')
			// ->orWhere('description', 'LIKE', '%' . $q . '%')
			->paginate(6)
			->setPath ('');
			$pagination = $stories_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count( $stories_paginate)>0)
				return view('web-routes.stories.searchstories')->withDetails($stories_paginate)->withQuery($q);
		}
		return view('web-routes.stories.searchstories')->withMessage ('No Details found. Try to search again!!!');
	}










	public function homepagenewalldata()
	{	
		$news_paginate = DB::table('news')->paginate(9);
		// $news_flagship = DB::table('news')->get();

		$news_flagship = Newy::where('date_input', Newy::max('date_input'))->orderBy('date_input','desc')->get();
		return view('web-routes.news.news', compact('news_paginate','news_flagship'));
	}

	public function searchpaginatenews(Request $request)
	{
		$q = $request->get('q');
		if($q != ""){
			$news_paginate = Newy::where ( 'title', 'LIKE', '%' . $q . '%')
			->orWhere('date_input', 'LIKE', '%' .$q. '%')
			// ->orWhere('description', 'LIKE', '%' . $q . '%')
			->paginate(6)
			->setPath ('');
			$pagination = $news_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count( $news_paginate)>0)
				return view('web-routes.news.searchnews')->withDetails($news_paginate)->withQuery($q);
		}
		return view('web-routes.news.searchnews')->withMessage ('No Details found. Try to search again!!!');
	}


	public function homepagenewsdetail($id, Request $request)
	{
		$new = $request->get('id');
		$new = Newy::findorFail($id);
		// $authors = DB::select(DB::raw('SELECT id,name,display_image,description,facebook_link,twitter_link FROM authors LIMIT 1'));
		$authors = DB::table(DB::raw('authors'))->limit(1)->get();

		return view('web-routes.news.news-detail', compact('new', 'authors'));
	}


	public function homepagebidsandawardsalldata()
	{
		$awards = DB::table(DB::raw('awards'))->get();
		return view('web-routes.bids_awards.bids-and-awards', compact('awards'));
	}


	public function homepagegalleriesalldata()
	{	
		// $homegalleries = DB::table(DB::raw('home_galleries'))->get();

		// dd($homegalleries);

		return view('web-routes.gallery.gallery');
	}




	public function videosalldata()
	{
		$videos = DB::table(DB::raw('home_videos'))->paginate(10);
		return view('web-routes.video.videos', compact('videos'));
	}

	public function searchpaginatevideos(Request $request)
	{
		$q = $request->get('q');
		if($q != ""){
			$videos_paginate = HomesVideo::where ( 'title', 'LIKE', '%' . $q . '%')
			// ->orWhere('date_input', 'LIKE', '%' .$q. '%')
			// ->orWhere('description', 'LIKE', '%' . $q . '%')
			->paginate(6)
			->setPath ('');
			$pagination = $videos_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count( $videos_paginate)>0)
				return view('web-routes.video.searchvideos')->withDetails($videos_paginate)->withQuery($q);
		}
		return view('web-routes.video.searchvideos')->withMessage ('No Details found. Try to search again!!!');
	}


	public function directorypagedata()
	{	
		$directories = DB::table('directories')->get();
		return view('web-routes.about.directory', compact('directories'));
	}


	public function historypagedata()
	{
		$histories = DB::table(DB::raw('histories'))->get();

		return view('web-routes.about.history', compact('histories'));
	}


	public function barangayspagedata(Request $request)
	{
		$barangays = DB::table('barangays')->get();
		return view('web-routes.about.barangays', compact('barangays'));
	}


	public function barangaydetailpage(Request $request)
	{
		$barangay_id = $request->id;
		$barangaydata = Barangay::findorFail($barangay_id);

		return view('web-routes.about.barangays-detail', compact('barangaydata'));
	}


	public function homepagecovidupdatesalldata()
	{
		$covids_paginate = DB::table('covids')->paginate(9);

		// $covids_flagship = Covid::where('date_created', Covid::max('date_created'))->orderBy('date_created','desc')->get();
		$covids_flagship = DB::table('covids')->get();


		return view('web-routes.covid.covid-all-data', compact('covids_paginate', 'covids_flagship'));
	}



	public function coviddetailpage(Request $request)
	{
		$covid_id = $request->id;
		$covid = Covid::findorFail($covid_id);

		$authors = DB::table(DB::raw('authors'))->limit(1)->get();

		return view('web-routes.covid.covid-detail', compact('covid', 'authors'));
	}



	public function searchpaginatecovid(Request $request)
	{
		$q = $request->get('q');
		if($q != ""){
			$covids_paginate = Covid::where ( 'title', 'LIKE', '%' . $q . '%')
			->orWhere('date_created', 'LIKE', '%' .$q. '%')
			// ->orWhere('description', 'LIKE', '%' . $q . '%')
			->paginate(6)
			->setPath ('');
			$pagination = $covids_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count( $covids_paginate)>0)
				return view('web-routes.covid.searchcovids')->withDetails($covids_paginate)->withQuery($q);
		}
		return view('web-routes.covid.searchcovids')->withMessage ('No Details found. Try to search again!!!');
	}












	public function albumdetailpage($id, Request $request)
	{
		// $album_id = $request->id;
		$album = $request->get('id');
		$album = Album::findorFail($id);

		$albums = Album::select('*')
		->join('home_galleries', 'home_galleries.album_id', '=', 'albums.id', 'left outer')
		->groupBy('album_id')
		->limit(1)
		->get();

		
		$albums2 = Album::select('*')
		->join('home_galleries', 'home_galleries.album_id', '=', 'albums.id', 'left outer')
		->groupBy('album_id')
		->get();

		// $albumdata = DB::table('albums')->where('id', $album_id)->get();

		return view('web-routes.albums.albums-detail', compact('album','albums','albums2'));
	}



	public function homepagealbumsalldata()
	{
		$albums_paginate = Album::select('*')
		->join('home_galleries', 'home_galleries.album_id', '=', 'albums.id', 'left outer')
		->groupBy('album_id')
		->paginate(9);
		// ->get();
		// dd($albums);

		return view('web-routes.albums.albums', compact('albums_paginate'));
	}


	public function searchpaginatealbums(Request $request)
	{
		$q = $request->get('q');
		if($q != "")
		{
			$albums_paginate = Album::select('*')
			->join('home_galleries', 'home_galleries.album_id', '=', 'albums.id', 'left outer')
			->where('title', 'LIKE', '%' . $q . '%')
			->orWhere('date_input', 'LIKE', '%' .$q. '%')
			->groupBy('album_id')
			->paginate(6)
			->setPath ('');
			$pagination = $albums_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count($albums_paginate) > 0)
				return view('web-routes.albums.searchalbums')->withDetails($albums_paginate)->withQuery($q);
		}
		return view('web-routes.albums.searchalbums')->withMessage ('No Details found. Try to search again!!!');
	}




























	// TOP BAR PAGES
	public function officemayor_about_page()
	{

		$mayoroffice = DB::table(DB::raw('mayor_office'))->get();

		return view('web-routes.about.office-mayor', compact('mayoroffice'));
	}

	public function organizationalchartpage()
	{
		return view('web-routes.organizational.organizational-chart');
	}









	public function covidupdatespage()
	{
		return view('web-routes.covid-updates.covid-updates');
	}

	public function transparencypage()
	{
		return view('web-routes.transparency.transparency');
	}

}
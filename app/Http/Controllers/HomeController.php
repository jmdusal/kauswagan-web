<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{	
		$authors = DB::table('authors')->count();
		$stories = DB::table('stories')->count();
		$news = DB::table('news')->count();
		$awards = DB::table('awards')->count();
		$homegalleries = DB::table('home_galleries')->get();
		$albums = DB::table('albums')->get();
		
		$homevideos = DB::table('home_videos')->count();


		$sidebar = "dashboard";

		return view('user.dashboard_user', compact('authors','stories','news','awards','homegalleries','homevideos','sidebar','albums'));
	}

}

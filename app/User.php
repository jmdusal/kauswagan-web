<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Story;
use App\Models\Newy;
use App\Models\Activity;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['name', 'email', 'password', 'created_at', 'updated_at',
];

protected $hidden = [
    'password', 'remember_token',
];
protected $primaryKey = 'id';
public $timestamps = true;
const created_at = 'date_posted';
const updated_at = 'last_update';

}

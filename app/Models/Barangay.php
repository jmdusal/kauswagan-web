<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
	protected $fillable = ['barangay_name', 'description', 'display_image'];
	protected $table = 'barangays';
	protected $primaryKey = 'id';
	public $timestamps = true;



	public function barangaysofficials()
	{
		return $this->hasMany(BarangaysOfficial::class);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Covid extends Model
{
	protected $fillable = ['title','description', 'display_image','date_created','is_flagship','author_id'];
	protected $table = 'covids';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function covidsgalleries()
	{   
		return $this->hasMany(CovidsGallery::class);
	}

	public function author()
	{
		return $this->belongsTo(Author::class, 'author_id');
	}
}

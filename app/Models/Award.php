<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $fillable = ['title', 'description', 'display_image', 'link', 'date_award_taken'];
    protected $table = 'awards';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

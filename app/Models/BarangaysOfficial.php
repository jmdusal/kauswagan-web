<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangaysOfficial extends Model
{
	protected $fillable = ['name', 'position', 'display_image', 'barangay_id'];
	protected $table = 'barangays_official';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function barangay()
	{
		return $this->belongsTo(Barangay::class, 'barangay_id');

	}
}

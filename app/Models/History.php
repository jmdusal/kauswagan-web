<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
	protected $fillable = ['title', 'description'];
	protected $table = 'histories';
	protected $primaryKey = 'id';
	public $timestamps = true;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name','email', 'password', 'created_at', 'updated_at'];
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = true;
    const created_at = 'date_posted';
    const updated_at = 'last_update';

    // public function stories()
    // {
    //     return $this->hasMany(Story::class);
    // }
    // public function activities()
    // {
    //     return $this->hasMany(Activity::class);
    // }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
	protected $fillable = ['title','date_input'];
	protected $table = 'albums';
	protected $primaryKey = 'id';
	public $timestamps = false;


	public function homesgalleries()
	{
		return $this->hasMany(HomesGallery::class);
	}
}

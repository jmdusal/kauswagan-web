<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newy extends Model
{
    protected $fillable = ['title', 'description', 'display_image', 'is_flagship', 'input_by', 'date_input', 'is_covid', 'category_id', 'author_id'];
    protected $table = 'news';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function category()
    {   
        return $this->belongsTo(Category::class, 'category_id');
    }
    
    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id');
    }

    public function newsgalleries()
    {
        return $this->hasMany(NewsGallery::class);
    }

    // public function category()
    // {
    //     return $this->belongsToMany(Category::class, 'category_id');
    // }
    // public function users()
    // {
    //     return $this->belongsToMany('App\User');
    // }
    // public function author()
    // {
    //     return $this->belongsToMany(Authors::class, 'author_id');
    // }
    // public function galleries()
    // {
    //     return $this->hasMany(Gallery::class);
    // }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $fillable = ['category_name', 'created_at', 'updated_at'];
    protected $guarded = [];
    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    // const created_at = 'date_posted';
    // const updated_at = 'last_update';


    public function stories()
    {
        return $this->hasMany(Story::class);
    }

    // public function news()
    // {
    //     return $this->hasMany(Newy::class);
    // }
    // public function activity()
    // {
    //     return $this->hasMany(Activity::class);
    // }

    
}

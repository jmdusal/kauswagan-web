<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsGallery extends Model
{
    protected $fillable = ['display_image', 'date_input', 'newy_id'];
    protected $table = 'news_gallery';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function newy()
    {
        return $this->belongsTo(Newy::class, 'newy_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
	protected $fillable = ['directory_name', 'description'];
	protected $table = 'directories';
	protected $primaryKey = 'id';
	public $timestamps = true;
}

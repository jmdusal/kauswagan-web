<?php

namespace App\Models;
// namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $fillable = ['title','description', 'display_image','is_flagship','input_by', 'category_id', 'author_id', 'date_input', 'is_covid'];
    protected $table = 'stories';
    protected $primaryKey = 'id';
    public $timestamps = true;

    
    
    public function category()
    {   
        return $this->belongsTo(Category::class, 'category_id');
    }
    
    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id');
    }
    
    public function storiesgalleries()
    {   
        return $this->hasMany(StoriesGallery::class);
    }


}
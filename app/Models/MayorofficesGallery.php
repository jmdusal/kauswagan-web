<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MayorofficesGallery extends Model
{
	protected $fillable = ['display_image', 'date_input', 'mayor_office_id'];
	protected $table = 'mayor_office_gallery';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function mayoroffice()
	{
		return $this->belongsTo(Mayoroffice::class);
	}


}

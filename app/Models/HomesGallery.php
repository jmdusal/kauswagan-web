<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomesGallery extends Model
{
	protected $fillable = ['display_image', 'date_taken','album_id'];
	protected $table = 'home_galleries';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function album()
	{
		return $this->belongsTo(Album::class, 'album_id');
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoriesGallery extends Model
{
	
	protected $fillable = ['display_image', 'date_input', 'story_id'];
	protected $table = 'stories_gallery';
	protected $primaryKey = 'id';
	public $timestamps = true;


	public function story()
	{
		return $this->belongsTo(Story::class, 'story_id');
	}
}

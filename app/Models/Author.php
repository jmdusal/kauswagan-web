<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name', 'display_image', 'description', 'facebook_link', 'twitter_link'];
    protected $table = 'authors';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function covids()
    {
        return $this->hasMany(Covid::class);
    }

    public function stories()
    {
        return $this->hasMany(Story::class);
    }

    
    public function newies()
    {
        return $this->hasMany(Newy::class);
    }
    
}

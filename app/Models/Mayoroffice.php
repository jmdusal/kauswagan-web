<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mayoroffice extends Model
{
	protected $fillable = ['mayor_name', 'display_image', 'mini_title', 'description'];
	protected $table = 'mayor_office';
	protected $primaryKey = 'id';
	public $timestamps = true;


	public function mayorofficesgalleries()
	{   
		return $this->hasMany(MayorofficesGallery::class);
	}


}

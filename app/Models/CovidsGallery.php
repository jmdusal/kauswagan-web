<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CovidsGallery extends Model
{
	protected $fillable = ['display_image', 'date_input', 'covid_id'];
	protected $table = 'covids_gallery';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function covid()
	{
		return $this->belongsTo(Covid::class, 'covid_id');
	}
}

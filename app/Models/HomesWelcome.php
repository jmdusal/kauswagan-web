<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomesWelcome extends Model
{
	protected $fillable = ['title', 'display_image', 'description'];
	protected $table = 'home_welcome';
	protected $primaryKey = 'id';
	public $timestamps = true;
}

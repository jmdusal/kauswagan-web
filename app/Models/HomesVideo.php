<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomesVideo extends Model
{
    protected $fillable = ['title', 'display_image', 'embed_video'];
    protected $table = 'home_videos';
    protected $primaryKey = 'id';
    public $timestamps = true;
}
